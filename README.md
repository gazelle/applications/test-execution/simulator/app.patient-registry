# app.patient-registry

## Deployment

This version of Patient Registry is to be deployed in Wildfly 18.0.1.Final.

postgres model shall be added to your the modules of your wildfly server.

## Database

This version of Patient Registry uses PatientManager's database.
Datasource to be added in ${SERVER_PATH}/standalone/configuration/standalone.xml

```
<datasource jndi-name="java:/comp/env/jdbc/patient-registryDS" pool-name="patient-registry" enabled="true" use-java-context="true">
                    <connection-url>jdbc:postgresql://localhost:5432/pam-simulator</connection-url>
                    <driver>postgresql</driver>
                    <security>
                        <user-name>gazelle</user-name>
                        <password>gazelle</password>
                    </security>
                </datasource>
```

If it is the first Gazelle application you are deploying, also declare postgresql driver:

```
<driver name="postgresql" module="org.postgresql">
                        <driver-class>org.postgresql.Driver</driver-class>
                        <xa-datasource-class>org.postgresql.xa.PGXADataSource</xa-datasource-class>
</driver>

```

This application slightly changes the schema of Patient Manager's database, execute patient-registry-war/src/update-db.sql to fix your database.
