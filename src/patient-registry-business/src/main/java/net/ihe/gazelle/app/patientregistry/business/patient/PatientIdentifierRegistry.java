/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.business.patient;

import net.ihe.gazelle.app.patientregistry.business.exceptions.MissingRequiredDataException;

/**
 * <p>PatientIdentifierRegistry class.</p>
 *
 * @author abe
 * @version 1.0: 11/10/2019
 */

public class PatientIdentifierRegistry {

    private PatientIdentifier patientIdentifier;

    private Patient patient;

    public PatientIdentifierRegistry(PatientIdentifier patientIdentifier, Patient patient) throws MissingRequiredDataException {
        if (patientIdentifier == null || patient == null){
            throw new MissingRequiredDataException("Both the patient and the patientIdentifier shall be provided");
        } else {
            this.patientIdentifier = patientIdentifier;
            this.patient = patient;
        }
    }

    public PatientIdentifier getPatientIdentifier() {
        return patientIdentifier;
    }

    public void setPatientIdentifier(PatientIdentifier patientIdentifier) {
        this.patientIdentifier = patientIdentifier;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }


    @Override
    public String toString(){
        final StringBuilder sb = new StringBuilder("PatientIdentifierRegistry{");
        sb.append("patient=").append(patient);
        sb.append(", patientIdentifier=").append(patientIdentifier);
        sb.append('}');
        return sb.toString();
    }
}
