/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.business.patient;

/**
 * <p>ObservationTypeEnum enum.</p>
 *
 * @author abe
 * @version 1.0: 10/12/2019
 */
public enum ObservationTypeEnum implements ObservationType {
    ACCOUNT_NUMBER, MOTHERS_MAINDEN_NAME, BIRTHPLACE_NAME, BLOOD_GROUP, LUNAR_DATE_MONTH, RELIGION, RACE, LUNAR_DATE_WEEK, WEEKS_OF_GESTATION,
    LUNAR_DATE_YEAR, IS_PREGNANT;


    @Override
    public String getLabel() {
        return name();
    }
}
