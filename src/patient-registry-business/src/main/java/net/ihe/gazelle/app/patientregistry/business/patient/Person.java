/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.business.patient;

import net.ihe.gazelle.app.patientregistry.business.exceptions.MissingRequiredDataException;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * <p>Person class.</p>
 * This class represent a person, it might be a patient but can be a relative or a practitioner
 * @author abe
 * @version 1.0: 11/10/2019
 */

public class Person {

    protected Set<PersonName> names = new HashSet<>();

    protected GenderCode gender;

    protected Date dateOfBirth;

    protected Set<Address> addresses = new HashSet<>();

    protected Set<ContactPoint> contactPoints = new HashSet<>();

    protected Set<Observation> observations = new HashSet<>();

    public Person() {
    }

    public Set<PersonName> getNames() {
        return names;
    }

    public void setNames(Set<PersonName> names) {
        this.names = names;
    }

    public GenderCode getGender() {
        return gender;
    }

    public void setGender(GenderCode gender) {
        this.gender = gender;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }

    public Set<ContactPoint> getContactPoints() {
        return contactPoints;
    }

    public void setContactPoints(Set<ContactPoint> contactPoints) {
        this.contactPoints = contactPoints;
    }

    public Set<Observation> getObservations() {
        return observations;
    }

    public void setObservations(Set<Observation> observations) {
        this.observations = observations;
    }

    public void addContactPoint(ContactPoint contactPoint){
        if (this.contactPoints == null){
            this.contactPoints = new HashSet<ContactPoint>();
        }
        this.contactPoints.add(contactPoint);
    }

    public void addAddress(Address address){
        if (this.addresses == null){
            this.addresses = new HashSet<Address>();
        }
        this.addresses.add(address);
    }

    @Override
    public String toString(){
        final StringBuilder sb = new StringBuilder("Person{");
        sb.append("names=").append(names);
        sb.append(", gender=").append(gender);
        sb.append(", dateOfBirth='").append(dateOfBirth).append('\'');
        sb.append(", addresses=").append(addresses);
        sb.append(", contactPoints=").append(contactPoints);
        sb.append(", observations=").append(observations);
        sb.append('}');
        return sb.toString();
    }

    public void addObservation(ObservationType observationType, String value) throws MissingRequiredDataException {
        if (this.observations == null){
            this.observations = new HashSet<Observation>();
        }
        this.observations.add(new Observation(observationType, value));
    }

    public void addName(PersonName personName) {
        if (this.names == null){
            this.names = new HashSet<PersonName>();
        }
        this.names.add(personName);
    }
}
