/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.business.patient;

/**
 * <p>TelecommunicationType class.</p>
 * Type of telecommunication mean (email address, phone number, fax number)
 * @author abe
 * @version 1.0: 11/10/2019
 */

public enum ContactPointType {

    PHONE, // telephone
    FAX, // fax
    MODEM, // modem
    MOBILE, // cellular or mobile phone
    BEEPER, // beeper or pager
    EMAIL, // Internet address
    DEAF_DEVICE, // Telecommunications device for the deaf
    URL, //A contact that is not a phone, fax, pager or email address and is expressed as a URL (ex: Skype, Twitter, Facebook).
    OTHER,
    SMS // A contact that can be use for sending a sms message
}
