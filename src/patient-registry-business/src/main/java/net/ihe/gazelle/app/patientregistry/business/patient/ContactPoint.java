/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.business.patient;

/**
 * <p>Telecommunication class.</p>
 * Telecommunication represents either a phone number or an email address or any other telecommunication mean (described by ms_type)
 * @author abe
 * @version 1.0: 11/10/2019
 */

public class ContactPoint {

    /*
     The phone number, the email address, fax number... anything representing the information to contact the person
     */
    private String value;

    /*
    The type of telecommunication: fax number, phone number, email address
     */
    private ContactPointType type;

    /*
     Is it a personal number/email, work email/phone number etc
     */
    private ContactPointUse use;

    public ContactPoint(){

    }

    public ContactPoint(String value, ContactPointType type, ContactPointUse use) {
        this.value = value;
        this.type = type;
        this.use = use;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String s_value) {
        this.value = s_value;
    }

    public ContactPointType getType() {
        return type;
    }

    public void setType(ContactPointType s_type) {
        this.type = s_type;
    }

    public ContactPointUse getUse() {
        return use;
    }

    public void setUse(ContactPointUse s_use) {
        this.use = s_use;
    }

    @Override
    public String toString(){
        final StringBuilder sb = new StringBuilder("ContactPoint{");
        sb.append("use='").append(use).append('\'');
        sb.append(", type=''").append(type).append('\'');
        sb.append(", value='").append(value).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
