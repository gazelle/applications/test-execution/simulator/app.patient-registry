/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.business.patient;

import net.ihe.gazelle.app.patientregistry.business.exceptions.MissingRequiredDataException;

/**
 * <p>Observation class.</p>
 * This class is used to describe person characteristics in addition to the attributes common to each person. Examples: race, religion, marital status
 * @author abe
 * @version 1.0: 17/10/2019
 */

public class Observation {

    /*
     The type of observation (what trait does it refer to)
     */
    private ObservationType observationType;

    /*
     The value of the observation
     */
    private String value;

    public Observation(ObservationType observationType, String value) throws MissingRequiredDataException {
        if (observationType == null){
            throw new MissingRequiredDataException("The type of observation shall be provided");
        }
        this.observationType = observationType;
        this.value = value;
    }

    public ObservationType getObservationType() {
        return observationType;
    }

    public void setObservationType(ObservationType observationType) {
        this.observationType = observationType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString(){
        final StringBuilder sb = new StringBuilder("Observation{");
        sb.append("observationType=").append(observationType);
        sb.append(", value='").append(value).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
