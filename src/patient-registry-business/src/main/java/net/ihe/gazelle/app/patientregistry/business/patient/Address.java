/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.business.patient;

import java.util.HashSet;
import java.util.Set;

/**
 * <p>Address class.</p>
 * This class represent a mailing address
 *
 * @author abe
 * @version 1.0: 11/10/2019
 */

public class Address {

    /*
     city name in plain text
     */
    private String city;

    /*
     The ISO3166/3 code to identify a country
     */
    private String countryIso3;

    /*
    postal code
     */
    private String postalCode;

    /*
    state, country, parish, province
     */
    private String state;

    /*
     Street name, number, direction & P.O. Box etc.
     This repeating element order: The order in which lines should appear in an address label
     */
    private Set<String> lines;

    /*
    use of this address: home, work, temp...
     */
    private AddressUse use;

    public Address() {
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountryIso3() {
        return countryIso3;
    }

    public void setCountryIso3(String countryIso3) {
        this.countryIso3 = countryIso3;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Set<String> getLines() {
        return lines;
    }

    public void setLines(Set<String> lines) {
        this.lines = lines;
    }

    public void removeLine(String value){
        if (this.lines != null) {
            this.lines.remove(value);
        }
    }

    public void addLine(String value){
        if (this.lines == null){
            this.lines = new HashSet<String>();
        }
        this.lines.add(value);
    }

    public AddressUse getUse() {
        return use;
    }

    public void setUse(AddressUse o_use) {
        this.use = o_use;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Address{");
        sb.append("use='").append(use).append('\'');
        sb.append(", lines=").append(lines);
        sb.append(", city='").append(city).append('\'');
        sb.append(", postalCode='").append(postalCode).append('\'');
        sb.append(", state='").append(state).append('\'');
        sb.append(", country='").append(countryIso3).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
