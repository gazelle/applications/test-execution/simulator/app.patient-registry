/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.business.search;

/**
 * <p>PatientIdentifier class.</p>
 * Represent the identifier of a patient. note that an identifier without assigning authority is meaningless
 *
 * @author abe
 * @version 1.0: 11/10/2019
 */

public class IdentifierSearchCriteria {

    /*
     The value of the patient identifier.
     Might be named extension in HL7V3, IDNumber in HL7v2
     */
    private String value;

    /*
     The type of identifier. Example: PI, MPID, SPID etc
     HL7v2: CX- / HL7v3: Role.code / FHIR: identifier.use
     */
    private String type;

    /*
     Identifies, by its OID, the system that owns this identifier
     HL7v2: HD-2 / HL7v3: II@system / FHIR: identifier.system
     */
    private String systemIdentifier;

    /*
    Name of the system that owns the identifier
    HL7v2: HD-1 / HL7v3: II@assigningAuthorityName
     */
    private String systemName;

    public IdentifierSearchCriteria(){

    }

    public IdentifierSearchCriteria(String value, String systemIdentifier) {
        this.value = value;
        this.systemIdentifier = systemIdentifier;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSystemIdentifier() {
        return systemIdentifier;
    }

    public void setSystemIdentifier(String systemIdentifier) {
        this.systemIdentifier = systemIdentifier;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PatientIdentifier{");
        sb.append("value='").append(value).append('\'');
        sb.append(", systemIdentifier='").append(systemIdentifier).append('\'');
        sb.append(", systemName='").append(systemName).append('\'');
        sb.append(", type='").append(type).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
