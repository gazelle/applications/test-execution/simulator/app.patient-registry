/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.business.patient;

/**
 * <p>CodingValue interface.</p>
 * This interface represent an attribute which have a common label but transmitted code depends on the underlying standard because not all the
 * standards use the same value sets
 * @author abe
 * @version 1.0: 11/10/2019
 */
public interface Code {

    /**
     * The human readable label
     * @return a human readable label for this entry
     */
    String getLabel();

    /**
     * The code to be transmitted
     * @return the code to be transmitted
     */
    String getCode();

    /**
     * Coding System
     * @return the code system from which this code is taken
     */
    String getCodingSystem();
}
