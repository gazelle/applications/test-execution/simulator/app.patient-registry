/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.business.patient;

/**
 * <p>AddressUse enumeration.</p>
 * The use of an address: home, work, temporary etc
 * @author abe
 * @version 1.0: 11/10/2019
 */
public enum AddressUse {

    BIRTH_PLACE, // birth address (no further defined)
    BIRTH_DELIVERY, // where the birth occurred
    HOME, // address at home
    PRIMARY_HOME, // to reach a person after business hours
    WORK, // office address
    TEMPORARY, // temporary address, currently in use
    VACATION_HOME, // to reach a person while on vacation
    BAD, // old address, useless
    BILLING, //
    DIRECT, // work place - reach the individual without intermediaries
    PUBLIC, // work place - may reach a reception service, mail-room etc
    COUNTRY_OF_ORIGIN,
    LEGAL, // Legal address
    MAILING, // Mailing address
    BIRTH_RESIDENCE, // residence at birth
    SERVICE, // service location
    SHIPPING, // shipping address
}
