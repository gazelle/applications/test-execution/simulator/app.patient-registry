/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.business.search;

/**
 * <p>GenderCode enum.</p>
 *
 * @author abe
 * @version 1.0: 03/12/2019
 */
public enum GenderCode {

    MALE(net.ihe.gazelle.app.patientregistry.business.patient.GenderCode.MALE),
    FEMALE(net.ihe.gazelle.app.patientregistry.business.patient.GenderCode.FEMALE),
    UNDEFINED(net.ihe.gazelle.app.patientregistry.business.patient.GenderCode.UNDEFINED),
    OTHER(net.ihe.gazelle.app.patientregistry.business.patient.GenderCode.OTHER);

    private final net.ihe.gazelle.app.patientregistry.business.patient.GenderCode patientGenderCode;

    private GenderCode(net.ihe.gazelle.app.patientregistry.business.patient.GenderCode patientGenderCode) {
        this.patientGenderCode = patientGenderCode;
    }

    public static GenderCode valueOf(net.ihe.gazelle.app.patientregistry.business.patient.GenderCode patientGenderCode) {
        switch (patientGenderCode) {
            case MALE: return MALE;
            case FEMALE: return FEMALE;
            case UNDEFINED: return UNDEFINED;
            case OTHER: return OTHER;
            default: throw new IllegalArgumentException("Unkown Patient GenderCode");
        }
    }

    public net.ihe.gazelle.app.patientregistry.business.patient.GenderCode toPatientGenderCode() {
        return patientGenderCode;
    }

}
