/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.business.search;

import net.ihe.gazelle.app.patientregistry.business.patient.Address;
import net.ihe.gazelle.app.patientregistry.business.patient.ContactPoint;
import net.ihe.gazelle.app.patientregistry.business.patient.PersonName;

import java.util.List;

/**
 * <p>PatientCriteria class.</p>
 *
 * @author abe
 * @version 1.0: 03/11/2019
 */
public class PatientSearchCriteria {

    private List<PersonName> personNames;
    private String mothersMaidenName;
    private DateInterval dateOfBirth;
    private DateInterval dateOfDeath;
    private Boolean deceased;
    private GenderCode genderCode;
    private List<IdentifierSearchCriteria> identifiers;
    private IdentifierSearchCriteria accountNumber;
    private List<Address> addresses;
    private List<ContactPoint> contactPoints;
    private Boolean active;

    public PatientSearchCriteria() {
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getMothersMaidenName() {
        return mothersMaidenName;
    }

    public void setMothersMaidenName(String mothersMaidenName) {
        this.mothersMaidenName = mothersMaidenName;
    }

    public DateInterval getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(DateInterval dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public GenderCode getGenderCode() {
        return genderCode;
    }

    public void setGenderCode(GenderCode genderCode) {
        this.genderCode = genderCode;
    }

    public List<IdentifierSearchCriteria> getIdentifiers() {
        return identifiers;
    }

    public void setIdentifiers(List<IdentifierSearchCriteria> identifiers) {
        this.identifiers = identifiers;
    }

    public IdentifierSearchCriteria getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(IdentifierSearchCriteria accountNumber) {
        this.accountNumber = accountNumber;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public List<ContactPoint> getContactPoints() {
        return contactPoints;
    }

    public void setContactPoints(List<ContactPoint> contactPoints) {
        this.contactPoints = contactPoints;
    }

    public List<PersonName> getPersonNames() {
        return personNames;
    }

    public void setPersonNames(List<PersonName> personNames) {
        this.personNames = personNames;
    }

    public DateInterval getDateOfDeath() {
        return dateOfDeath;
    }

    public void setDateOfDeath(DateInterval dateOfDeath) {
        this.dateOfDeath = dateOfDeath;
    }

    public Boolean getDeceased() {
        return deceased;
    }

    public void setDeceased(Boolean deceased) {
        this.deceased = deceased;
    }
}
