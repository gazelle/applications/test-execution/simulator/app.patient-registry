/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.business.patient;

/**
 * <p>TelecommunicationUse interface.</p>
 * Represent the usage of the telecommunication mean
 * @author abe
 * @version 1.0: 11/10/2019
 */
public enum ContactPointUse {

    HOME, // home
    PRIMARY_HOME, // primary home
    WORK, // work or office
    TEMPORARY, // vacation/temporary home number
    EMERGENCY, // emergency number
    MOBILE, // personal mobile number
    BEEPER, // personal beeper/pager number
    EMAIL, // network (email) address
    OTHER, // other use
    ANSWERING_SERVICE, // automated answering machine used for less urgent cases
    DIRECT, // work place - reach the individual without intermediaries
    PUBLIC, // work place - may reach a reception service, mail-room etc
    BAD, // old, useless

}
