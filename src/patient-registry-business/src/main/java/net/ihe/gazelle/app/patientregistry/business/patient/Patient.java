/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.business.patient;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * <p>AbstractPatient class.</p>
 * This class represent a patient as it is basically used in Gazelle applications
 * @author abe
 * @version 1.0: 11/10/2019
 */

public class Patient extends Person {

    private String uuid;

    private boolean active;

    // TODO positive integer only - set to 0 if single birth
    /*
    If the patient is part of a multiple birth, the rank of birth
     */
    private Integer multipleBirthOrder;

    /*
    If the patient is dead, the date/time of death
     */
    private Date deathDateTime;

    /*
    The patients' contacts, relatives etc
     */
    private Set<RelatedPerson> contacts = new HashSet<>();

    private Set<PatientIdentifier> identifiers = new HashSet<>();

    public boolean isMultipleBirth(){
        return this.multipleBirthOrder > 0;
    }

    public boolean isPatientDead(){
        return this.deathDateTime != null;
    }

    public Patient() {
        super();
        this.multipleBirthOrder = 0;
        this.deathDateTime = null;
    }

    public Integer getMultipleBirthOrder() {
        return multipleBirthOrder;
    }

    public void setMultipleBirthOrder(Integer multipleBirthOrder) {
        this.multipleBirthOrder = multipleBirthOrder;
    }

    public Date getDeathDateTime() {
        return deathDateTime;
    }

    public void setDeathDateTime(Date deathDateTime) {
        this.deathDateTime = deathDateTime;
    }

    public Set<RelatedPerson> getContacts() {
        return contacts;
    }

    public void setContacts(Set<RelatedPerson> contacts) {
        this.contacts = contacts;
    }

    @Override
    public String toString(){
        final StringBuilder sb = new StringBuilder("Patient{");
        sb.append(super.toString());
        sb.append(", multipleBirthOrder='").append(multipleBirthOrder).append('\'');
        sb.append(", deathDateTime='").append(deathDateTime).append('\'');
        sb.append(", contacts=").append(contacts);
        sb.append('}');
        return sb.toString();
    }

    public Set<PatientIdentifier> getIdentifiers() {
        return identifiers;
    }

    public void setIdentifiers(Set<PatientIdentifier> identifiers) {
        this.identifiers = identifiers;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void addPatientIdentifier(PatientIdentifier patientIdentifier) {
        if (this.identifiers == null){
            this.identifiers = new HashSet<PatientIdentifier>();
        }
        this.identifiers.add(patientIdentifier);
    }
}
