/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.business.search;

import java.util.Date;

/**
 * <p>DateInterval class.</p>
 *
 * @author abe
 * @version 1.0: 19/11/2019
 */

public class DateInterval {

    private Date lowLimit;

    private Boolean lowLimitInclusive;

    private Date highLimit;

    private Boolean highLimitInclusive;

    private Date exactDate;

    public DateInterval(){

    }

    public Boolean isLowLimitInclusive() {
        return lowLimitInclusive;
    }

    public void setLowLimitInclusive(Boolean lowLimitInclusive) {
        this.lowLimitInclusive = lowLimitInclusive;
    }

    public Boolean isHighLimitInclusive() {
        return highLimitInclusive;
    }

    public void setHighLimitInclusive(Boolean highLimitInclusive) {
        this.highLimitInclusive = highLimitInclusive;
    }

    public Date getExactDate() {
        return exactDate;
    }

    public void setExactDate(Date exactDate) {
        this.exactDate = exactDate;
    }

    public Date getLowLimit() {
        return lowLimit;
    }

    public void setLowLimit(Date lowLimit) {
        this.lowLimit = lowLimit;
    }

    public Date getHighLimit() {
        return highLimit;
    }

    public void setHighLimit(Date highLimit) {
        this.highLimit = highLimit;
    }
}
