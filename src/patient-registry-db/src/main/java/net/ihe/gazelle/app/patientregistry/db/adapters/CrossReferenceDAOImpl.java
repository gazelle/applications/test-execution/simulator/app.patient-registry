/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use patientDB file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.db.adapters;

import net.ihe.gazelle.app.patientregistry.application.search.CrossReferenceDAO;
import net.ihe.gazelle.app.patientregistry.business.patient.Patient;
import net.ihe.gazelle.app.patientregistry.business.search.PatientIdentifierCriteria;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * <p>CrossReferenceDAO class.</p>
 *
 * @author abe
 * @version 1.0: 03/12/2019
 */
@Named("crossReferenceDAO")
public class CrossReferenceDAOImpl implements CrossReferenceDAO {

    @PersistenceContext
    private EntityManager entityManager;

    public CrossReferenceDAOImpl(){

    }

    public CrossReferenceDAOImpl(EntityManager entityManager){
        this.entityManager = entityManager;
    }

    public List<Patient> searchForReferences(PatientIdentifierCriteria patientIdentifierCriteria, List<String> restrictedDomain) {
       return null;
    }
}
