/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.db.adapters;

import net.ihe.gazelle.app.patientregistry.db.patient.PatientDB;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * <p>QueryBuilder class.</p>
 *
 * @author abe
 * @version 1.0: 13/12/2019
 */

public class QueryBuilder<T> {

    private Map<String, Object> queryParameters;
    private Class<T> queriedClass;
    private CriteriaQuery<T> criteriaQuery;
    private CriteriaBuilder builder;
    private EntityManager entityManager;
    private Set<ParameterExpression> parameterExpressions;

    public QueryBuilder(Class<T> queriedClass, EntityManager entityManager){
        this.queriedClass = queriedClass;
        this.queryParameters = new HashMap<String, Object>();
        this.entityManager = entityManager;
        this.parameterExpressions = new HashSet<ParameterExpression>();
        initQueryString();
    }

    private void initQueryString() {
        builder = entityManager.getCriteriaBuilder();
        criteriaQuery = builder.createQuery(queriedClass);
    }

    public void addQueryParameter(String parameterName, Object value, Class<?> valueType){
        this.queryParameters.put(parameterName, value);
        ParameterExpression parameterExpression = builder.parameter(valueType, parameterName);
        parameterExpressions.add(parameterExpression);
    }

    public void appendClause(String clause){

    }

    public TypedQuery<T> createTypedQuery(EntityManager entityManager){
        TypedQuery<T> query = entityManager.createQuery(criteriaQuery);
        for (Map.Entry<String, Object> entry: queryParameters.entrySet()){
            query.setParameter(entry.getKey(), entry.getValue());
        }
        return query;
    }
}
