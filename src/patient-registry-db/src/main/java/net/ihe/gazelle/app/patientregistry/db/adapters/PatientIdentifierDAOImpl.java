/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use patientIdentifier file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.db.adapters;

import net.ihe.gazelle.app.patientregistry.application.search.PatientIdentifierDAO;
import net.ihe.gazelle.app.patientregistry.business.patient.Patient;
import net.ihe.gazelle.app.patientregistry.business.patient.PatientIdentifier;
import net.ihe.gazelle.app.patientregistry.db.converters.PatientIdentifierDBConverter;
import net.ihe.gazelle.app.patientregistry.db.patient.HierarchicDesignator;
import net.ihe.gazelle.app.patientregistry.db.patient.PatientDB;
import net.ihe.gazelle.app.patientregistry.db.patient.PatientIdentifierDB;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;
import java.util.List;
import java.util.Set;

/**
 * <p>PatientIdentifierDAO class.</p>
 *
 * @author abe
 * @version 1.0: 03/12/2019
 */

@Named("patientIdentifierDAO")
public class PatientIdentifierDAOImpl implements PatientIdentifierDAO {

    @PersistenceContext(unitName = "patientRegistryPU")
    EntityManager entityManager;

    public PatientIdentifierDAOImpl(EntityManager entityManager){
        this.entityManager = entityManager;
    }

    public PatientIdentifierDAOImpl(){

    }

    @Override
    public Set<PatientIdentifier> getPatientIdentifiersForDomain(Patient patient, List<String> restrictedDomains) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<PatientIdentifierDB> criteriaQuery = criteriaBuilder.createQuery(PatientIdentifierDB.class);
        Root<PatientIdentifierDB> root = criteriaQuery.from(PatientIdentifierDB.class);

        Metamodel metamodel = entityManager.getMetamodel();

        EntityType<PatientIdentifierDB> patientIdentifierDBMetamodel = metamodel.entity(PatientIdentifierDB.class);
        Join<PatientIdentifierDB, PatientDB> joinPatient = root.join(patientIdentifierDBMetamodel.getList("patients", PatientDB.class));
        Path<String> patientUuidPath = joinPatient.get("uuid");
        Predicate patientUuidPredicate = criteriaBuilder.equal(patientUuidPath, patient.getUuid());

        TypedQuery<PatientIdentifierDB> query;

        if (restrictedDomains == null || restrictedDomains.isEmpty()){
            query = entityManager.createQuery(criteriaQuery.select(root).where(patientUuidPredicate));
        } else {
            Join<PatientIdentifierDB, HierarchicDesignator> joinDomain = root.join(patientIdentifierDBMetamodel.getList("domain", HierarchicDesignator.class));
            Path<String> universalIDPath = joinDomain.get("universalID");
            Predicate inDomainPredicate = universalIDPath.in(restrictedDomains);
            query = entityManager.createQuery(criteriaQuery.select(root).where(criteriaBuilder.and(inDomainPredicate, patientUuidPredicate)));
        }
        List<PatientIdentifierDB> patientIdentifierDBS = query.getResultList();
        return PatientIdentifierDBConverter.toPatientIdentifiers(patientIdentifierDBS);
    }
}
