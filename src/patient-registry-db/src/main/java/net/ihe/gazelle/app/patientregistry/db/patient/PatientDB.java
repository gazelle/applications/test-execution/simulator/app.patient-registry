package net.ihe.gazelle.app.patientregistry.db.patient;

import net.ihe.gazelle.app.patientregistry.db.references.CrossReference;
import org.slf4j.LoggerFactory;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.NoResultException;
import javax.persistence.OneToMany;
import javax.persistence.Query;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>Patient class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Entity
@NamedQueries({
        @NamedQuery(name="PatientDB.findByUuid",
        query="SELECT p FROM PatientDB p WHERE p.uuid = :uuid"),
@NamedQuery(name="PatientDB.findForSimulatedActor",
        query="SELECT p FROM PatientDB p WHERE p.simulatedActor = :actorKeyword")
        })
@DiscriminatorValue("pam_patient")
public class PatientDB extends AbstractPatient {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static int generalFakeId = 0;
    private transient int fakeId;
    private static org.slf4j.Logger log = LoggerFactory.getLogger(PatientDB.class);

    @Column(name = "uuid")
    private String uuid;

    @Column(name = "email")
    private String email;

    @OneToMany(mappedBy = "patient", fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    private List<PatientPhoneNumber> phoneNumbers;

    @Column(name = "creator")
    private String creator;

    @Column(name = "simulated_actor_keyword")
    private String simulatedActor;

    @Column(name = "still_active")
    private Boolean stillActive;

    @Column(name = "account_number")
    private String accountNumber;

    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "pam_patient_patient_identifier", joinColumns = @JoinColumn(name = "patient_id"), inverseJoinColumns = @JoinColumn(name =
            "patient_identifier_id"), uniqueConstraints = @UniqueConstraint(columnNames = {
            "patient_id", "patient_identifier_id"}))
    private List<PatientIdentifierDB> patientIdentifiers = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "cross_reference_id")
    private CrossReference pixReference;

    @Column(name = "blood_group")
    private String bloodGroup;

    @Column(name = "vip_indicator")
    private String vipIndicator;

    /**
     * those extra columns are used to store the double metaphone (needed for automatically cross-referencing patients)
     */
    @Column(name = "dmetaphone_first_name")
    private String dmetaphoneFirstName;

    @Column(name = "dmetaphone_last_name")
    private String dmetaphoneLastName;

    @Column(name = "dmetaphone_mother_maiden_name")
    private String dmetaphoneMotherMaidenName;

    @Column(name = "multiple_birth_indicator")
    private Boolean multipleBirthIndicator;

    @Transient
    private String multipleBirthIndicatorAsString;

    @Transient
    private String birthOrderAsString;

    @Column(name = "birth_order")
    private Integer birthOrder;

    @Column(name = "last_update_facility")
    private String lastUpdateFacility;

    @Column(name = "marital_status")
    private String maritalStatus;

    @Column(name = "identity_reliability_code")
    private String identityReliabilityCode;

    @Column(name = "is_patient_dead")
    private boolean patientDead;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "patient_death_time")
    private Date patientDeathTime;

    @Column(name = "is_test_data")
    private boolean testData;

    @OneToMany(mappedBy = "patient", targetEntity = Person.class)
    // TODO @Cascade(org.hibernate.annotations.CascadeType.MERGE)
    private List<Person> personalRelationships;

    @Column(name = "birth_place_name")
    private String birthPlaceName;

    /**
     * Constructors
     */
    public PatientDB() {
        super();
        this.pixReference = null;
        setFakeId();
        patientDead = false;
        patientDeathTime = null;
        this.testData = false;
        this.stillActive = true;
    }

    private void setFakeId() {
        synchronized (PatientDB.class) {
            fakeId = generalFakeId++;
        }
    }

    /**
     * <p>Constructor for Patient.</p>
     *
     */
    public PatientDB(AbstractPatient abPatient) {

    }

    /**
     * <p>Constructor for Patient.</p>
     *
     * @param oldPatient     a {@link PatientDB} object.
     */
    public PatientDB(PatientDB oldPatient, String simulatedActor) {

    }

    /**
     * <p>isTestData.</p>
     *
     * @return a boolean.
     */
    public boolean isTestData() {
        return testData;
    }

    /**
     * <p>Setter for the field <code>testData</code>.</p>
     *
     * @param testData a boolean.
     */
    public void setTestData(boolean testData) {
        this.testData = testData;
    }

    /**
     * <p>isPatientDead.</p>
     *
     * @return a boolean.
     */
    public boolean isPatientDead() {
        return patientDead;
    }

    /**
     * <p>Setter for the field <code>patientDead</code>.</p>
     *
     * @param patientDead a boolean.
     */
    public void setPatientDead(boolean patientDead) {
        this.patientDead = patientDead;
    }

    /**
     * <p>Getter for the field <code>patientDeathTime</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getPatientDeathTime() {
        return patientDeathTime;
    }

    /**
     * <p>Setter for the field <code>patientDeathTime</code>.</p>
     *
     * @param patientDeathTime a {@link java.util.Date} object.
     */
    public void setPatientDeathTime(Date patientDeathTime) {
        if (patientDeathTime != null) {
            this.patientDeathTime = (Date) patientDeathTime.clone();
        } else {
            this.patientDeathTime = null;
        }
    }

    /**
     * <p>Getter for the field <code>maritalStatus</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getMaritalStatus() {
        return maritalStatus;
    }

    /**
     * <p>Setter for the field <code>maritalStatus</code>.</p>
     *
     * @param maritalStatus a {@link java.lang.String} object.
     */
    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    /**
     * <p>Getter for the field <code>identityReliabilityCode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getIdentityReliabilityCode() {
        return identityReliabilityCode;
    }

    /**
     * <p>Setter for the field <code>identityReliabilityCode</code>.</p>
     *
     * @param identityReliabilityCode a {@link java.lang.String} object.
     */
    public void setIdentityReliabilityCode(String identityReliabilityCode) {
        this.identityReliabilityCode = identityReliabilityCode;
    }

    /**
     * <p>Getter for the field <code>creator</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCreator() {
        return creator;
    }

    /**
     * <p>Setter for the field <code>creator</code>.</p>
     *
     * @param creator a {@link java.lang.String} object.
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * <p>Setter for the field <code>simulatedActor</code>.</p>
     *
     * @param simulatedActor .
     */
    public void setSimulatedActor(String simulatedActor) {
        this.simulatedActor = simulatedActor;
    }

    /**
     * <p>Getter for the field <code>simulatedActor</code>.</p>
     *
     */
    public String getSimulatedActor() {
        return simulatedActor;
    }

    /**
     * <p>Getter for the field <code>email</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getEmail() {
        return email;
    }

    /**
     * <p>Getter for the field <code>vipIndicator</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getVipIndicator() {
        return vipIndicator;
    }

    /**
     * <p>Setter for the field <code>vipIndicator</code>.</p>
     *
     * @param vipIndicator a {@link java.lang.String} object.
     */
    public void setVipIndicator(String vipIndicator) {
        this.vipIndicator = vipIndicator;
    }

    /**
     * <p>Setter for the field <code>email</code>.</p>
     *
     * @param email a {@link java.lang.String} object.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * <p>Setter for the field <code>stillActive</code>.</p>
     *
     * @param stillActive a {@link java.lang.Boolean} object.
     */
    public void setStillActive(Boolean stillActive) {
        this.stillActive = stillActive;
    }

    /**
     * <p>Getter for the field <code>stillActive</code>.</p>
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean getStillActive() {
        return stillActive;
    }

    /**
     * <p>Setter for the field <code>patientIdentifiers</code>.</p>
     *
     * @param patientIdentifiers a {@link java.util.List} object.
     */
    public void setPatientIdentifiers(List<PatientIdentifierDB> patientIdentifiers) {
        this.patientIdentifiers = patientIdentifiers;
    }

    /**
     * <p>Getter for the field <code>patientIdentifiers</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<PatientIdentifierDB> getPatientIdentifiers() {
      /*  if (getId() != null) {
            patientIdentifiers = HibernateHelper.getLazyValue(this, "patientIdentifiers", patientIdentifiers);
        }*/
        return this.patientIdentifiers;
    }

    /**
     * <p>Getter for the field <code>phoneNumbers</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<PatientPhoneNumber> getPhoneNumbers() {
        if (phoneNumbers == null) {
            phoneNumbers = new ArrayList<PatientPhoneNumber>();
        }
        return phoneNumbers;
    }

    /**
     * <p>Setter for the field <code>phoneNumbers</code>.</p>
     *
     * @param phoneNumbers a {@link java.util.List} object.
     */
    public void setPhoneNumbers(List<PatientPhoneNumber> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }


    /**
     * <p>computeDoubleMetaphones.</p>
     *
     * @param entityManager a {@link javax.persistence.EntityManager} object.
     *
     * @return a {@link PatientDB} object.
     */
    public PatientDB computeDoubleMetaphones(EntityManager entityManager) {
        Query query = entityManager
                .createQuery("SELECT dmetaphone(firstName), dmetaphone(lastName), dmetaphone(motherMaidenName) FROM Patient where id=:patientId");
        query.setParameter("patientId", this.getId());
        try {
            Object[] metaphones = (Object[]) query.getSingleResult();
            this.dmetaphoneFirstName = (String) metaphones[0];
            this.dmetaphoneLastName = (String) metaphones[1];
            this.dmetaphoneMotherMaidenName = (String) metaphones[2];
            PatientDB patient = entityManager.merge(this);
            entityManager.flush();
            return patient;
        } catch (NoResultException e) {
            log.error("Cannot compute double metaphones for patient with id " + this.getId());
            return this;
        }
    }



    /**
     * <p>Setter for the field <code>accountNumber</code>.</p>
     *
     * @param accountNumber a {@link java.lang.String} object.
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * <p>Getter for the field <code>accountNumber</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * <p>Getter for the field <code>pixReference</code>.</p>
     *
     */
    public CrossReference getPixReference() {
        return pixReference;
    }

    /**
     * <p>Setter for the field <code>pixReference</code>.</p>
     *
     */
    public void setPixReference(CrossReference pixReference) {
        this.pixReference = pixReference;
    }

    /**
     * <p>Getter for the field <code>dmetaphoneFirstName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDmetaphoneFirstName() {
        return dmetaphoneFirstName;
    }

    /**
     * <p>Getter for the field <code>dmetaphoneLastName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDmetaphoneLastName() {
        return dmetaphoneLastName;
    }

    /**
     * <p>Getter for the field <code>dmetaphoneMotherMaidenName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDmetaphoneMotherMaidenName() {
        return dmetaphoneMotherMaidenName;
    }

    /**
     * <p>Getter for the field <code>bloodGroup</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getBloodGroup() {
        return bloodGroup;
    }

    /**
     * <p>Setter for the field <code>bloodGroup</code>.</p>
     *
     * @param bloodGroup a {@link java.lang.String} object.
     */
    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    /**
     * <p>Getter for the field <code>birthOrder</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getBirthOrder() {
        return birthOrder;
    }

    /**
     * <p>Setter for the field <code>birthOrder</code>.</p>
     *
     * @param birthOrder a {@link java.lang.Integer} object.
     */
    public void setBirthOrder(Integer birthOrder) {
        this.birthOrder = birthOrder;
    }

    /**
     * <p>Getter for the field <code>lastUpdateFacility</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLastUpdateFacility() {
        return lastUpdateFacility;
    }

    /**
     * <p>Setter for the field <code>lastUpdateFacility</code>.</p>
     *
     * @param lastUpdateFacility a {@link java.lang.String} object.
     */
    public void setLastUpdateFacility(String lastUpdateFacility) {
        this.lastUpdateFacility = lastUpdateFacility;
    }

    /**
     * <p>Getter for the field <code>multipleBirthIndicator</code>.</p>
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean getMultipleBirthIndicator() {
        return multipleBirthIndicator;
    }

    /**
     * <p>Setter for the field <code>multipleBirthIndicator</code>.</p>
     *
     * @param multipleBirthIndicator a {@link java.lang.Boolean} object.
     */
    public void setMultipleBirthIndicator(Boolean multipleBirthIndicator) {
        this.multipleBirthIndicator = multipleBirthIndicator;
    }

    /**
     * <p>Getter for the field <code>birthOrderAsString</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getBirthOrderAsString() {
        return birthOrderAsString;
    }

    /**
     * <p>Setter for the field <code>birthOrderAsString</code>.</p>
     *
     * @param birthOrderAsString a {@link java.lang.String} object.
     */
    public void setBirthOrderAsString(String birthOrderAsString) {
        this.birthOrderAsString = birthOrderAsString;
    }

    /**
     * <p>Getter for the field <code>multipleBirthIndicatorAsString</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getMultipleBirthIndicatorAsString() {
        return multipleBirthIndicatorAsString;
    }

    /**
     * <p>Setter for the field <code>multipleBirthIndicatorAsString</code>.</p>
     *
     * @param multipleBirthIndicatorAsString a {@link java.lang.String} object.
     */
    public void setMultipleBirthIndicatorAsString(String multipleBirthIndicatorAsString) {
        this.multipleBirthIndicatorAsString = multipleBirthIndicatorAsString;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object p) {
        if (p == null) {
            return false;
        }
        if (!(p instanceof PatientDB)) {
            return false;
        }
        PatientDB patient = (PatientDB) p;
        if (getId() == null || patient.getId() == null) {
            return fakeId == patient.fakeId;
        } else {
            return super.equals(patient);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        if (getId() == null) {
            final int prime = 31;
            int result = 1;
            result = (prime * result) + fakeId;
            return result;
        } else {
            return super.hashCode();
        }
    }

    /**
     * <p>addPhoneNumber.</p>
     *
     * @param phoneNumber a {@link PatientPhoneNumber} object.
     */
    public void addPhoneNumber(PatientPhoneNumber phoneNumber) {
        if (this.phoneNumbers == null) {
            this.phoneNumbers = new ArrayList<PatientPhoneNumber>();
        }
        this.phoneNumbers.add(phoneNumber);
    }

    /**
     * <p>Getter for the field <code>personalRelationships</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<Person> getPersonalRelationships() {
        if (this.personalRelationships == null) {
            this.personalRelationships = new ArrayList<Person>();
        }
        return personalRelationships;
    }

    /**
     * <p>Setter for the field <code>personalRelationships</code>.</p>
     *
     * @param personalRelationships a {@link java.util.List} object.
     */
    public void setPersonalRelationships(List<Person> personalRelationships) {
        this.personalRelationships = personalRelationships;
    }

    /**
     * <p>addPersonalRelationship.</p>
     *
     * @param inPerson a {@link Person} object.
     */
    public void addPersonalRelationship(Person inPerson) {
        this.getPersonalRelationships().add(inPerson);
    }

    /**
     * <p>removePersonalRelationship.</p>
     *
     * @param inPerson a {@link Person} object.
     */
    public void removePersonalRelationship(Person inPerson) {
        this.getPersonalRelationships().remove(inPerson);
    }

    public String getBirthPlaceName() {
        return birthPlaceName;
    }

    public void setBirthPlaceName(String birthPlaceName) {
        this.birthPlaceName = birthPlaceName;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
