package net.ihe.gazelle.app.patientregistry.db.patient;

/**
 * This enumeration is used to specified for which kind of identifier this HD is used
 *
 * @author aberge
 *
 */
public enum DesignatorType {
    VISIT_NUMBER("VN", "Visit number", "visit_assigning_authority"),
    PATIENT_ID("PI", "Patient identifier", "patient_assigning_authority"),
    ACCOUNT_NUMBER("AN", "Account number", "account_assigning_authority"),
    MOVEMENT_ID("MOV", "Movement identifier", "movement_assigning_authority"),
    FILTERED_DOMAIN("FILTERED", "What domains returned in queries", "none");

    private String friendlyName;
    private String identifierTypeCode;
    private String appPreference;

    DesignatorType(String value, String name, String app_preference) {
        this.identifierTypeCode = value;
        this.appPreference = app_preference;
        this.friendlyName = name;
    }

    public static DesignatorType getDesignatorTypeByValue(String value){
        for (DesignatorType dt: values()){
            if (dt.getIdentifierTypeCode().equals(value)){
                return dt;
            }
        }
        return null;
    }

    public String getIdentifierTypeCode() {
        return identifierTypeCode;
    }

    public String getAppPreference() {
        return appPreference;
    }

    public String getFriendlyName() {
        return friendlyName;
    }


}
