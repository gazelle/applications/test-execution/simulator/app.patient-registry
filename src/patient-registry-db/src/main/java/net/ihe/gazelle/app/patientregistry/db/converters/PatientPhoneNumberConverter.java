/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.db.converters;

import net.ihe.gazelle.app.patientregistry.business.patient.ContactPoint;
import net.ihe.gazelle.app.patientregistry.business.patient.ContactPointType;
import net.ihe.gazelle.app.patientregistry.business.patient.ContactPointUse;
import net.ihe.gazelle.app.patientregistry.business.patient.Patient;
import net.ihe.gazelle.app.patientregistry.db.patient.PatientPhoneNumber;
import net.ihe.gazelle.app.patientregistry.db.patient.PhoneNumberType;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * <p>PatientPhoneNumberConverter class.</p>
 *
 * @author abe
 * @version 1.0: 18/12/2019
 */

public class PatientPhoneNumberConverter {

    public static Set<ContactPoint> toContactPointSet(List<PatientPhoneNumber> phoneNumbers) {
        Set<ContactPoint> contactPoints = new HashSet<ContactPoint>();
        if (phoneNumbers != null && !phoneNumbers.isEmpty()){
            for (PatientPhoneNumber phoneNumber: phoneNumbers){
                contactPoints.add(toContactPoint(phoneNumber));
            }
        }
        return contactPoints;
    }

    private static ContactPoint toContactPoint(PatientPhoneNumber phoneNumber) {
        return new ContactPoint(phoneNumber.getValue(), toContactPointType(phoneNumber.getType()),
                toContactPointUse(phoneNumber.getType()));
    }

    private static ContactPointType toContactPointType(PhoneNumberType type) {
        if (type != null) {
            switch (type) {
                case MOBILE:
                    return ContactPointType.MOBILE;
                case BEEPER:
                    return ContactPointType.BEEPER;
                default:
                    return ContactPointType.PHONE;
            }
        } else {
            return ContactPointType.PHONE;
        }
    }

    private static ContactPointUse toContactPointUse(PhoneNumberType type) {
        if (type != null) {
            switch (type) {
                case MOBILE:
                    return ContactPointUse.MOBILE;
                case HOME:
                    return ContactPointUse.PRIMARY_HOME;
                case BEEPER:
                    return ContactPointUse.BEEPER;
                case OTHER:
                    return ContactPointUse.OTHER;
                case EMERGENCY:
                    return ContactPointUse.EMERGENCY;
                case VACATION:
                    return ContactPointUse.TEMPORARY;
                case WORK:
                    return ContactPointUse.WORK;
                default:
                    return ContactPointUse.HOME;
            }
        } else {
            return ContactPointUse.HOME;
        }
    }
}
