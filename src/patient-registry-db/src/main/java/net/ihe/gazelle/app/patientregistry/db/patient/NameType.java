package net.ihe.gazelle.app.patientregistry.db.patient;

/**
 * <p>NameType enum.</p>
 *
 * @author abe
 * @version 1.0: 08/06/18
 */
public enum NameType {

    OFFICIAL,
    TEMP,
    NICKNAME,
    ANONYMOUS,
    MAIDEN,
    NULL
}
