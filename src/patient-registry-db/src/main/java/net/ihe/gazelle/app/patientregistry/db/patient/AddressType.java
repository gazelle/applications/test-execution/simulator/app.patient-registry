package net.ihe.gazelle.app.patientregistry.db.patient;

/**
 * <b>Class Description : </b>AddressType<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 09/11/16
 * @since 2.2.0
 */
public enum AddressType {

    BAD_ADDRESS("BA", "Bad address"), //0
    BIRTH("N", "Birth (nee)"), //1
    BDL("BDL", "Birth Delivery Location"), //2
    ORIGIN("F", "Country of origin"),//3
    CURRENT("C", "Current or Temporary"), //4
    BUSINESS("B", "Firm/Buisiness"), //5
    HOME("H", "Home"), //6
    LEGAL("L", "Legal address"), //7
    MAILING("M", "Mailing address"), //8
    OFFICE("O", "Office"), //9
    PERMANENT("P", "Permanent"), //10
    REGISTRY_HOME("RH", "Registry home"), //11
    BIRTH_RESIDENCE("BR", "Residence at birth"); //12

    String hl7Code;
    String label;

    AddressType(String hl7Code, String label){
        this.hl7Code = hl7Code;
        this.label = label;
    }

    /**
     * <p>Getter for the field <code>hl7Code</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getHl7Code() {
        return hl7Code;
    }

    /**
     * <p>Getter for the field <code>label</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLabel() {
        return label;
    }
}
