/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.db.adapters;

import net.ihe.gazelle.app.patientregistry.application.search.HierarchicDesignatorDAO;
import net.ihe.gazelle.app.patientregistry.db.patient.HierarchicDesignator;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * <p>HierarchicDesignatorDAO class.</p>
 *
 * @author abe
 * @version 1.0: 03/12/2019
 */

public class HierarchicDesignatorDAOImpl implements HierarchicDesignatorDAO {

    private EntityManager entityManager;

    public HierarchicDesignatorDAOImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public boolean isDomainKnown(String domainUniversalServiceId) {
        List<HierarchicDesignator> resultList = findHierarchicDesignatorsByUniversalId(domainUniversalServiceId);
        return (resultList != null && !resultList.isEmpty());
    }

    List<HierarchicDesignator> findHierarchicDesignatorsByUniversalId(String universalID) {
        TypedQuery<HierarchicDesignator> query = entityManager
              .createNamedQuery("HierarchicDesignator.findByUniversalID", HierarchicDesignator.class);
        query.setParameter("universalID", universalID);
        return query.getResultList();
    }

}
