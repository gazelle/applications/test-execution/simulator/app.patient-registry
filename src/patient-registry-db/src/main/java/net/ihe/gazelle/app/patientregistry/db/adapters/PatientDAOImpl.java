/**
 * Copyright 2019 IHE International (http://www.ihe.net) Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required by
 * applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.db.adapters;

import net.ihe.gazelle.app.patientregistry.application.search.PatientDAO;
import net.ihe.gazelle.app.patientregistry.business.exceptions.UnknownRequestedDomainException;
import net.ihe.gazelle.app.patientregistry.business.patient.Address;
import net.ihe.gazelle.app.patientregistry.business.patient.AddressUse;
import net.ihe.gazelle.app.patientregistry.business.patient.ContactPoint;
import net.ihe.gazelle.app.patientregistry.business.patient.ContactPointType;
import net.ihe.gazelle.app.patientregistry.business.patient.ContactPointUse;
import net.ihe.gazelle.app.patientregistry.business.patient.Patient;
import net.ihe.gazelle.app.patientregistry.business.patient.PersonName;
import net.ihe.gazelle.app.patientregistry.business.search.DateInterval;
import net.ihe.gazelle.app.patientregistry.business.search.GenderCode;
import net.ihe.gazelle.app.patientregistry.business.search.IdentifierSearchCriteria;
import net.ihe.gazelle.app.patientregistry.business.search.PatientSearchCriteria;
import net.ihe.gazelle.app.patientregistry.db.converters.PatientDBConverter;
import net.ihe.gazelle.app.patientregistry.db.patient.AddressType;
import net.ihe.gazelle.app.patientregistry.db.patient.GenderCodeDB;
import net.ihe.gazelle.app.patientregistry.db.patient.PatientAddress;
import net.ihe.gazelle.app.patientregistry.db.patient.PatientDB;
import net.ihe.gazelle.app.patientregistry.db.patient.PatientIdentifierDB;
import net.ihe.gazelle.app.patientregistry.db.patient.PatientPhoneNumber;
import net.ihe.gazelle.app.patientregistry.db.patient.PhoneNumberType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Named;
import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * <p>PatientDAO class.</p>
 *
 * @author abe
 * @version 1.0: 03/12/2019
 */


@Named("patientDAO")
public class PatientDAOImpl implements PatientDAO {

   private static final Logger LOG = LoggerFactory.getLogger(PatientDAOImpl.class);

   private static final char WILD_CARD = '*';
   private static final int FIRST_OCCURENCE = 0;
   private static final int SECOND_OCCURENCE = 1;
   private static final int THIRD_OCCURENCE = 2;

   @PersistenceContext(unitName = "patientRegistryPU")
   private EntityManager entityManager;

   private CriteriaBuilder criteriaBuilder;
   private Root<PatientDB> patientDBRoot;

   public PatientDAOImpl(EntityManager entityManager) throws IllegalArgumentException {
      if (entityManager == null) {
         throw new IllegalArgumentException("EntityManager cannot be null");
      }
      this.entityManager = entityManager;
      this.criteriaBuilder = entityManager.getCriteriaBuilder();
   }

   public PatientDAOImpl() {
      if (entityManager != null) {
         this.criteriaBuilder = entityManager.getCriteriaBuilder();
      } else {
         EntityManagerFactory entityManagerFactory = Persistence
               .createEntityManagerFactory("patientRegistryPU");
         if (entityManagerFactory != null) {
            this.entityManager = entityManagerFactory.createEntityManager();
            this.criteriaBuilder = entityManager.getCriteriaBuilder();
         }
      }
   }

   //TODO Handle thrown exception
   public Patient getPatientByUUID(String uuid) throws PersistenceException {
      PatientDBConverter converter = new PatientDBConverter(entityManager);
      try {
         return converter.toPatient(getPatientDBByUUID(uuid), null);
      } catch (NoResultException e) {
         return null;
      }
   }

   PatientDB getPatientDBByUUID(String uuid) throws PersistenceException {
      TypedQuery<PatientDB> query = entityManager.createNamedQuery("PatientDB.findByUuid", PatientDB.class);
      query.setParameter("uuid", uuid);
      return query.getSingleResult();
   }


   /**
    * 3.21.4.1.3.2 The Supplier shall return at least all exact matches to the query parameters sent by the Consumer;
    *
    * @param patientCriteria
    * @param restrictedDomains
    *
    * @return
    *
    * @throws UnknownRequestedDomainException
    */
   public List<Patient> findPatients(PatientSearchCriteria patientCriteria, List<String> restrictedDomains)
         throws UnknownRequestedDomainException {
      if (restrictedDomains != null && !restrictedDomains.isEmpty()) {
         checkDomainsAreAllKnown(restrictedDomains);
      }
      CriteriaQuery<PatientDB> criteriaQuery = criteriaBuilder.createQuery(PatientDB.class);
      patientDBRoot = criteriaQuery.from(PatientDB.class);
      List<Predicate> andPredicates = new ArrayList<Predicate>();
      // restrict the search to patients owned by PDS or defined for test session (CONNECTATHON), active and not used as test data
      addEqualRestrictionIfValued(patientDBRoot, andPredicates, "stillActive", true);
      addEqualRestrictionIfValued(patientDBRoot, andPredicates, "testData", false);

      // use criteria
      if (patientCriteria.getPersonNames() != null && !patientCriteria.getPersonNames().isEmpty()) {
         List<Predicate> defaultNamePredicates = new ArrayList<Predicate>();
         List<Predicate> alternateNamePredicates = new ArrayList<Predicate>();
         if (patientCriteria.getPersonNames().size() > 0) {
            // use the first iteration to match on first name/second/third
            PersonName personName = patientCriteria.getPersonNames().get(FIRST_OCCURENCE);
            final List<String> givens = personName.getGivens();
            if (givens != null && !givens.isEmpty()) {
               Iterator<String> givenIterator = givens.iterator();
               if (givenIterator.hasNext()) {
                  addLikeRestrictionIfValued(patientDBRoot, defaultNamePredicates, "firstName", givenIterator.next());
               }
               if (givenIterator.hasNext()) {
                  addLikeRestrictionIfValued(patientDBRoot, defaultNamePredicates, "secondName", givenIterator.next());
               }
               if (givenIterator.hasNext()) {
                  addLikeRestrictionIfValued(patientDBRoot, defaultNamePredicates, "thirdName", givenIterator.next());
               }
            }
            addLikeRestrictionIfValued(patientDBRoot, defaultNamePredicates, "lastName", personName.getFamily());
         }
         if (patientCriteria.getPersonNames().size() > 1) {
            // use the second iteration to match on alternate name/second/third
            PersonName alternatePersonName = patientCriteria.getPersonNames().get(SECOND_OCCURENCE);
            final List<String> alternateGivens = alternatePersonName.getGivens();
            if (alternateGivens != null && !alternateGivens.isEmpty()) {
               Iterator<String> alternateGivenIterator = alternateGivens.iterator();
               if (alternateGivenIterator.hasNext()) {
                  addLikeRestrictionIfValued(patientDBRoot, alternateNamePredicates, "alternateFirstName", alternateGivenIterator.next());
               }
               if (alternateGivenIterator.hasNext()) {
                  addLikeRestrictionIfValued(patientDBRoot, alternateNamePredicates, "alternateSecondName", alternateGivenIterator.next());
               }
               if (alternateGivenIterator.hasNext()) {
                  addLikeRestrictionIfValued(patientDBRoot, alternateNamePredicates, "alternateThirdName", alternateGivenIterator.next());
               }
            }
            addLikeRestrictionIfValued(patientDBRoot, alternateNamePredicates, "alternateLastName", alternatePersonName.getFamily());
         }
         if (alternateNamePredicates.isEmpty()) {
            // will be added to the final AND clause
            andPredicates.addAll(defaultNamePredicates);
         } else {
            // default name OR alternate name
            Predicate andDefaultName = criteriaBuilder.and((Predicate[]) defaultNamePredicates.toArray(new Predicate[defaultNamePredicates.size()]));
            Predicate andAlternateName = criteriaBuilder
                  .and((Predicate[]) alternateNamePredicates.toArray(new Predicate[alternateNamePredicates.size()]));
            Predicate patientNamePredicate = criteriaBuilder.or(andAlternateName, andDefaultName);
            andPredicates.add(patientNamePredicate);
         }
      }

      addLikeRestrictionIfValued(patientDBRoot, andPredicates, "motherMaidenName", patientCriteria.getMothersMaidenName());
      addDateRestrictionIfValued(andPredicates, "dateOfBirth", patientCriteria.getDateOfBirth());
      addEqualRestrictionIfValued(patientDBRoot, andPredicates, "genderCode", patientCriteria.getGenderCode());
      queryOnPatientIdentifiers(andPredicates, patientCriteria.getIdentifiers());
      queryOnAddresses(criteriaQuery, andPredicates, patientCriteria.getAddresses());
      queryOnContactPoints(criteriaQuery, andPredicates, patientCriteria.getContactPoints());

      Predicate finalPredicate = criteriaBuilder.and(andPredicates.toArray(new Predicate[andPredicates.size()]));
      TypedQuery<PatientDB> query = entityManager.createQuery(criteriaQuery.select(patientDBRoot).where(finalPredicate));
      List<PatientDB> result = query.getResultList();
      return convertToPatientList(result, restrictedDomains);
   }

   private void addEqualRestrictionIfValued(From queryRoot, List<Predicate> predicates, String property, boolean value) {
      Path<Boolean> propertyPath = queryRoot.get(property);
      if (value) {
         predicates.add(criteriaBuilder.isTrue(propertyPath));
      } else {
         predicates.add(criteriaBuilder.isFalse(propertyPath));
      }
   }

   private void checkDomainsAreAllKnown(List<String> restrictedDomains) throws UnknownRequestedDomainException {
      List<String> unknownDomains = new ArrayList<String>();
      HierarchicDesignatorDAOImpl hierarchicDesignatorDAO = new HierarchicDesignatorDAOImpl(entityManager);
      for (String domainId : restrictedDomains) {
         if (!hierarchicDesignatorDAO.isDomainKnown(domainId)) {
            unknownDomains.add(domainId);
         }
      }
      if (!unknownDomains.isEmpty()) {
         throw new UnknownRequestedDomainException(unknownDomains);
      }
   }

   private List<Patient> convertToPatientList(List<PatientDB> patientDBList, List<String> restrictedDomains) {
      PatientDBConverter converter = new PatientDBConverter(entityManager);
      return converter.toPatientList(patientDBList, restrictedDomains);
   }

   private void queryOnContactPoints(CriteriaQuery<PatientDB> criteriaQuery, List<Predicate> predicates, List<ContactPoint> contactPoints) {
      List<Integer> matchingPhoneNumbers = new ArrayList<Integer>();
      Metamodel metamodel = entityManager.getMetamodel();
      EntityType<PatientDB> patientMetamodel = metamodel.entity(PatientDB.class);
      if (contactPoints != null && !contactPoints.isEmpty()) {
         Join<PatientDB, PatientPhoneNumber> joinPhoneNumber = patientDBRoot.join(patientMetamodel.getList("phoneNumbers",
               PatientPhoneNumber.class), JoinType.LEFT);
         List<Predicate> orPredicates = new ArrayList<Predicate>();
         for (ContactPoint contactPoint : contactPoints) {
            if (ContactPointType.EMAIL.equals(contactPoint.getType())) {
               addEqualRestrictionIfValued(patientDBRoot, predicates, "email", contactPoint.getValue());
            } else {
               buildSinglePhoneNumberPredicate(joinPhoneNumber, orPredicates, contactPoint);
            }
         }
         if (!orPredicates.isEmpty()) {
            Subquery phoneSubquery = criteriaQuery.subquery(PatientPhoneNumber.class);
            Root<PatientPhoneNumber> phoneNumberRoot = phoneSubquery.from(PatientPhoneNumber.class);
            phoneSubquery.select(phoneNumberRoot.get("id"));
            phoneSubquery.where(buildOrPredicate(orPredicates));
            predicates.add(joinPhoneNumber.get("id").in(phoneSubquery));
         }
      }
   }

   private void buildSinglePhoneNumberPredicate(Join<PatientDB, PatientPhoneNumber> joinPhoneNumber, List<Predicate> predicates,
                                                ContactPoint contactPoint) {
      List<Predicate> phonePredicates = new ArrayList<Predicate>();
      addEqualRestrictionIfValued(joinPhoneNumber, phonePredicates, "value", contactPoint.getValue());
      // currently contactPoint.use refers to the same value set as patientPhoneNumber.type, contactPoint.type is not used yet
      addEqualRestrictionIfValued(joinPhoneNumber, phonePredicates, "type", contactPoint.getUse());
      Predicate andClause = buildAndPredicate(phonePredicates);
      predicates.add(andClause);
   }

   private void addEqualRestrictionIfValued(From queryRoot, List<Predicate> predicates, String property, ContactPointUse value) {
      if (value != null) {
         Path<PhoneNumberType> propertyPath = queryRoot.get(property);
         // map ContactPointUse and PatientPhoneType
         PhoneNumberType phoneNumberType;
         switch (value) {
            case HOME:
               phoneNumberType = PhoneNumberType.HOME;
               break;
            case BEEPER:
               phoneNumberType = PhoneNumberType.BEEPER;
               break;
            case MOBILE:
               phoneNumberType = PhoneNumberType.MOBILE;
               break;
            case EMERGENCY:
               phoneNumberType = PhoneNumberType.EMERGENCY;
               break;
            case TEMPORARY:
               phoneNumberType = PhoneNumberType.VACATION;
               break;
            default:
               phoneNumberType = PhoneNumberType.OTHER;
         }
         predicates.add(criteriaBuilder.equal(propertyPath, phoneNumberType));
      }
   }

   private void queryOnAddresses(CriteriaQuery<PatientDB> criteriaQuery, List<Predicate> predicates, List<Address> addresses) {
      List<Predicate> orPredicates = new ArrayList<Predicate>();
      Metamodel metamodel = entityManager.getMetamodel();
      EntityType<PatientDB> patientMetamodel = metamodel.entity(PatientDB.class);


      if (addresses != null && !addresses.isEmpty()) {
         Join<PatientDB, PatientAddress> joinPatientAddress = patientDBRoot
               .join(patientMetamodel.getList("addressList", PatientAddress.class), JoinType.LEFT);
         for (Address address : addresses) {
            buildSingleAddressPredicate(joinPatientAddress, orPredicates, address);
         }
         if (!orPredicates.isEmpty()) {
            Subquery addressSubquery = criteriaQuery.subquery(PatientAddress.class);
            Root<PatientAddress> addressRoot = addressSubquery.from(PatientAddress.class);
            addressSubquery.select(addressRoot.get("id"));
            addressSubquery.where(buildOrPredicate(orPredicates));
            predicates.add(joinPatientAddress.get("id").in(addressSubquery));
         }
      }
   }

   private Predicate buildOrPredicate(List<Predicate> orPredicates) {
      if (orPredicates.size() > 1) {
         return criteriaBuilder.or(orPredicates.toArray(new Predicate[orPredicates.size()]));
      } else {
         return orPredicates.get(FIRST_OCCURENCE);
      }
   }

   private void buildSingleAddressPredicate(Join<PatientDB, PatientAddress> joinPatientAddress, List<Predicate> orPredicates, Address address) {
      List<Predicate> predicates = new ArrayList<Predicate>();
      addLikeRestrictionIfValued(joinPatientAddress, predicates, "city", address.getCity());
      addLikeRestrictionIfValued(joinPatientAddress, predicates, "countryCode", address.getCountryIso3());
      addLikeRestrictionIfValued(joinPatientAddress, predicates, "zipCode", address.getPostalCode());
      addLikeRestrictionIfValued(joinPatientAddress, predicates, "state", address.getState());
      if (address.getLines() != null && !address.getLines().isEmpty()) {
         Iterator<String> lineIterator = address.getLines().iterator();
         while (lineIterator.hasNext()) {
            addLikeRestrictionIfValued(joinPatientAddress, predicates, "addressLine", lineIterator.next());
         }
      }
      addEqualRestrictionIfValued(joinPatientAddress, predicates, "addressType", address.getUse());
      Predicate andClause = buildAndPredicate(predicates);
      orPredicates.add(andClause);
   }

   private Predicate buildAndPredicate(List<Predicate> predicatesToAdd) {
      if (predicatesToAdd.size() > 1) {
         return criteriaBuilder.and(predicatesToAdd.toArray(new Predicate[predicatesToAdd.size()]));
      } else {
         return predicatesToAdd.get(FIRST_OCCURENCE);
      }
   }

   private void addEqualRestrictionIfValued(From queryRoot, List<Predicate> predicates, String property, AddressUse use) {
      if (use != null) {
         Path<AddressType> propertyPath = queryRoot.get(property);
         // map address use on address type
         AddressType addressType;
         switch (use) {
            case WORK:
               addressType = AddressType.BUSINESS;
               break;
            case LEGAL:
               addressType = AddressType.LEGAL;
               break;
            case TEMPORARY:
               addressType = AddressType.CURRENT;
               break;
            case BAD:
               addressType = AddressType.BAD_ADDRESS;
               break;
            case BIRTH_PLACE:
               addressType = AddressType.BIRTH;
               break;
            case BIRTH_RESIDENCE:
               addressType = AddressType.BIRTH_RESIDENCE;
               break;
            case BIRTH_DELIVERY:
               addressType = AddressType.BDL;
               break;
            default:
               addressType = AddressType.HOME;
         }
         predicates.add(criteriaBuilder.equal(propertyPath, addressType));
      }
   }

   private void queryOnPatientIdentifiers(List<Predicate> predicates, List<IdentifierSearchCriteria> identifiers) {
      if (identifiers != null && !identifiers.isEmpty()) {
         Join<PatientDB, PatientIdentifierDB> patientIdentifiers = patientDBRoot.joinList("patientIdentifiers").join("domain");
         Path<List<PatientIdentifierDB>> idValuePath = patientDBRoot.joinList("patientIdentifiers").get("idNumber");
         Path<String> universalIdPath = patientDBRoot.joinList("patientIdentifiers").get("domain").get("universalID");
         for (IdentifierSearchCriteria patientIdentifier : identifiers) {
            Predicate domainPredicate = null;
            Predicate valuePredicate = null;
            if (patientIdentifier.getSystemIdentifier() != null) {
               domainPredicate = criteriaBuilder.equal(universalIdPath, patientIdentifier.getSystemIdentifier());
            }
            if (patientIdentifier.getValue() != null) {
               valuePredicate = criteriaBuilder.equal(idValuePath, patientIdentifier.getValue());
            }
            if (domainPredicate != null && valuePredicate != null) {
               predicates.add(criteriaBuilder.and(domainPredicate, valuePredicate));
            } else if (domainPredicate == null) {
               predicates.add(valuePredicate);
            } else {
               predicates.add(domainPredicate);
            }
         }
      }
   }

   private void addEqualRestrictionIfValued(From queryRoot, List<Predicate> predicates, String property, String value) {
      if (value != null) {
         Path<String> propertyPath = queryRoot.get(property);
         predicates.add(criteriaBuilder.equal(criteriaBuilder.lower(propertyPath), formatValue(value).toLowerCase()));
      }
   }

   private void addEqualRestrictionIfValued(From queryRoot, List<Predicate> predicates, String property, GenderCode value) {
      if (value != null) {
         Path<String> propertyPath = queryRoot.get(property);
         predicates.add(criteriaBuilder.equal(propertyPath, GenderCodeDB.fromGenderCode(value.toPatientGenderCode())));
      }
   }

   private void addLikeRestrictionIfValued(From queryRoot, List<Predicate> predicates, String property, String value) {
      if (value != null && !value.isEmpty()) {
         Path<String> parameter = queryRoot.get(property);
         predicates.add(criteriaBuilder.like(criteriaBuilder.lower(parameter), formatValue(value).toLowerCase()));
      }
   }

   private void addDateRestrictionIfValued(List<Predicate> predicates, String property, DateInterval dateInterval) {
      Path<Date> propertyPath = patientDBRoot.get(property);
      if (dateInterval != null) {
         final Date exactDate = dateInterval.getExactDate();
         if (exactDate != null) {
            Calendar begin = Calendar.getInstance();
            begin.setTime(exactDate);
            begin.set(Calendar.HOUR_OF_DAY, 0);
            begin.set(Calendar.MINUTE, 0);
            begin.set(Calendar.SECOND, 0);
            Calendar end = Calendar.getInstance();
            end.setTime(exactDate);
            end.set(Calendar.HOUR_OF_DAY, 23);
            end.set(Calendar.MINUTE, 59);
            end.set(Calendar.SECOND, 59);
            Predicate beginPredicate = criteriaBuilder.greaterThanOrEqualTo(propertyPath, begin.getTime());
            Predicate endPredicate = criteriaBuilder.lessThanOrEqualTo(propertyPath, end.getTime());
            predicates.add(criteriaBuilder.and(beginPredicate, endPredicate));
         } else {
            final Date highLimit = dateInterval.getHighLimit();
            final Date lowLimit = dateInterval.getLowLimit();
            if (highLimit != null) {
               if (dateInterval.isHighLimitInclusive()) {
                  predicates.add(criteriaBuilder.lessThanOrEqualTo(propertyPath, highLimit));
               } else {
                  predicates.add(criteriaBuilder.lessThan(propertyPath, highLimit));
               }
            }
            if (lowLimit != null) {
               if (dateInterval.isLowLimitInclusive()) {
                  predicates.add(criteriaBuilder.greaterThanOrEqualTo(propertyPath, lowLimit));
               } else {
                  predicates.add(criteriaBuilder.lessThan(propertyPath, highLimit));
               }
            }
         }
      }
   }


   private String formatValue(final String param) {
      String value = null;
      if (param != null) {
         value = param.replace(WILD_CARD, '%');
      }
      return value;
   }
}
