package net.ihe.gazelle.app.patientregistry.db.patient;

/**
 * <b>Class Description : </b>LunarWeek<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 22/10/15
 */
public enum LunarWeek {

    FIRST_WEEK(31, "1.week"),
    SECOND_WEEK(32, "2.week"),
    THIRD_WEEK(33, "3.week"),
    FOURTH_WEEK(34, "4.week"),
    FIFTH_WEEK(35, "5.week");

    private Integer day;
    private String label;

    LunarWeek(Integer day, String label){
        this.day = day;
        this.label = label;
    }

    /**
     * <p>Getter for the field <code>day</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getDay() {
        return day;
    }

    /**
     * <p>Getter for the field <code>label</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLabel() {
        return label;
    }

    /**
     * <p>getWeekBasedOnDay.</p>
     *
     * @param day a {@link java.lang.Integer} object.
     * @return a {@link LunarWeek} object.
     */
    public static LunarWeek getWeekBasedOnDay(Integer day){
        for (LunarWeek week: values()){
            if (week.getDay().equals(day)){
                return week;
            }
            else {
                continue;
            }
        }
        return null;
    }
}
