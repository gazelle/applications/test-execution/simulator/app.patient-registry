package net.ihe.gazelle.app.patientregistry.db.patient;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by aberge on 03/07/17.
 *
 * @author aberge
 * @version $Id: $Id
 */

@Entity
@Table(name = "pam_person", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "pam_person_sequence", sequenceName = "pam_person_id_seq", allocationSize = 1)
public class Person implements Serializable {

    @Id
    @GeneratedValue(generator = "pam_person_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "second_name")
    private String secondName;

    @Column(name = "last_name")
    private String lastName;

    /**
     * populated using SVS Simulator (OID: 2.16.840.1.113883.12.63, keyword: RELATIONSHIP)
     */
    @Column(name = "relationship_code")
    private String relationshipCode;

    /**
     * populate using SVS Simulator (OID :2.16.840.1.113883.12.131, keyword: CONTACT_ROLE)
     */
    @Column(name = "contact_role_code")
    private String contactRoleCode;

    @Column(name = "identifier")
    private String identifier;


    @ManyToOne(targetEntity = PatientDB.class)
    @JoinColumn(name = "patient_id")
    private PatientDB patient;

    @OneToMany(mappedBy = "person", targetEntity = PersonPhoneNumber.class)
    // TODO @Cascade(CascadeType.ALL)
    private List<PersonPhoneNumber> phoneNumberList;

    @OneToMany(mappedBy = "person", targetEntity = PersonAddress.class)
    // TODO @Cascade(CascadeType.ALL)
    private List<PersonAddress> addressList;


    /**
     * <p>Constructor for Person.</p>
     */
    public Person() {

    }

    /**
     * <p>Constructor for Person.</p>
     *
     * @param inPatient a {@link PatientDB} object.
     */
    public Person(PatientDB inPatient) {
        this.patient = inPatient;
    }

    /**
     * <p>Constructor for Person.</p>
     *
     */
    public Person(PersonalRelationshipRoleType relationshipRole) {
        this.relationshipCode = relationshipRole.name();
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>firstName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * <p>Setter for the field <code>firstName</code>.</p>
     *
     * @param firstName a {@link java.lang.String} object.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * <p>Getter for the field <code>secondName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSecondName() {
        return secondName;
    }

    /**
     * <p>Setter for the field <code>secondName</code>.</p>
     *
     * @param secondName a {@link java.lang.String} object.
     */
    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    /**
     * <p>Getter for the field <code>lastName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * <p>Setter for the field <code>lastName</code>.</p>
     *
     * @param lastName a {@link java.lang.String} object.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * <p>Getter for the field <code>relationshipCode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getRelationshipCode() {
        return relationshipCode;
    }

    /**
     * <p>Setter for the field <code>relationshipCode</code>.</p>
     *
     * @param relationshipCode a {@link java.lang.String} object.
     */
    public void setRelationshipCode(String relationshipCode) {
        this.relationshipCode = relationshipCode;
    }

    /**
     * <p>Getter for the field <code>contactRoleCode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getContactRoleCode() {
        return contactRoleCode;
    }

    /**
     * <p>Setter for the field <code>contactRoleCode</code>.</p>
     *
     * @param contactRoleCode a {@link java.lang.String} object.
     */
    public void setContactRoleCode(String contactRoleCode) {
        this.contactRoleCode = contactRoleCode;
    }

    /**
     * <p>Getter for the field <code>identifier</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * <p>Setter for the field <code>identifier</code>.</p>
     *
     * @param identifier a {@link java.lang.String} object.
     */
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    /**
     * <p>Getter for the field <code>patient</code>.</p>
     *
     * @return a {@link PatientDB} object.
     */
    public PatientDB getPatient() {
        return patient;
    }

    /**
     * <p>Setter for the field <code>patient</code>.</p>
     *
     * @param patient a {@link PatientDB} object.
     */
    public void setPatient(PatientDB patient) {
        this.patient = patient;
    }

    /**
     * <p>Getter for the field <code>phoneNumberList</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<PersonPhoneNumber> getPhoneNumberList() {
        if (this.phoneNumberList == null){
            this.phoneNumberList = new ArrayList<PersonPhoneNumber>();
        }
        return phoneNumberList;
    }

    /**
     * <p>Setter for the field <code>phoneNumberList</code>.</p>
     *
     * @param phoneNumberList a {@link java.util.List} object.
     */
    public void setPhoneNumberList(List<PersonPhoneNumber> phoneNumberList) {
        this.phoneNumberList = phoneNumberList;
    }

    /**
     * <p>Getter for the field <code>addressList</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<PersonAddress> getAddressList() {
        if (this.addressList == null){
            this.addressList = new ArrayList<PersonAddress>();
        }
        return addressList;
    }

    /**
     * <p>Setter for the field <code>addressList</code>.</p>
     *
     * @param addressList a {@link java.util.List} object.
     */
    public void setAddressList(List<PersonAddress> addressList) {
        this.addressList = addressList;
    }

    /**
     * <p>addPhoneNumber.</p>
     *
     * @param inNumber a {@link PersonPhoneNumber} object.
     */
    public void addPhoneNumber(PersonPhoneNumber inNumber){
        this.getPhoneNumberList().add(inNumber);
    }

    /**
     * <p>removePhoneNumber.</p>
     *
     * @param inNumber a {@link PersonPhoneNumber} object.
     */
    public void removePhoneNumber(PersonPhoneNumber inNumber){
        this.getPhoneNumberList().remove(inNumber);
    }

    /**
     * <p>addPersonAddress.</p>
     *
     * @param inAddress a {@link PersonAddress} object.
     */
    public void addPersonAddress(PersonAddress inAddress) {
        this.getAddressList().add(inAddress);
    }

    /**
     * <p>removePersonAddress.</p>
     *
     * @param inAddress a {@link PersonAddress} object.
     */
    public void removePersonAddress(PersonAddress inAddress){
        this.getAddressList().remove(inAddress);
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Person)) {
            return false;
        }

        Person person = (Person) o;

        if (firstName != null ? !firstName.equals(person.firstName) : person.firstName != null) {
            return false;
        }
        if (secondName != null ? !secondName.equals(person.secondName) : person.secondName != null) {
            return false;
        }
        if (lastName != null ? !lastName.equals(person.lastName) : person.lastName != null) {
            return false;
        }
        if (relationshipCode != null ? !relationshipCode.equals(person.relationshipCode) : person.relationshipCode != null) {
            return false;
        }
        if (contactRoleCode != null ? !contactRoleCode.equals(person.contactRoleCode) : person.contactRoleCode != null) {
            return false;
        }
        return identifier != null ? identifier.equals(person.identifier) : person.identifier == null;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + (secondName != null ? secondName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (relationshipCode != null ? relationshipCode.hashCode() : 0);
        result = 31 * result + (contactRoleCode != null ? contactRoleCode.hashCode() : 0);
        result = 31 * result + (identifier != null ? identifier.hashCode() : 0);
        return result;
    }

    /**
     * <p>isDefined.</p>
     *
     * @return a boolean.
     */
    public boolean isDefined() {
        return (this.relationshipCode != null && this.hashCode() != this.relationshipCode.hashCode()) || this.hashCode() != 0;
    }

}
