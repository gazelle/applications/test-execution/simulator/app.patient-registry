package net.ihe.gazelle.app.patientregistry.db.patient;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by aberge on 03/07/17.
 *
 * @author aberge
 * @version $Id: $Id
 */
@Entity
@DiscriminatorValue("person_address")
public class PersonAddress extends AbstractAddress {

    @ManyToOne(targetEntity = Person.class)
    @JoinColumn(name = "person_id")
    private Person person;

    /**
     * <p>Constructor for PersonAddress.</p>
     */
    public PersonAddress(){
        super();
    }

    /**
     * <p>Constructor for PersonAddress.</p>
     *
     * @param inPerson a {@link Person} object.
     */
    public PersonAddress(Person inPerson){
        super();
        this.person = inPerson;
    }

    /**
     * <p>Constructor for PersonAddress.</p>
     *
     * @param inPersonAddress a {@link PersonAddress} object.
     */
    public PersonAddress(PersonAddress inPersonAddress){
        super(inPersonAddress);
    }

    /**
     * <p>Getter for the field <code>person</code>.</p>
     *
     * @return a {@link Person} object.
     */
    public Person getPerson() {
        return person;
    }

    /**
     * <p>Setter for the field <code>person</code>.</p>
     *
     * @param person a {@link Person} object.
     */
    public void setPerson(Person person) {
        this.person = person;
    }
}
