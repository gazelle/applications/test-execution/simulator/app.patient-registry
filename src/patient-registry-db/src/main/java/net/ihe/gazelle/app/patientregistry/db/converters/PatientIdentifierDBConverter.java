/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.db.converters;

import net.ihe.gazelle.app.patientregistry.business.exceptions.MissingRequiredDataException;
import net.ihe.gazelle.app.patientregistry.business.patient.PatientIdentifier;
import net.ihe.gazelle.app.patientregistry.db.patient.PatientIdentifierDB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * <p>PatientIdentifierDBConverter class.</p>
 *
 * @author abe
 * @version 1.0: 18/12/2019
 */

public class PatientIdentifierDBConverter {

    private static final Logger LOG = LoggerFactory.getLogger(PatientIdentifierDBConverter.class);

    public static Set<PatientIdentifier> toPatientIdentifiers(List<PatientIdentifierDB> patientIdentifierDBs) {
        Set<PatientIdentifier> patientIdentifiers = new HashSet<PatientIdentifier>();
        if (patientIdentifierDBs != null && !patientIdentifierDBs.isEmpty()) {
            for (PatientIdentifierDB patientIdentifierDB : patientIdentifierDBs) {
                try {
                    if (patientIdentifierDB.getDomain() == null){
                        LOG.warn("Cannot convert patient identifier {} because domain is not filled in in database", patientIdentifierDB.getFullPatientId());
                    } else {
                        PatientIdentifier returnedPatientIdentifier = new PatientIdentifier(patientIdentifierDB.getIdNumber(),
                                patientIdentifierDB.getDomain().getUniversalID());
                        returnedPatientIdentifier.setSystemName(patientIdentifierDB.getDomain().getNamespaceID());
                        patientIdentifiers.add(returnedPatientIdentifier);
                    }
                } catch (MissingRequiredDataException e) {
                    LOG.warn("Cannot convert patient identifier {} because of the following cause {}", patientIdentifierDB.getFullPatientId(),
                            e.getMessage());
                }
            }
        }
        return patientIdentifiers;
    }
}
