/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.db.converters;

import net.ihe.gazelle.app.patientregistry.application.search.PatientAddressDAO;
import net.ihe.gazelle.app.patientregistry.application.search.PatientIdentifierDAO;
import net.ihe.gazelle.app.patientregistry.application.search.PatientPhoneNumberDAO;
import net.ihe.gazelle.app.patientregistry.business.exceptions.MissingRequiredDataException;
import net.ihe.gazelle.app.patientregistry.business.patient.Address;
import net.ihe.gazelle.app.patientregistry.business.patient.ContactPoint;
import net.ihe.gazelle.app.patientregistry.business.patient.ContactPointType;
import net.ihe.gazelle.app.patientregistry.business.patient.ContactPointUse;
import net.ihe.gazelle.app.patientregistry.business.patient.ObservationTypeEnum;
import net.ihe.gazelle.app.patientregistry.business.patient.Patient;
import net.ihe.gazelle.app.patientregistry.business.patient.PersonName;
import net.ihe.gazelle.app.patientregistry.db.adapters.PatientAddressDAOImpl;
import net.ihe.gazelle.app.patientregistry.db.adapters.PatientIdentifierDAOImpl;
import net.ihe.gazelle.app.patientregistry.db.adapters.PatientPhoneNumberDAOImpl;
import net.ihe.gazelle.app.patientregistry.db.patient.LunarWeek;
import net.ihe.gazelle.app.patientregistry.db.patient.PatientDB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * <p>PatientDBConverter class.</p>
 *
 * @author abe
 * @version 1.0: 04/12/2019
 */

public class PatientDBConverter {

    private EntityManager entityManager;

    private static final Logger LOG = LoggerFactory.getLogger(PatientDBConverter.class);

    public PatientDBConverter(EntityManager entityManager){
        this.entityManager = entityManager;
    }

    //TODO Why do we need restricted domains ? DB request should have take care of this
    public Patient toPatient(PatientDB patientDB, List<String> restrictedDomains) {
        Patient patient = new Patient();
        patient.setUuid(patientDB.getUuid());
        //If all entries in the list of patient identifiers are eliminated, which would leave PID-3
        //empty, then the corresponding PID segment group shall not be present in the response at all.
        addPatientIdentifiers(restrictedDomains, patient);
        setPersonNames(patientDB, patient);
        if(patientDB.getGenderCode() != null) {
            patient.setGender(patientDB.getGenderCode().toGenderCode());
        }
        patient.setDateOfBirth(patientDB.getDateOfBirth());
        patient.setMultipleBirthOrder(patientDB.getBirthOrder());
        patient.setDeathDateTime(patientDB.getPatientDeathTime());
        setPatientAddresses(patient);
        setPhoneNumbers(patient);
        if (patientDB.getEmail() != null && !patientDB.getEmail().isEmpty()) {
            patient.addContactPoint(new ContactPoint(patientDB.getEmail(), ContactPointType.EMAIL, ContactPointUse.EMAIL));
        }
        addObservationIfValued(patient, patientDB.getAccountNumber(), ObservationTypeEnum.ACCOUNT_NUMBER);
        addObservationIfValued(patient, patientDB.getMotherMaidenName(), ObservationTypeEnum.MOTHERS_MAINDEN_NAME);
        addObservationIfValued(patient, patientDB.getBirthPlaceName(), ObservationTypeEnum.BIRTHPLACE_NAME);
        addObservationIfValued(patient, patientDB.getBloodGroup(), ObservationTypeEnum.BLOOD_GROUP);
        addObservationIfValued(patient, patientDB.getReligionCode(), ObservationTypeEnum.RELIGION);
        addObservationIfValued(patient, patientDB.getRaceCode(), ObservationTypeEnum.RACE);
        return patient;
    }

    private void setPhoneNumbers(Patient patient) {
        PatientPhoneNumberDAO phoneNumberDAO = new PatientPhoneNumberDAOImpl(entityManager);
        Set<ContactPoint> contactPoints = phoneNumberDAO.getPhoneNumbersForPatient(patient);
        patient.setContactPoints(contactPoints);
    }

    private void setPatientAddresses(Patient patient) {
        PatientAddressDAO patientAddressDAO = new PatientAddressDAOImpl(entityManager);
        Set<Address> addresses = patientAddressDAO.getAddressesForPatient(patient);
        patient.setAddresses(addresses);
    }

    private void addObservationIfValued(Patient patient, boolean value, ObservationTypeEnum ObservationTypeEnum) {
        try {
            patient.addObservation(ObservationTypeEnum, Boolean.toString(value));
        } catch (MissingRequiredDataException e) {
            LOG.debug("Cannot add observation: {}", e.getMessage());
        }
    }

    private void addObservationIfValued(Patient patient, LunarWeek value, ObservationTypeEnum ObservationTypeEnum) {
        if (value != null) {
            try {
                patient.addObservation(ObservationTypeEnum, value.getDay().toString());
            } catch (MissingRequiredDataException e) {
                LOG.debug("Cannot add observation: {}", e.getMessage());
            }
        }
    }

    private void addObservationIfValued(Patient patient, String value,
                                        ObservationTypeEnum ObservationTypeEnum) {
        if (value != null && !value.isEmpty()) {
            try {
                patient.addObservation(ObservationTypeEnum, value);
            } catch (MissingRequiredDataException e) {
                LOG.debug("Cannot add observation: {}", e.getMessage());
            }
        }
    }

    private void addObservationIfValued(Patient patient, Integer value,
                                        ObservationTypeEnum ObservationTypeEnum) {
        if (value != null) {
            try {
                patient.addObservation(ObservationTypeEnum, value.toString());
            } catch (MissingRequiredDataException e) {
                LOG.debug("Cannot add observation: {}", e.getMessage());
            }
        }
    }

    private void setPersonNames(PatientDB patientDB, Patient patient) {
        if ((patientDB.getFirstName() != null && !patientDB.getFirstName().isEmpty())
                || (patientDB.getLastName() != null && !patientDB.getLastName().isEmpty())) {
            addPersonName(patient, "usual", patientDB.getLastName(), patientDB.getFirstName(), patientDB.getSecondName(),
                    patientDB.getThirdName());
            if ((patientDB.getAlternateFirstName() != null && !patientDB.getAlternateFirstName().isEmpty())
                    || (patientDB.getAlternateLastName() != null && !patientDB.getAlternateLastName().isEmpty())) {
                addPersonName(patient, "alternate", patientDB.getAlternateLastName(), patientDB.getAlternateFirstName(),
                        patientDB.getAlternateSecondName(), patientDB.getAlternateThirdName());
            }
        }
    }

    //TODO This should not be done here
    private void addPatientIdentifiers(List<String> restrictedDomains, Patient patient) {
        PatientIdentifierDAO patientIdentifierDAO = new PatientIdentifierDAOImpl(entityManager);
        patient.setIdentifiers(patientIdentifierDAO.getPatientIdentifiersForDomain(patient, restrictedDomains));
    }

    private void addPersonName(Patient patient, String usual, String lastName, String firstName, String secondName,
                               String thirdName) {
        PersonName personName = new PersonName();
        // TODO personName.setUse();
        personName.setFamily(lastName);
        List<String> givens = new ArrayList<>();
        givens.add(firstName);
        givens.add(secondName);
        givens.add(thirdName);
        personName.setGivens(givens);
        patient.addName(personName);
    }

    //TODO Why do we need restricted domains ? DB request should have take care of this
    public List<Patient> toPatientList(List<PatientDB> patientDBList, List<String> restrictedDomains) {
        if (patientDBList != null) {
            List<Patient> patients = new ArrayList<Patient>();
            for (PatientDB fromDbPatient : patientDBList) {
                Patient patient = toPatient(fromDbPatient, restrictedDomains);
                if (patient != null) {
                    patients.add(patient);
                }
            }
            return patients;
        } else {
            return null;
        }
    }
}

