/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.db.adapters;

import net.ihe.gazelle.app.patientregistry.application.search.PatientPhoneNumberDAO;
import net.ihe.gazelle.app.patientregistry.business.patient.ContactPoint;
import net.ihe.gazelle.app.patientregistry.business.patient.Patient;
import net.ihe.gazelle.app.patientregistry.db.converters.PatientPhoneNumberConverter;
import net.ihe.gazelle.app.patientregistry.db.patient.PatientDB;
import net.ihe.gazelle.app.patientregistry.db.patient.PatientPhoneNumber;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;
import java.util.List;
import java.util.Set;

/**
 * <p>PatientPhoneNumberDAO class.</p>
 *
 * @author abe
 * @version 1.0: 18/12/2019
 */
@Named("patientPhoneNumberDAO")
public class PatientPhoneNumberDAOImpl implements PatientPhoneNumberDAO {

    @PersistenceContext(unitName = "patientRegistryPU")
    EntityManager entityManager;

    public PatientPhoneNumberDAOImpl(EntityManager entityManager){
        this.entityManager = entityManager;
    }

    public PatientPhoneNumberDAOImpl(){

    }

    @Override
    public Set<ContactPoint> getPhoneNumbersForPatient(Patient patient){
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<PatientPhoneNumber> criteriaQuery = criteriaBuilder.createQuery(PatientPhoneNumber.class);
        Root<PatientPhoneNumber> root = criteriaQuery.from(PatientPhoneNumber.class);

        Metamodel metamodel = entityManager.getMetamodel();

        EntityType<PatientPhoneNumber> patientPhoneNumberMetadata = metamodel.entity(PatientPhoneNumber.class);
        Join<PatientPhoneNumber, PatientDB> joinPatient = root.join(patientPhoneNumberMetadata.getSingularAttribute("patient", PatientDB.class));
        Path<String> patientUuidPath = joinPatient.get("uuid");
        Predicate patientUuidPredicate = criteriaBuilder.equal(patientUuidPath, patient.getUuid());

        TypedQuery<PatientPhoneNumber> query = entityManager.createQuery(criteriaQuery.select(root).where(patientUuidPredicate));
        List<PatientPhoneNumber> phoneNumbers = query.getResultList();
        return PatientPhoneNumberConverter.toContactPointSet(phoneNumbers);
    }

}
