package net.ihe.gazelle.app.patientregistry.db.patient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * <b>Class Description : </b>PatientAddress<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 09/11/16
 *
 * @since 2.2.0
 */

@Entity
@DiscriminatorValue("patient_address")
public class PatientAddress extends AbstractAddress implements Serializable {

    @ManyToOne(targetEntity = PatientDB.class)
    @JoinColumn(name = "patient_id")
    private PatientDB patient;

    @Column(name = "is_main_address")
    private boolean mainAddress;


    /**
     * <p>Constructor for PatientAddress.</p>
     */
    public PatientAddress() {
        super();
        this.mainAddress = true;
    }

    /**
     * <p>Constructor for PatientAddress.</p>
     *
     * @param inPatientAddress a {@link PatientAddress} object.
     */
    public PatientAddress(PatientAddress inPatientAddress) {
        super(inPatientAddress);
        this.mainAddress = inPatientAddress.isMainAddress();
    }

    /**
     * <p>Constructor for PatientAddress.</p>
     *
     * @param addressType a {@link AddressType} object.
     */
    public PatientAddress(AddressType addressType) {
        super(addressType);
    }

    /**
     * <p>Constructor for PatientAddress.</p>
     *
     * @param inPatient a {@link AbstractPatient} object.
     */
    public PatientAddress(PatientDB inPatient) {
        super();
        this.patient = inPatient;
    }



    /**
     * <p>isMainAddress.</p>
     *
     * @return a boolean.
     */
    public boolean isMainAddress() {
        return mainAddress;
    }

    /**
     * <p>Setter for the field <code>mainAddress</code>.</p>
     *
     * @param mainAddress a boolean.
     */
    public void setMainAddress(boolean mainAddress) {
        this.mainAddress = mainAddress;
    }


    /**
     * <p>Getter for the field <code>patient</code>.</p>
     *
     * @return a {@link AbstractPatient} object.
     */
    public AbstractPatient getPatient() {
        return patient;
    }

    /**
     * <p>Setter for the field <code>patient</code>.</p>
     *
     * @param patient a {@link AbstractPatient} object.
     */
    public void setPatient(PatientDB patient) {
        this.patient = patient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PatientAddress)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        PatientAddress that = (PatientAddress) o;

        return mainAddress == that.mainAddress;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (mainAddress ? 1 : 0);
        return result;
    }
}
