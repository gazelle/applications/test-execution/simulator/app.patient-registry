package net.ihe.gazelle.app.patientregistry.db.patient;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by aberge on 09/03/17.
 */
@Entity
@DiscriminatorValue("person_phone_number")
public class PersonPhoneNumber extends AbstractPhoneNumber implements Serializable {



    @ManyToOne(targetEntity = Person.class)
    @JoinColumn(name = "person_id")
    private Person person;

    public PersonPhoneNumber(){
        super();
    }

    public PersonPhoneNumber(Person inPerson) {
        super();
        this.person = inPerson;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
