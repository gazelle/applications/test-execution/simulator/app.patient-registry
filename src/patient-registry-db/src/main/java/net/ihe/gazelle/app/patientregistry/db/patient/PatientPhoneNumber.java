package net.ihe.gazelle.app.patientregistry.db.patient;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by aberge on 09/03/17.
 *
 * @author aberge
 * @version $Id: $Id
 */
@Entity
@DiscriminatorValue("patient_phone_number")
public class PatientPhoneNumber extends AbstractPhoneNumber implements Serializable {



    @ManyToOne(targetEntity = PatientDB.class)
    @JoinColumn(name = "patient_id")
    private PatientDB patient;

    @Column(name = "is_principal")
    private boolean principal;

    /**
     * <p>Constructor for PatientPhoneNumber.</p>
     */
    public PatientPhoneNumber(){
        super();
        this.principal = true;
    }

    /**
     * <p>Constructor for PatientPhoneNumber.</p>
     *
     * @param inPatient a {@link PatientDB} object.
     */
    public PatientPhoneNumber(PatientDB inPatient) {
        super();
        this.principal = true;
        this.patient = inPatient;
    }

    /**
     * <p>Getter for the field <code>patient</code>.</p>
     *
     * @return a {@link PatientDB} object.
     */
    public PatientDB getPatient() {
        return patient;
    }

    /**
     * <p>Setter for the field <code>patient</code>.</p>
     *
     * @param patient a {@link PatientDB} object.
     */
    public void setPatient(PatientDB patient) {
        this.patient = patient;
    }

    /**
     * <p>isPrincipal.</p>
     *
     * @return a boolean.
     */
    public boolean isPrincipal() {
        return principal;
    }

    /**
     * <p>Setter for the field <code>principal</code>.</p>
     *
     * @param principal a boolean.
     */
    public void setPrincipal(boolean principal) {
        this.principal = principal;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PatientPhoneNumber)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        PatientPhoneNumber that = (PatientPhoneNumber) o;

        return principal == that.principal;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (principal ? 1 : 0);
        return result;
    }
}
