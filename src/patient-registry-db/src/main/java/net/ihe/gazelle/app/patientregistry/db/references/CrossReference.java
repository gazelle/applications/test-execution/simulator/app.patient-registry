package net.ihe.gazelle.app.patientregistry.db.references;

import net.ihe.gazelle.app.patientregistry.db.patient.PatientDB;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "pix_cross_reference", schema = "public")
@SequenceGenerator(name = "pix_cross_reference_sequence", sequenceName = "pix_cross_reference_id_seq", allocationSize = 1)
public class CrossReference implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1074748883268222875L;

	@Id
	@GeneratedValue(generator = "pix_cross_reference_sequence", strategy = GenerationType.SEQUENCE)
	@Column(name = "id")
	private Integer id;

	@OneToMany(mappedBy = "pixReference", fetch = FetchType.LAZY)
	private List<PatientDB> patients;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_changed")
	private Date lastChanged;

	@Column(name = "last_modifier")
	private String lastModifier;

	public CrossReference(String username) {
		this.lastChanged = new Date();
		this.lastModifier = username;
	}

	public CrossReference() {
		this.lastChanged = new Date();
	}

	public List<PatientDB> getPatients() {
		return patients;
	}

	public void setPatients(List<PatientDB> patients) {
		this.patients = patients;
	}

	public Date getLastChanged() {
		return lastChanged;
	}

	public void setLastChanged(Date lastChanged) {
		if (lastChanged != null) {
			this.lastChanged = (Date) lastChanged.clone();
		} else {
			this.lastChanged = null;
		}
	}

	public String getLastModifier() {
		return lastModifier;
	}

	public void setLastModifier(String lastModifier) {
		this.lastModifier = lastModifier;
	}

	public Integer getId() {
		return id;
	}

	public CrossReference updateReference(EntityManager entityManager, String user) {
		this.lastChanged = new Date();
		this.lastModifier = user;
		return entityManager.merge(this);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof CrossReference)) {
			return false;
		}

		CrossReference that = (CrossReference) o;

		if (patients != null ? !patients.equals(that.patients) : that.patients != null) {
			return false;
		}
		if (lastChanged != null ? !lastChanged.equals(that.lastChanged) : that.lastChanged != null) {
			return false;
		}
		return lastModifier != null ? lastModifier.equals(that.lastModifier) : that.lastModifier == null;
	}

	@Override
	public int hashCode() {
		int result = patients != null ? patients.hashCode() : 0;
		result = 31 * result + (lastChanged != null ? lastChanged.hashCode() : 0);
		result = 31 * result + (lastModifier != null ? lastModifier.hashCode() : 0);
		return result;
	}
}
