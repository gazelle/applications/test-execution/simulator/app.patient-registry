package net.ihe.gazelle.app.patientregistry.db.patient;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>PatientIdentifier class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Entity
@Table(name = "pam_patient_identifier", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {
		"full_patient_id", "identifier_type_code" }))
@SequenceGenerator(name = "pam_patient_identifier_sequence", sequenceName = "pam_patient_identifier_id_seq", allocationSize = 1)
public class PatientIdentifierDB implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 3283628371977314516L;

	@Id
	@GeneratedValue(generator = "pam_patient_identifier_sequence", strategy = GenerationType.SEQUENCE)
	@Column(name = "id")
	private Integer id;

	@Column(name = "full_patient_id")
	private String fullPatientId;

	/**
	 * PID-3-1 (ST) or extension (HL7v3)
	 */
	@Column(name = "id_number")
	private String idNumber;

	/**
	 * PID-3-4 (HD)
	 */
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "domain_id")
	private HierarchicDesignator domain;

	/**
	 * used to fill PID-3-5 (ID)
	 */
	@Column(name = "identifier_type_code")
	private String identifierTypeCode;

	public List<PatientDB> getPatients() {
		return patients;
	}

	public void setPatients(List<PatientDB> patients) {
		this.patients = patients;
	}

	@ManyToMany(mappedBy = "patientIdentifiers", targetEntity = PatientDB.class)
	private List<PatientDB> patients = new ArrayList<>();

	/**
	 * <p>Constructor for PatientIdentifier.</p>
	 */
	public PatientIdentifierDB() {

	}

	/**
	 * <p>Constructor for PatientIdentifier.</p>
	 *
	 * @param patientId a {@link java.lang.String} object.
	 * @param identifierTypeCode a {@link java.lang.String} object.
	 */
	public PatientIdentifierDB(String patientId, String identifierTypeCode) {
		this.fullPatientId = patientId;
		if ((identifierTypeCode != null) && !identifierTypeCode.isEmpty()) {
			this.identifierTypeCode = identifierTypeCode;
		} else {
			this.identifierTypeCode = null;
		}
	}

	/**
	 * <p>setFullPatientIdentifierIfEmpty.</p>
	 */
	@PrePersist
	@PreUpdate
	public void setFullPatientIdentifierIfEmpty(){
		if (this.fullPatientId == null){
			fullPatientId = (this.getIdNumber() != null ?this.getIdNumber(): "");
			if (domain != null){
				fullPatientId = fullPatientId.concat("^^^");
				if (domain.getNamespaceID() != null) {
					fullPatientId = fullPatientId.concat(domain.getNamespaceID());
				}
				fullPatientId = fullPatientId.concat("&");
				if (domain.getUniversalID() != null) {
					fullPatientId = fullPatientId.concat(domain.getUniversalID());
				}
				fullPatientId = fullPatientId.concat("&");
				if (domain.getUniversalIDType() != null) {
					fullPatientId = fullPatientId.concat(domain.getUniversalIDType());
				}
			}
		}
	}

	/**
	 * Constructor for copy
	 *
	 * @param pid a {@link PatientIdentifierDB} object.
	 */
	public PatientIdentifierDB(PatientIdentifierDB pid) {
		this.fullPatientId = pid.getFullPatientId();
		this.idNumber = pid.getIdNumber();
		this.identifierTypeCode = pid.getIdentifierTypeCode();
		this.domain = pid.getDomain();
	}

	/**
	 * <p>Setter for the field <code>fullPatientId</code>.</p>
	 *
	 * @param patientId a {@link java.lang.String} object.
	 */
	public void setFullPatientId(String patientId) {
		this.fullPatientId = patientId;
	}

	/**
	 * <p>Getter for the field <code>fullPatientId</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getFullPatientId() {
		return fullPatientId;
	}

	/**
	 * <p>Getter for the field <code>id</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * <p>Setter for the field <code>id</code>.</p>
	 *
	 * @param id a {@link java.lang.Integer} object.
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * <p>Setter for the field <code>identifierTypeCode</code>.</p>
	 *
	 * @param identifierTypeCode a {@link java.lang.String} object.
	 */
	public void setIdentifierTypeCode(String identifierTypeCode) {
		if ((identifierTypeCode != null) && identifierTypeCode.isEmpty()) {
			this.identifierTypeCode = null;
		}
		this.identifierTypeCode = identifierTypeCode;
	}

	/**
	 * <p>Getter for the field <code>identifierTypeCode</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getIdentifierTypeCode() {
		return identifierTypeCode;
	}


	/**
	 * <p>displayIdentifier.</p>
	 *
	 * @param pid a {@link PatientIdentifierDB} object.
	 * @return a {@link java.lang.String} object.
	 */
	// use fullPatientId attribute instead
	@Deprecated
	public String displayIdentifier(PatientIdentifierDB pid) {
		if (pid != null) {
			return pid.getFullPatientId();
		} else {
			return null;
		}
	}



	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof PatientIdentifierDB)) {
			return false;
		}

		PatientIdentifierDB that = (PatientIdentifierDB) o;

		if (fullPatientId != null ? !fullPatientId.equals(that.fullPatientId) : that.fullPatientId != null) {
			return false;
		}
		if (idNumber != null ? !idNumber.equals(that.idNumber) : that.idNumber != null) {
			return false;
		}
		return identifierTypeCode != null ? identifierTypeCode.equals(that.identifierTypeCode) : that.identifierTypeCode == null;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		int result = fullPatientId != null ? fullPatientId.hashCode() : 0;
		result = 31 * result + (idNumber != null ? idNumber.hashCode() : 0);
		result = 31 * result + (identifierTypeCode != null ? identifierTypeCode.hashCode() : 0);
		return result;
	}

	/**
	 * <p>Setter for the field <code>domain</code>.</p>
	 *
	 * @param domain a {@link HierarchicDesignator} object.
	 */
	public void setDomain(HierarchicDesignator domain) {
		this.domain = domain;
	}

	/**
	 * <p>Getter for the field <code>domain</code>.</p>
	 *
	 * @return a {@link HierarchicDesignator} object.
	 */
	public HierarchicDesignator getDomain() {
		return domain;
	}

	/**
	 * <p>Setter for the field <code>idNumber</code>.</p>
	 *
	 * @param idNumber a {@link java.lang.String} object.
	 */
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	/**
	 * <p>Getter for the field <code>idNumber</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getIdNumber() {
		return idNumber;
	}

}
