package net.ihe.gazelle.app.patientregistry.db.patient;

import net.ihe.gazelle.app.patientregistry.business.patient.Code;
import net.ihe.gazelle.app.patientregistry.business.patient.GenderCode;

public enum GenderCodeDB {

   M(GenderCode.MALE),
   F(GenderCode.FEMALE),
   U(GenderCode.UNDEFINED),
   O(GenderCode.OTHER);

   private final GenderCode genderCode;

   GenderCodeDB(GenderCode genderCode) {
      this.genderCode = genderCode;
   }

   public static GenderCodeDB fromGenderCode(GenderCode genderCode) {
      switch (genderCode) {
         case MALE:
            return GenderCodeDB.M;
         case FEMALE:
            return GenderCodeDB.F;
         case UNDEFINED:
            return GenderCodeDB.U;
         case OTHER:
            return GenderCodeDB.O;
         default:
            throw new IllegalArgumentException("Unknown GenderCode");
      }
   }

   public GenderCode toGenderCode() {
      return genderCode;
   }

}
