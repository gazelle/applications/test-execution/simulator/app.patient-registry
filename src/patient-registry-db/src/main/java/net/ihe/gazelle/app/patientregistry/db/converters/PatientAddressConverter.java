/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.db.converters;

import net.ihe.gazelle.app.patientregistry.business.patient.Address;
import net.ihe.gazelle.app.patientregistry.db.patient.PatientAddress;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * <p>PatientAddressConverter class.</p>
 *
 * @author abe
 * @version 1.0: 18/12/2019
 */

public class PatientAddressConverter {

    public static Address toAddress(PatientAddress patientAddress){
        Address address = new Address();
        address.setCity(patientAddress.getCity());
        address.setCountryIso3(patientAddress.getCountryCode());
        address.setState(patientAddress.getState());
        address.setPostalCode(patientAddress.getZipCode());
        Set<String> lines = new HashSet<>();
        lines.add(patientAddress.getAddressLine());
        address.setLines(lines);
        return address;
    }

    public static Set<Address> toAddressSet(List<PatientAddress> patientAddresses){
        Set<Address> addresses = new HashSet<Address>();
        for (PatientAddress patientAddress: patientAddresses){
            addresses.add(toAddress(patientAddress));
        }
        return addresses;
    }
}
