package net.ihe.gazelle.app.patientregistry.db.adapters;

import net.ihe.gazelle.app.patientregistry.business.exceptions.UnknownRequestedDomainException;
import net.ihe.gazelle.app.patientregistry.business.patient.GenderCode;
import net.ihe.gazelle.app.patientregistry.business.patient.ObservationTypeEnum;
import net.ihe.gazelle.app.patientregistry.business.patient.Patient;
import net.ihe.gazelle.app.patientregistry.business.patient.PersonName;
import net.ihe.gazelle.app.patientregistry.business.search.DateInterval;
import net.ihe.gazelle.app.patientregistry.business.search.IdentifierSearchCriteria;
import net.ihe.gazelle.app.patientregistry.business.search.PatientSearchCriteria;
import net.ihe.gazelle.app.patientregistry.db.patient.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

//TODO Do error cases
public class PatientDAOImplTest {

    private static final String PERSISTENCE_UNIT_NAME_CONST = "PersistenceUnitTest";

    PatientDAOImpl patientDAO;

    EntityManager entityManager;
    int increment = 0;

    @BeforeEach
    public void initializeDatabase(){
        entityManager = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME_CONST).createEntityManager();
        entityManager.getTransaction().begin();
        patientDAO = new PatientDAOImpl(entityManager);
    }

    @AfterEach
    public void closeDatabase(){
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Test
    public void getPatientByUUIDOK(){
        String uuid = "uuid";
        PatientDB patientDB = new PatientDB();
        patientDB.setUuid(uuid);
        patientDB.setPatientIdentifiers(getPatientIdentifier());
        patientDB = mergePatient(patientDB);

        Patient patient = patientDAO.getPatientByUUID(uuid);
        assertEquals(uuid, patient.getUuid());

        removePatient(patientDB);
    }

    @Test
    public void getPatientByUUIDNotExisting(){
        String uuid = "uuid";
        Patient patient = patientDAO.getPatientByUUID(uuid);
        assertNull(patient);
    }

    @Test
    public void getPatientByUUIDMultipleExisting(){
        String uuid = "uuid";
        PatientDB patientDB = new PatientDB();
        patientDB.setUuid(uuid);
        patientDB = mergePatient(patientDB);
        PatientDB patientDB2 = new PatientDB();
        patientDB2.setUuid(uuid);
        patientDB2 = mergePatient(patientDB2);
        try {
            patientDAO.getPatientByUUID(uuid);
            fail("getPatientByUUID method should have thrown PersistenceException");
        } catch (PersistenceException e){
            assertEquals("query did not return a unique result: 2", e.getMessage());
        }

        removePatient(patientDB);
        removePatient(patientDB2);
    }

    @Test
    public void findPatientNoCriteriaConnectathon(){
        String uuid = "uuid";
        PatientDB patientDB = new PatientDB();
        patientDB.setSimulatedActor("CONNECTATHON");
        patientDB.setStillActive(true);
        patientDB.setTestData(false);
        patientDB.setUuid(uuid);
        patientDB.setPatientIdentifiers(getPatientIdentifier());
        patientDB.addPatientAddress(new PatientAddress());
        patientDB.addPhoneNumber(new PatientPhoneNumber());
        patientDB = mergePatient(patientDB);

        PatientSearchCriteria patientSearchCriteria = new PatientSearchCriteria();
        try {
            List<Patient> patients = patientDAO.findPatients(patientSearchCriteria, new ArrayList<>());
            assertEquals(1, patients.size());
        } catch (UnknownRequestedDomainException e){
            fail("findPatients should not throw exception", e);
        }

        removePatient(patientDB);
    }

    @Test
    public void findPatientNoCriteriaPDS(){
        String uuid = "uuid";
        PatientDB patientDB = new PatientDB();
        patientDB.setSimulatedActor("PDS");
        patientDB.setStillActive(true);
        patientDB.setTestData(false);
        patientDB.setUuid(uuid);
        patientDB.setPatientIdentifiers(getPatientIdentifier());
        patientDB = mergePatient(patientDB);

        PatientSearchCriteria patientSearchCriteria = new PatientSearchCriteria();
        try {
            List<Patient> patients = patientDAO.findPatients(patientSearchCriteria, new ArrayList<>());
            assertEquals(1, patients.size());
        } catch (UnknownRequestedDomainException e){
            fail("findPatients should not throw exception", e);
        }
        removePatient(patientDB);
    }

    @Test
    public void findPatientPersonNameFamily(){
        String family = "family";
        String uuid = "uuid";

        PatientDB patientDB = new PatientDB();
        patientDB.setSimulatedActor("CONNECTATHON");
        patientDB.setStillActive(true);
        patientDB.setTestData(false);
        patientDB.addPatientAddress(new PatientAddress());
        patientDB.addPhoneNumber(new PatientPhoneNumber());
        patientDB.setLastName(family);
        patientDB.setUuid(uuid);
        patientDB.setPatientIdentifiers(getPatientIdentifier());
        patientDB = mergePatient(patientDB);

        PatientSearchCriteria patientSearchCriteria = new PatientSearchCriteria();
        List<PersonName> personNames = new ArrayList<>();
        PersonName personName = new PersonName();
        personName.setFamily(family);
        personNames.add(personName);
        patientSearchCriteria.setPersonNames(personNames);

        try {
            List<Patient> patients = patientDAO.findPatients(patientSearchCriteria, new ArrayList<>());
            assertEquals(1, patients.size());
            assertEquals(family, patients.get(0).getNames().iterator().next().getFamily());
        } catch (UnknownRequestedDomainException e){
            fail("findPatients should not throw exception", e);
        }

        removePatient(patientDB);
    }

    @Test
    public void findPatientPersonNameFamilyNoResult(){
        String family = "family";
        String uuid = "uuid";

        PatientDB patientDB = new PatientDB();
        patientDB.setSimulatedActor("CONNECTATHON");
        patientDB.setStillActive(true);
        patientDB.setTestData(false);
        patientDB.addPatientAddress(new PatientAddress());
        patientDB.addPhoneNumber(new PatientPhoneNumber());
        patientDB.setLastName(family + "12");
        patientDB.setUuid(uuid);
        patientDB.setPatientIdentifiers(getPatientIdentifier());
        patientDB = mergePatient(patientDB);

        PatientSearchCriteria patientSearchCriteria = new PatientSearchCriteria();
        List<PersonName> personNames = new ArrayList<>();
        PersonName personName = new PersonName();
        personName.setFamily(family);
        personNames.add(personName);
        patientSearchCriteria.setPersonNames(personNames);

        try {
            List<Patient> patients = patientDAO.findPatients(patientSearchCriteria, new ArrayList<>());
            assertEquals(0, patients.size());
        } catch (UnknownRequestedDomainException e){
            fail("findPatients should not throw exception", e);
        }

        removePatient(patientDB);
    }

    @Test
    public void findPatientPersonNameGivenFirstName(){
        String given = "given";
        String uuid = "uuid";

        PatientDB patientDB = new PatientDB();
        patientDB.setSimulatedActor("CONNECTATHON");
        patientDB.setStillActive(true);
        patientDB.setTestData(false);
        patientDB.addPatientAddress(new PatientAddress());
        patientDB.addPhoneNumber(new PatientPhoneNumber());
        patientDB.setFirstName(given);
        patientDB.setUuid(uuid);
        patientDB.setPatientIdentifiers(getPatientIdentifier());
        patientDB = mergePatient(patientDB);

        PatientSearchCriteria patientSearchCriteria = new PatientSearchCriteria();
        List<PersonName> personNames = new ArrayList<>();
        PersonName personName = new PersonName();
        personName.addGiven(given);
        personNames.add(personName);
        patientSearchCriteria.setPersonNames(personNames);

        try {
            List<Patient> patients = patientDAO.findPatients(patientSearchCriteria, new ArrayList<>());
            assertEquals(1, patients.size());
            assertEquals(given, patients.get(0).getNames().iterator().next().getGivens().iterator().next());
        } catch (UnknownRequestedDomainException e){
            fail("findPatients should not throw exception", e);
        }

        removePatient(patientDB);
    }

    @Test
    public void findPatientPersonNameGivenSecondName(){
        String firstName = "Jean";
        String secondName = "Charles";
        String uuid = "uuid";

        PatientDB patientDB = new PatientDB();
        patientDB.setSimulatedActor("CONNECTATHON");
        patientDB.setStillActive(true);
        patientDB.setTestData(false);
        patientDB.addPatientAddress(new PatientAddress());
        patientDB.addPhoneNumber(new PatientPhoneNumber());
        patientDB.setFirstName(firstName);
        patientDB.setSecondName(secondName);
        patientDB.setUuid(uuid);
        patientDB.setPatientIdentifiers(getPatientIdentifier());
        patientDB = mergePatient(patientDB);

        PatientSearchCriteria patientSearchCriteria = new PatientSearchCriteria();
        List<PersonName> personNames = new ArrayList<>();
        PersonName personName = new PersonName();
        personName.addGiven(firstName);
        personName.addGiven(secondName);
        personNames.add(personName);
        patientSearchCriteria.setPersonNames(personNames);

        try {
            List<Patient> patients = patientDAO.findPatients(patientSearchCriteria, new ArrayList<>());
            assertEquals(1, patients.size());
            Iterator givens = patients.get(0).getNames().iterator().next().getGivens().iterator();
            assertEquals(firstName, givens.next());
            assertEquals(secondName, givens.next());
        } catch (UnknownRequestedDomainException e){
            fail("findPatients should not throw exception", e);
        }

        removePatient(patientDB);
    }

    @Test
    public void findPatientPersonNameGivenThirdName(){
        String firstName = "Jean";
        String secondName = "Charles";
        String thirdName = "Henri";
        String uuid = "uuid";

        PatientDB patientDB = new PatientDB();
        patientDB.setSimulatedActor("CONNECTATHON");
        patientDB.setStillActive(true);
        patientDB.setTestData(false);
        patientDB.addPatientAddress(new PatientAddress());
        patientDB.addPhoneNumber(new PatientPhoneNumber());
        patientDB.setFirstName(firstName);
        patientDB.setSecondName(secondName);
        patientDB.setThirdName(thirdName);
        patientDB.setUuid(uuid);
        patientDB.setPatientIdentifiers(getPatientIdentifier());
        patientDB = mergePatient(patientDB);

        PatientSearchCriteria patientSearchCriteria = new PatientSearchCriteria();
        List<PersonName> personNames = new ArrayList<>();
        PersonName personName = new PersonName();
        personName.addGiven(null);
        personName.addGiven(null);
        personName.addGiven(thirdName);
        personNames.add(personName);
        patientSearchCriteria.setPersonNames(personNames);

        try {
            List<Patient> patients = patientDAO.findPatients(patientSearchCriteria, new ArrayList<>());
            assertEquals(1, patients.size());
            assertEquals(thirdName, patients.get(0).getNames().iterator().next().getGivens().get(2));
        } catch (UnknownRequestedDomainException e){
            fail("findPatients should not throw exception", e);
        }

        removePatient(patientDB);
    }

    @Test
    public void findPatientPersonNameGivenAlternativeFirstName(){
        String given = "given";
        String uuid = "uuid";

        PatientDB patientDB = new PatientDB();
        patientDB.setSimulatedActor("CONNECTATHON");
        patientDB.setStillActive(true);
        patientDB.setTestData(false);
        patientDB.addPatientAddress(new PatientAddress());
        patientDB.addPhoneNumber(new PatientPhoneNumber());
        patientDB.setFirstName(given);
        patientDB.setAlternateFirstName(given);
        patientDB.setUuid(uuid);
        patientDB.setPatientIdentifiers(getPatientIdentifier());
        patientDB = mergePatient(patientDB);

        PatientSearchCriteria patientSearchCriteria = new PatientSearchCriteria();
        List<PersonName> personNames = new ArrayList<>();
        PersonName personName = new PersonName();
        personName.addGiven(given);
        personNames.add(new PersonName());
        personNames.add(personName);
        patientSearchCriteria.setPersonNames(personNames);

        try {
            List<Patient> patients = patientDAO.findPatients(patientSearchCriteria, new ArrayList<>());
            assertEquals(1, patients.size());
            assertEquals(given, patients.get(0).getNames().iterator().next().getGivens().iterator().next());
        } catch (UnknownRequestedDomainException e){
            fail("findPatients should not throw exception", e);
        }

        removePatient(patientDB);
    }

    @Test
    public void findPatientPersonNameGivenAlternateSecondName(){
        String firstName = "Jean";
        String secondName = "Charles";
        String uuid = "uuid";

        PatientDB patientDB = new PatientDB();
        patientDB.setSimulatedActor("CONNECTATHON");
        patientDB.setStillActive(true);
        patientDB.setTestData(false);
        patientDB.addPatientAddress(new PatientAddress());
        patientDB.addPhoneNumber(new PatientPhoneNumber());
        patientDB.setFirstName(firstName);
        patientDB.setSecondName(secondName);
        patientDB.setAlternateFirstName(firstName);
        patientDB.setAlternateSecondName(secondName);
        patientDB.setUuid(uuid);
        patientDB.setPatientIdentifiers(getPatientIdentifier());
        patientDB = mergePatient(patientDB);

        PatientSearchCriteria patientSearchCriteria = new PatientSearchCriteria();
        List<PersonName> personNames = new ArrayList<>();
        PersonName personName = new PersonName();
        personName.addGiven(null);
        personName.addGiven(secondName);
        personNames.add(new PersonName());
        personNames.add(personName);
        patientSearchCriteria.setPersonNames(personNames);

        try {
            List<Patient> patients = patientDAO.findPatients(patientSearchCriteria, new ArrayList<>());
            assertEquals(1, patients.size());
            Iterator givens = patients.get(0).getNames().iterator().next().getGivens().iterator();
            assertEquals(firstName, givens.next());
            assertEquals(secondName, givens.next());
        } catch (UnknownRequestedDomainException e){
            fail("findPatients should not throw exception", e);
        }

        removePatient(patientDB);
    }

    @Test
    public void findPatientPersonNameGivenAlternativeThirdName(){
        String firstName = "Jean";
        String secondName = "Charles";
        String thirdName = "Henri";
        String uuid = "uuid";

        PatientDB patientDB = new PatientDB();
        patientDB.setSimulatedActor("CONNECTATHON");
        patientDB.setStillActive(true);
        patientDB.setTestData(false);
        patientDB.addPatientAddress(new PatientAddress());
        patientDB.addPhoneNumber(new PatientPhoneNumber());
        patientDB.setFirstName(firstName);
        patientDB.setSecondName(secondName);
        patientDB.setThirdName(thirdName);
        patientDB.setAlternateFirstName(firstName);
        patientDB.setAlternateSecondName(secondName);
        patientDB.setAlternateThirdName(thirdName);
        patientDB.setUuid(uuid);
        patientDB.setPatientIdentifiers(getPatientIdentifier());
        patientDB = mergePatient(patientDB);

        PatientSearchCriteria patientSearchCriteria = new PatientSearchCriteria();
        List<PersonName> personNames = new ArrayList<>();
        PersonName personName = new PersonName();
        personName.addGiven(null);
        personName.addGiven(null);
        personName.addGiven(thirdName);
        personNames.add(new PersonName());
        personNames.add(personName);
        patientSearchCriteria.setPersonNames(personNames);

        try {
            List<Patient> patients = patientDAO.findPatients(patientSearchCriteria, new ArrayList<>());
            assertEquals(1, patients.size());
            Iterator<PersonName> names = patients.get(0).getNames().iterator();
            names.next();
            assertEquals(thirdName, names.next().getGivens().get(2));
        } catch (UnknownRequestedDomainException e){
            fail("findPatients should not throw exception", e);
        }

        removePatient(patientDB);
    }

    @Test
    public void findPatientMothersMaidenName(){
        String mothersMaidenName = "mothersMaidenName";
        String uuid = "uuid";

        PatientDB patientDB = new PatientDB();
        patientDB.setSimulatedActor("CONNECTATHON");
        patientDB.setStillActive(true);
        patientDB.setTestData(false);
        patientDB.addPatientAddress(new PatientAddress());
        patientDB.addPhoneNumber(new PatientPhoneNumber());
        patientDB.setMotherMaidenName(mothersMaidenName);
        patientDB.setUuid(uuid);
        patientDB.setPatientIdentifiers(getPatientIdentifier());
        patientDB = mergePatient(patientDB);

        PatientSearchCriteria patientSearchCriteria = new PatientSearchCriteria();
        patientSearchCriteria.setMothersMaidenName(mothersMaidenName);

        try {
            List<Patient> patients = patientDAO.findPatients(patientSearchCriteria, new ArrayList<>());
            assertEquals(1, patients.size());
            assertEquals(1, patients.get(0).getObservations().size());
            assertEquals(mothersMaidenName, patients.get(0).getObservations().iterator().next().getValue());
            assertEquals(ObservationTypeEnum.MOTHERS_MAINDEN_NAME, patients.get(0).getObservations().iterator().next().getObservationType());
        } catch (UnknownRequestedDomainException e){
            fail("findPatients should not throw exception", e);
        }

        removePatient(patientDB);
    }

    @Test
    public void findPatientBirthDateExact(){
        Date birthExactDate = new Date();
        String uuid = "uuid";

        PatientDB patientDB = new PatientDB();
        patientDB.setSimulatedActor("CONNECTATHON");
        patientDB.setStillActive(true);
        patientDB.setTestData(false);
        patientDB.addPatientAddress(new PatientAddress());
        patientDB.addPhoneNumber(new PatientPhoneNumber());
        patientDB.setDateOfBirth(birthExactDate);
        patientDB.setUuid(uuid);
        patientDB.setPatientIdentifiers(getPatientIdentifier());
        patientDB = mergePatient(patientDB);

        PatientSearchCriteria patientSearchCriteria = new PatientSearchCriteria();
        DateInterval dateOfBirth = new DateInterval();
        dateOfBirth.setExactDate(birthExactDate);
        patientSearchCriteria.setDateOfBirth(dateOfBirth);

        try {
            List<Patient> patients = patientDAO.findPatients(patientSearchCriteria, new ArrayList<>());
            assertEquals(1, patients.size());
            assertEquals(birthExactDate, patients.get(0).getDateOfBirth());
        } catch (UnknownRequestedDomainException e){
            fail("findPatients should not throw exception", e);
        }

        removePatient(patientDB);
    }

    @Test
    public void findPatientBirthDateLow(){
        Date birthLowDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(birthLowDate);
        c.add(Calendar.DATE, 1);
        Date birthDate = c.getTime();

        String uuid = "uuid";

        PatientDB patientDB = new PatientDB();
        patientDB.setSimulatedActor("CONNECTATHON");
        patientDB.setStillActive(true);
        patientDB.setTestData(false);
        patientDB.addPatientAddress(new PatientAddress());
        patientDB.addPhoneNumber(new PatientPhoneNumber());
        patientDB.setDateOfBirth(birthDate);
        patientDB.setUuid(uuid);
        patientDB.setPatientIdentifiers(getPatientIdentifier());
        patientDB = mergePatient(patientDB);

        PatientSearchCriteria patientSearchCriteria = new PatientSearchCriteria();
        DateInterval dateOfBirth = new DateInterval();
        dateOfBirth.setLowLimit(birthLowDate);
        dateOfBirth.setLowLimitInclusive(true);
        patientSearchCriteria.setDateOfBirth(dateOfBirth);

        try {
            List<Patient> patients = patientDAO.findPatients(patientSearchCriteria, new ArrayList<>());
            assertEquals(1, patients.size());
            assertEquals(birthDate, patients.get(0).getDateOfBirth());
        } catch (UnknownRequestedDomainException e){
            fail("findPatients should not throw exception", e);
        }

        removePatient(patientDB);
    }

    @Test
    public void findPatientBirthDateLowNotInclusive(){
        Date birthLowDate = new Date();

        String uuid = "uuid";

        PatientDB patientDB = new PatientDB();
        patientDB.setSimulatedActor("CONNECTATHON");
        patientDB.setStillActive(true);
        patientDB.setTestData(false);
        patientDB.addPatientAddress(new PatientAddress());
        patientDB.addPhoneNumber(new PatientPhoneNumber());
        patientDB.setDateOfBirth(birthLowDate);
        patientDB.setUuid(uuid);
        patientDB.setPatientIdentifiers(getPatientIdentifier());
        patientDB = mergePatient(patientDB);

        PatientSearchCriteria patientSearchCriteria = new PatientSearchCriteria();
        DateInterval dateOfBirth = new DateInterval();
        dateOfBirth.setLowLimit(birthLowDate);
        dateOfBirth.setLowLimitInclusive(false);
        patientSearchCriteria.setDateOfBirth(dateOfBirth);

        try {
            List<Patient> patients = patientDAO.findPatients(patientSearchCriteria, new ArrayList<>());
            assertEquals(0, patients.size());
        } catch (UnknownRequestedDomainException e){
            fail("findPatients should not throw exception", e);
        }

        removePatient(patientDB);
    }

    @Test
    public void findPatientBirthDateLowInclusive(){
        Date birthLowDate = new Date();

        String uuid = "uuid";

        PatientDB patientDB = new PatientDB();
        patientDB.setSimulatedActor("CONNECTATHON");
        patientDB.setStillActive(true);
        patientDB.setTestData(false);
        patientDB.addPatientAddress(new PatientAddress());
        patientDB.addPhoneNumber(new PatientPhoneNumber());
        patientDB.setDateOfBirth(birthLowDate);
        patientDB.setUuid(uuid);
        patientDB.setPatientIdentifiers(getPatientIdentifier());
        patientDB = mergePatient(patientDB);

        PatientSearchCriteria patientSearchCriteria = new PatientSearchCriteria();
        DateInterval dateOfBirth = new DateInterval();
        dateOfBirth.setLowLimit(birthLowDate);
        dateOfBirth.setLowLimitInclusive(true);
        patientSearchCriteria.setDateOfBirth(dateOfBirth);

        try {
            List<Patient> patients = patientDAO.findPatients(patientSearchCriteria, new ArrayList<>());
            assertEquals(1, patients.size());
            assertEquals(birthLowDate, patients.get(0).getDateOfBirth());
        } catch (UnknownRequestedDomainException e){
            fail("findPatients should not throw exception", e);
        }

        removePatient(patientDB);
    }

    @Test
    public void findPatientBirthDateHigh(){
        Date birthHighDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(birthHighDate);
        c.add(Calendar.DATE, -1);
        Date birthDate = c.getTime();

        String uuid = "uuid";

        PatientDB patientDB = new PatientDB();
        patientDB.setSimulatedActor("CONNECTATHON");
        patientDB.setStillActive(true);
        patientDB.setTestData(false);
        patientDB.addPatientAddress(new PatientAddress());
        patientDB.addPhoneNumber(new PatientPhoneNumber());
        patientDB.setDateOfBirth(birthDate);
        patientDB.setUuid(uuid);
        patientDB.setPatientIdentifiers(getPatientIdentifier());
        patientDB = mergePatient(patientDB);

        PatientSearchCriteria patientSearchCriteria = new PatientSearchCriteria();
        DateInterval dateOfBirth = new DateInterval();
        dateOfBirth.setHighLimit(birthHighDate);
        dateOfBirth.setHighLimitInclusive(true);
        patientSearchCriteria.setDateOfBirth(dateOfBirth);

        try {
            List<Patient> patients = patientDAO.findPatients(patientSearchCriteria, new ArrayList<>());
            assertEquals(1, patients.size());
            assertEquals(birthDate, patients.get(0).getDateOfBirth());
        } catch (UnknownRequestedDomainException e){
            fail("findPatients should not throw exception", e);
        }

        removePatient(patientDB);
    }

    @Test
    public void findPatientBirthDateHighNotInclusive(){
        Date birthHighDate = new Date();

        String uuid = "uuid";

        PatientDB patientDB = new PatientDB();
        patientDB.setSimulatedActor("CONNECTATHON");
        patientDB.setStillActive(true);
        patientDB.setTestData(false);
        patientDB.addPatientAddress(new PatientAddress());
        patientDB.addPhoneNumber(new PatientPhoneNumber());
        patientDB.setDateOfBirth(birthHighDate);
        patientDB.setUuid(uuid);
        patientDB.setPatientIdentifiers(getPatientIdentifier());
        patientDB = mergePatient(patientDB);

        PatientSearchCriteria patientSearchCriteria = new PatientSearchCriteria();
        DateInterval dateOfBirth = new DateInterval();
        dateOfBirth.setHighLimit(birthHighDate);
        dateOfBirth.setHighLimitInclusive(false);
        patientSearchCriteria.setDateOfBirth(dateOfBirth);

        try {
            List<Patient> patients = patientDAO.findPatients(patientSearchCriteria, new ArrayList<>());
            assertEquals(0, patients.size());
        } catch (UnknownRequestedDomainException e){
            fail("findPatients should not throw exception", e);
        }

        removePatient(patientDB);
    }

    @Test
    public void findPatientBirthDateHighInclusive(){
        Date birthHighDate = new Date();

        String uuid = "uuid";

        PatientDB patientDB = new PatientDB();
        patientDB.setSimulatedActor("CONNECTATHON");
        patientDB.setStillActive(true);
        patientDB.setTestData(false);
        patientDB.addPatientAddress(new PatientAddress());
        patientDB.addPhoneNumber(new PatientPhoneNumber());
        patientDB.setDateOfBirth(birthHighDate);
        patientDB.setUuid(uuid);
        patientDB.setPatientIdentifiers(getPatientIdentifier());
        patientDB = mergePatient(patientDB);

        PatientSearchCriteria patientSearchCriteria = new PatientSearchCriteria();
        DateInterval dateOfBirth = new DateInterval();
        dateOfBirth.setHighLimit(birthHighDate);
        dateOfBirth.setHighLimitInclusive(true);
        patientSearchCriteria.setDateOfBirth(dateOfBirth);

        try {
            List<Patient> patients = patientDAO.findPatients(patientSearchCriteria, new ArrayList<>());
            assertEquals(1, patients.size());
            assertEquals(birthHighDate, patients.get(0).getDateOfBirth());
        } catch (UnknownRequestedDomainException e){
            fail("findPatients should not throw exception", e);
        }

        removePatient(patientDB);
    }

    @Test
    public void findPatientBirthDateIntervalOK(){
        Date birthHighDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(birthHighDate);
        c.add(Calendar.DATE, -1);
        Date birthDate = c.getTime();
        c.add(Calendar.DATE, -1);
        Date birthLowDate = c.getTime();
        String uuid = "uuid";

        PatientDB patientDB = new PatientDB();
        patientDB.setSimulatedActor("CONNECTATHON");
        patientDB.setStillActive(true);
        patientDB.setTestData(false);
        patientDB.addPatientAddress(new PatientAddress());
        patientDB.addPhoneNumber(new PatientPhoneNumber());
        patientDB.setDateOfBirth(birthDate);
        patientDB.setUuid(uuid);
        patientDB.setPatientIdentifiers(getPatientIdentifier());
        patientDB = mergePatient(patientDB);

        PatientSearchCriteria patientSearchCriteria = new PatientSearchCriteria();
        DateInterval dateOfBirth = new DateInterval();
        dateOfBirth.setHighLimit(birthHighDate);
        dateOfBirth.setHighLimitInclusive(true);
        dateOfBirth.setLowLimit(birthLowDate);
        dateOfBirth.setLowLimitInclusive(true);
        patientSearchCriteria.setDateOfBirth(dateOfBirth);

        try {
            List<Patient> patients = patientDAO.findPatients(patientSearchCriteria, new ArrayList<>());
            assertEquals(1, patients.size());
            assertEquals(birthDate, patients.get(0).getDateOfBirth());
        } catch (UnknownRequestedDomainException e){
            fail("findPatients should not throw exception", e);
        }

        removePatient(patientDB);
    }

    @Test
    public void findPatientBirthDateIntervalKOAfter(){
        Date birthHighDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(birthHighDate);
        c.add(Calendar.DATE, 1);
        Date birthDate = c.getTime();
        c.add(Calendar.DATE, -2);
        Date birthLowDate = c.getTime();
        String uuid = "uuid";

        PatientDB patientDB = new PatientDB();
        patientDB.setSimulatedActor("CONNECTATHON");
        patientDB.setStillActive(true);
        patientDB.setTestData(false);
        patientDB.addPatientAddress(new PatientAddress());
        patientDB.addPhoneNumber(new PatientPhoneNumber());
        patientDB.setDateOfBirth(birthDate);
        patientDB.setUuid(uuid);
        patientDB.setPatientIdentifiers(getPatientIdentifier());
        patientDB = mergePatient(patientDB);

        PatientSearchCriteria patientSearchCriteria = new PatientSearchCriteria();
        DateInterval dateOfBirth = new DateInterval();
        dateOfBirth.setHighLimit(birthHighDate);
        dateOfBirth.setHighLimitInclusive(true);
        dateOfBirth.setLowLimit(birthLowDate);
        dateOfBirth.setLowLimitInclusive(true);
        patientSearchCriteria.setDateOfBirth(dateOfBirth);

        try {
            List<Patient> patients = patientDAO.findPatients(patientSearchCriteria, new ArrayList<>());
            assertEquals(0, patients.size());
        } catch (UnknownRequestedDomainException e){
            fail("findPatients should not throw exception", e);
        }

        removePatient(patientDB);
    }

    @Test
    public void findPatientBirthDateIntervalKOBefore(){
        Date birthHighDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(birthHighDate);
        c.add(Calendar.DATE, -2);
        Date birthDate = c.getTime();
        c.add(Calendar.DATE, 1);
        Date birthLowDate = c.getTime();
        String uuid = "uuid";

        PatientDB patientDB = new PatientDB();
        patientDB.setSimulatedActor("CONNECTATHON");
        patientDB.setStillActive(true);
        patientDB.setTestData(false);
        patientDB.addPatientAddress(new PatientAddress());
        patientDB.addPhoneNumber(new PatientPhoneNumber());
        patientDB.setDateOfBirth(birthDate);
        patientDB.setUuid(uuid);
        patientDB.setPatientIdentifiers(getPatientIdentifier());
        patientDB = mergePatient(patientDB);

        PatientSearchCriteria patientSearchCriteria = new PatientSearchCriteria();
        DateInterval dateOfBirth = new DateInterval();
        dateOfBirth.setHighLimit(birthHighDate);
        dateOfBirth.setHighLimitInclusive(true);
        dateOfBirth.setLowLimit(birthLowDate);
        dateOfBirth.setLowLimitInclusive(true);
        patientSearchCriteria.setDateOfBirth(dateOfBirth);

        try {
            List<Patient> patients = patientDAO.findPatients(patientSearchCriteria, new ArrayList<>());
            assertEquals(0, patients.size());
        } catch (UnknownRequestedDomainException e){
            fail("findPatients should not throw exception", e);
        }

        removePatient(patientDB);
    }

    @Test
    public void findPatientGenderCode(){
        GenderCode genderCode = GenderCode.FEMALE;
        String uuid = "uuid";

        PatientDB patientDB = new PatientDB();
        patientDB.setSimulatedActor("CONNECTATHON");
        patientDB.setStillActive(true);
        patientDB.setTestData(false);
        patientDB.addPatientAddress(new PatientAddress());
        patientDB.addPhoneNumber(new PatientPhoneNumber());
        patientDB.setGenderCode(GenderCodeDB.F);
        patientDB.setUuid(uuid);
        patientDB.setPatientIdentifiers(getPatientIdentifier());
        patientDB = mergePatient(patientDB);

        PatientSearchCriteria patientSearchCriteria = new PatientSearchCriteria();
        patientSearchCriteria.setGenderCode(net.ihe.gazelle.app.patientregistry.business.search.GenderCode.FEMALE);

        try {
            List<Patient> patients = patientDAO.findPatients(patientSearchCriteria, new ArrayList<>());
            assertEquals(1, patients.size());
            assertEquals(GenderCode.FEMALE, patients.get(0).getGender());
        } catch (UnknownRequestedDomainException e){
            fail("findPatients should not throw exception", e);
        }

        removePatient(patientDB);
    }

    @Test
    public void findPatientIdentifierValue(){
        String identifierValue = "id";
        String uuid = "uuid";

        PatientDB patientDB = new PatientDB();
        patientDB.setSimulatedActor("CONNECTATHON");
        patientDB.setStillActive(true);
        patientDB.setTestData(false);
        patientDB.setUuid(uuid);
        patientDB.setPatientIdentifiers(getPatientIdentifier());
        patientDB.getPatientIdentifiers().get(0).setIdNumber(identifierValue);
        patientDB = mergePatient(patientDB);

        PatientSearchCriteria patientSearchCriteria = new PatientSearchCriteria();
        IdentifierSearchCriteria identifierSearchCriteria = new IdentifierSearchCriteria();
        identifierSearchCriteria.setValue(identifierValue);
        List<IdentifierSearchCriteria> identifiers = new ArrayList<>();
        identifiers.add(identifierSearchCriteria);
        patientSearchCriteria.setIdentifiers(identifiers);

        try {
            List<Patient> patients = patientDAO.findPatients(patientSearchCriteria, new ArrayList<>());
            assertEquals(1, patients.size());
            assertEquals(1, patients.get(0).getIdentifiers().size());
            assertEquals(identifierValue, patients.get(0).getIdentifiers().iterator().next().getValue());
        } catch (UnknownRequestedDomainException e){
            fail("findPatients should not throw exception", e);
        }

        removePatient(patientDB);
    }

    @Test
    public void findPatientSystemIdentifier(){
        String domainUniversalId = "id";
        String uuid = "uuid";

        PatientDB patientDB = new PatientDB();
        patientDB.setSimulatedActor("CONNECTATHON");
        patientDB.setStillActive(true);
        patientDB.setTestData(false);
        patientDB.setUuid(uuid);
        patientDB.setPatientIdentifiers(getPatientIdentifier());
        patientDB.getPatientIdentifiers().get(0).getDomain().setUniversalID(domainUniversalId);
        patientDB = mergePatient(patientDB);

        PatientSearchCriteria patientSearchCriteria = new PatientSearchCriteria();
        IdentifierSearchCriteria identifierSearchCriteria = new IdentifierSearchCriteria();
        identifierSearchCriteria.setSystemIdentifier(domainUniversalId);
        List<IdentifierSearchCriteria> identifiers = new ArrayList<>();
        identifiers.add(identifierSearchCriteria);
        patientSearchCriteria.setIdentifiers(identifiers);

        try {
            List<Patient> patients = patientDAO.findPatients(patientSearchCriteria, new ArrayList<>());
            assertEquals(1, patients.size());
            assertEquals(1, patients.get(0).getIdentifiers().size());
            assertEquals(domainUniversalId, patients.get(0).getIdentifiers().iterator().next().getSystemIdentifier());
        } catch (UnknownRequestedDomainException e){
            fail("findPatients should not throw exception", e);
        }

        removePatient(patientDB);
    }

    private List<PatientIdentifierDB> getPatientIdentifier(){
        List<PatientIdentifierDB> identifierDBS = new ArrayList<>();
        PatientIdentifierDB identifier = new PatientIdentifierDB("test", "test");
        identifier.setFullPatientId("test"+ increment ++);
        identifier.setIdNumber("idNumber");
        HierarchicDesignator domain = new HierarchicDesignator("namespaceID", "universalID", "universalIDType", DesignatorType.PATIENT_ID);
        identifier.setDomain(domain);
        identifierDBS.add(identifier);
        return identifierDBS;
    }

    private PatientDB mergePatient(PatientDB patientDB){
        return entityManager.merge(patientDB);
    }

    private void removePatient(PatientDB patientDB){
        entityManager.remove(patientDB);
        if (patientDB.getPatientIdentifiers() != null){
            for (PatientIdentifierDB identifier : patientDB.getPatientIdentifiers()){
                entityManager.remove(identifier);
                entityManager.remove(identifier.getDomain());
            }
        }
    }
}
