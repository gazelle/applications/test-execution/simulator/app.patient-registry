package net.ihe.gazelle.app.patientregistry.ws;

import net.ihe.gazelle.app.patientregistry.application.search.PatientDAO;
import net.ihe.gazelle.app.patientregistry.db.adapters.PatientDAOImpl;
import net.ihe.gazelle.app.patientregistry.db.patient.DesignatorType;
import net.ihe.gazelle.app.patientregistry.db.patient.HierarchicDesignator;
import net.ihe.gazelle.app.patientregistry.db.patient.PatientDB;
import net.ihe.gazelle.app.patientregistry.db.patient.PatientIdentifierDB;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class PatientSearchImplTest {

    private static final String PERSISTENCE_UNIT_NAME_CONST = "PersistenceUnitTest";

    PatientSearchImpl patientSearch;

    EntityManager entityManager;
    int increment = 0;

    @BeforeEach
    public void initializeDatabase(){
        entityManager = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME_CONST).createEntityManager();
        entityManager.getTransaction().begin();
        PatientDAO patientDAO = new PatientDAOImpl(entityManager);
        patientSearch = new PatientSearchImpl(patientDAO);
    }

    @AfterEach
    public void closeDatabase(){
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Test
    public void getPatientByUUIDOK(){
        String uuid = "uuid";
        PatientDB patientDB = new PatientDB();
        patientDB.setUuid(uuid);
        patientDB.setPatientIdentifiers(getPatientIdentifier());
        patientDB = mergePatient(patientDB);

        Response response = patientSearch.searchPatientByUniqueId(uuid);
        assertEquals(200, response.getStatus());

        removePatient(patientDB);
    }

    @Test
    public void getPatientByUUIDNotExisting(){
        String uuid = "uuid";
        Response response = patientSearch.searchPatientByUniqueId(uuid);
        assertEquals(404, response.getStatus());
        assertNull(response.getEntity());
    }

    @Test
    public void getPatientByUUIDMultipleExisting(){
        String uuid = "uuid";
        PatientDB patientDB = new PatientDB();
        patientDB.setUuid(uuid);
        patientDB = mergePatient(patientDB);
        PatientDB patientDB2 = new PatientDB();
        patientDB2.setUuid(uuid);
        patientDB2 = mergePatient(patientDB2);
        Response response = patientSearch.searchPatientByUniqueId(uuid);
        assertEquals(404, response.getStatus());

        removePatient(patientDB);
        removePatient(patientDB2);
    }

    private List<PatientIdentifierDB> getPatientIdentifier(){
        List<PatientIdentifierDB> identifierDBS = new ArrayList<>();
        PatientIdentifierDB identifier = new PatientIdentifierDB("test", "test");
        identifier.setFullPatientId("test"+ increment ++);
        identifier.setIdNumber("idNumber");
        HierarchicDesignator domain = new HierarchicDesignator("namespaceID", "universalID", "universalIDType", DesignatorType.PATIENT_ID);
        identifier.setDomain(domain);
        identifierDBS.add(identifier);
        return identifierDBS;
    }

    private PatientDB mergePatient(PatientDB patientDB){
        return entityManager.merge(patientDB);
    }

    private void removePatient(PatientDB patientDB){
        entityManager.remove(patientDB);
        if (patientDB.getPatientIdentifiers() != null){
            for (PatientIdentifierDB identifier : patientDB.getPatientIdentifiers()){
                entityManager.remove(identifier);
                entityManager.remove(identifier.getDomain());
            }
        }
    }
}
