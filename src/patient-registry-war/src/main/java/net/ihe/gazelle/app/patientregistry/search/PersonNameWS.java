/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.search;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashSet;
import java.util.Set;

/**
 * <p>PersonName class.</p>
 * a person can have several names, this object represent one instance of a person name
 *
 * @author abe
 * @version 1.0: 11/10/2019
 */

@XmlRootElement(name = "name")
@XmlAccessorType(XmlAccessType.FIELD)
public class PersonNameWS{

    /*
     * Given names (not always 'first'). Includes middle names
     * This repeating element order: Given Names appear in the correct order for presenting the name
     */
    //@XmlElementWrapper(name = "givens")
    @XmlElement(name = "given")
    private Set<String> givens;

    /*
     * Family name (often called 'Surname')
     */
    private String family;

    /*
     * Part that come before the name
     */
    private String prefix;

    /*
     * Part that come after the name
     */
    private String suffix;

    /*
     * What is the usage of this name
     */
    private PersonNameCode use;

    /*
    Whether this is the principal name for the person (the one from which it is known)
     */
    private boolean principal;

    // TODO do we need the character set ?


    public PersonNameWS() {

    }

    public Set<String> getGivens() {
        if (givens == null) {
            givens = new HashSet<>();
        }
        return givens;
    }

    public void setGivens(Set<String> givens) {
        this.givens = givens;
    }

    public void addGiven(String givenToAdd){
        if (this.givens == null){
            this.givens = new HashSet<>();
        }
        this.givens.add(givenToAdd);
    }

    public void removeGiven(String givenToRemove){
        if (this.givens != null){
            this.givens.remove(givenToRemove);
        }
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public PersonNameCode getUse() {
        return use;
    }

    public void setUse(PersonNameCode use) {
        this.use = use;
    }

    public boolean isPrincipal() {
        return principal;
    }

    public void setPrincipal(boolean principal) {
        this.principal = principal;
    }


    @Override
    public String toString(){
        final StringBuilder sb = new StringBuilder("PersonName{");
        sb.append("given=").append(givens);
        sb.append(", family='").append(family).append('\'');
        sb.append(", prefix='").append(prefix).append('\'');
        sb.append(", suffix='").append(suffix).append('\'');
        sb.append(", use=").append(use);
        sb.append(", isPrincipal='").append(principal).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
