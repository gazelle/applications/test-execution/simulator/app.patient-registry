/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.search.wrapper;

import net.ihe.gazelle.app.patientregistry.business.search.IdentifierSearchCriteria;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * <p>PatientIdentifierSearchRequestWrapper class.</p>
 *
 * @author abe
 * @version 1.0: 20/11/2019
 */

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PatientIdentifierSearchRequestWrapper {

    @XmlElement(name = "identifierSearchCriteria")
    private IdentifierSearchCriteria patientIdentifierSearchCriteria;

    //@XmlElementWrapper(name = "restrictedDomains")
    @XmlElement(name = "restrictedDomain")
    private List<String> restrictedDomains;

    public PatientIdentifierSearchRequestWrapper() {
    }

    public IdentifierSearchCriteria getPatientIdentifierSearchCriteria() {
        return patientIdentifierSearchCriteria;
    }

    public void setPatientIdentifierSearchCriteria(IdentifierSearchCriteria patientIdentifierSearchCriteria) {
        this.patientIdentifierSearchCriteria = patientIdentifierSearchCriteria;
    }

    public List<String> getRestrictedDomains() {
        return restrictedDomains;
    }

    public void setRestrictedDomains(List<String> restrictedDomains) {
        this.restrictedDomains = restrictedDomains;
    }
}
