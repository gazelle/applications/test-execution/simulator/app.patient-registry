/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.ws.converters;

import net.ihe.gazelle.app.patientregistry.business.search.PatientSearchCriteria;
import net.ihe.gazelle.app.patientregistry.business.patient.Address;
import net.ihe.gazelle.app.patientregistry.business.patient.PersonName;
import net.ihe.gazelle.app.patientregistry.search.AddressWS;
import net.ihe.gazelle.app.patientregistry.search.PatientSearchCriteriaWS;
import net.ihe.gazelle.app.patientregistry.search.PersonNameWS;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * <p>PatientSearchCriteriaWSConverter class.</p>
 *
 * @author abe
 * @version 1.0: 04/12/2019
 */

public class PatientSearchCriteriaWSConverter {

    private static PatientSearchCriteriaWSConverter SINGLETON = new PatientSearchCriteriaWSConverter();

    public static PatientSearchCriteriaWSConverter getInstance(){
        return SINGLETON;
    }

    public PatientSearchCriteria toPatientSearchCriteria(PatientSearchCriteriaWS patientSearchCriteriaWS) throws NullPointerException{
        if (patientSearchCriteriaWS == null){
            throw new NullPointerException("received criteria are null");
        } else {
            PatientSearchCriteria searchCriteria = new PatientSearchCriteria();
            searchCriteria.setAccountNumber(patientSearchCriteriaWS.getAccountNumber());
            searchCriteria.setAddresses(convertAddressesWS(patientSearchCriteriaWS.getAddresses()));
            searchCriteria.setContactPoints(patientSearchCriteriaWS.getContactPoints());
            searchCriteria.setDateOfBirth(patientSearchCriteriaWS.getDateOfBirth());
            searchCriteria.setDateOfDeath(patientSearchCriteriaWS.getDateOfDeath());
            searchCriteria.setDeceased(patientSearchCriteriaWS.getDeceased());
            searchCriteria.setGenderCode(patientSearchCriteriaWS.getGenderCode());
            searchCriteria.setIdentifiers(patientSearchCriteriaWS.getIdentifiers());
            searchCriteria.setMothersMaidenName(patientSearchCriteriaWS.getMothersMaidenName());
            searchCriteria.setPersonNames(convertPersonNames(patientSearchCriteriaWS.getPersonNames()));
            searchCriteria.setActive(patientSearchCriteriaWS.getActive());
            return searchCriteria;
        }
    }

    private List<PersonName> convertPersonNames(List<PersonNameWS> personNamesWS) {
        if (personNamesWS == null){
            return null;
        } else {
            List<PersonName> personNames = new ArrayList<PersonName>();
            for (PersonNameWS personNameWS: personNamesWS){
                PersonName personName = new PersonName();
                personName.setFamily(personNameWS.getFamily());
                personName.setGivens(new ArrayList<>(personNameWS.getGivens()));
                personName.setPrefix(personNameWS.getPrefix());
                personName.setPrincipal(personNameWS.isPrincipal());
                personName.setSuffix(personNameWS.getSuffix());
                personName.setUse(personName.getUse());
                personNames.add(personName);
            }
            return personNames;
        }
    }

    private List<Address> convertAddressesWS(List<AddressWS> addressesWS) {
        if (addressesWS == null){
            return null;
        } else {
            return new ArrayList<Address>(addressesWS);
        }
    }
}
