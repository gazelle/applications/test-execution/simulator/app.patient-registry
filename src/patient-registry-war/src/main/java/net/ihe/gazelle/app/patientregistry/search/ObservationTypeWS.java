/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.search;

import net.ihe.gazelle.app.patientregistry.business.patient.ObservationType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * <p>ObservationTypeWS class.</p>
 *
 * @author abe
 * @version 1.0: 18/12/2019
 */

@XmlRootElement(name = "observationType")
@XmlAccessorType(XmlAccessType.NONE)
public class ObservationTypeWS implements ObservationType {

    @XmlElement(name = "label")
    private String label;

    public ObservationTypeWS(){

    }

    public ObservationTypeWS(String label){
        this.label = label;
    }

    @Override
    public String getLabel() {
        return this.label;
    }
}
