/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.search;

import net.ihe.gazelle.app.patientregistry.business.exceptions.MissingRequiredDataException;
import net.ihe.gazelle.app.patientregistry.business.patient.ContactPoint;
import net.ihe.gazelle.app.patientregistry.business.patient.GenderCode;
import net.ihe.gazelle.app.patientregistry.business.patient.PatientIdentifier;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * <p>MatchingPatient class.</p>
 *
 * @author abe
 * @version 1.0: 31/10/2019
 */

@XmlRootElement(name = "MatchingPatient")
@XmlAccessorType(XmlAccessType.FIELD)
public class MatchingPatient {

    @XmlElement(name = "active")
    private boolean active;

    @XmlElement(name = "uuid")
    private String uuid;

    //@XmlElementWrapper(name = "names")
    @XmlElement(name = "name")
    private Set<PersonNameWS> names;

    @XmlElement(name = "gender")
    private GenderCode gender;

    @XmlElement(name = "dateOfBirth")
    private Date dateOfBirth;

    //@XmlElementWrapper(name = "addresses")
    @XmlElement(name = "address")
    private Set<AddressWS> addresses;

    //@XmlElementWrapper(name = "contactPoints")
    @XmlElement(name = "contactPoint")
    private Set<ContactPoint> contactPoints;

    //@XmlElementWrapper(name = "observations")
    @XmlElement(name = "observation")
    private Set<ObservationWS> observations;

    //@XmlElementWrapper(name = "patientIdentifiers")
    @XmlElement(name = "identifiers")
    private Set<PatientIdentifier> patientIdentifiers;

    /*
    If the patient is part of a multiple birth, the rank of birth
     */
    @XmlElement(name = "multipleBirthOrder")
    private Integer multipleBirthOrder;

    /*
    If the patient is dead, the date/time of death
     */
    @XmlElement(name = "dateOfDeath")
    private Date dateOfDeath;

    public MatchingPatient() {
    }

    public Set<PersonNameWS> getNames() {
        return names;
    }

    public void setNames(Set<PersonNameWS> names) {
        this.names = names;
    }

    public GenderCode getGender() {
        return gender;
    }

    public void setGender(GenderCode gender) {
        this.gender = gender;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Set<AddressWS> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<AddressWS> addresses) {
        this.addresses = addresses;
    }

    public Set<ContactPoint> getContactPoints() {
        return contactPoints;
    }

    public void setContactPoints(Set<ContactPoint> contactPoints) {
        this.contactPoints = contactPoints;
    }

    public Set<ObservationWS> getObservations() {
        return observations;
    }

    public void setObservations(Set<ObservationWS> observations) {
        this.observations = observations;
    }

    public Integer getMultipleBirthOrder() {
        return multipleBirthOrder;
    }

    public void setMultipleBirthOrder(Integer multipleBirthOrder) {
        this.multipleBirthOrder = multipleBirthOrder;
    }

    public Date getDateOfDeath() {
        return dateOfDeath;
    }

    public void setDateOfDeath(Date dateOfDeath) {
        this.dateOfDeath = dateOfDeath;
    }

    public Set<PatientIdentifier> getPatientIdentifiers() {
        return patientIdentifiers;
    }

    public void setPatientIdentifiers(Set<PatientIdentifier> patientIdentifiers) {
        this.patientIdentifiers = patientIdentifiers;
    }

    @Override
    public String toString(){
        final StringBuilder sb = new StringBuilder("Person{");
        sb.append("names=").append(names);
        sb.append(", gender=").append(gender);
        sb.append(", dateOfBirth='").append(dateOfBirth).append('\'');
        sb.append(", addresses=").append(addresses);
        sb.append(", contactPoints=").append(contactPoints);
        sb.append(", multipleBirthOrder='").append(multipleBirthOrder).append('\'');
        sb.append(", deathDateTime='").append(dateOfDeath).append('\'');
        sb.append(", observations=").append(observations);
        sb.append(", patientIdentifiers=").append(patientIdentifiers);
        sb.append('}');
        return sb.toString();
    }

    public void addName(PersonNameWS personName) {
        if (this.names == null){
            this.names = new HashSet<PersonNameWS>();
        }
        this.names.add(personName);
    }

    public void addAddress(AddressWS address) {
        if (this.addresses == null){
            this.addresses = new HashSet<AddressWS>();
        }
        this.addresses.add(address);
    }

    public void addContactPoint(ContactPoint contactPoint) {
        if (this.contactPoints == null){
            this.contactPoints = new HashSet<ContactPoint>();
        }
        this.contactPoints.add(contactPoint);
    }

    public void addPatientIdentifier(PatientIdentifier returnedPatientIdentifier) {
        if (this.patientIdentifiers == null){
            this.patientIdentifiers = new HashSet<PatientIdentifier>();
        }
        this.patientIdentifiers.add(returnedPatientIdentifier);
    }

    public void addObservation(ObservationWS observationWS) throws MissingRequiredDataException {
        if (this.observations == null){
            this.observations = new HashSet<ObservationWS>();
        }
        this.observations.add(observationWS);
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
