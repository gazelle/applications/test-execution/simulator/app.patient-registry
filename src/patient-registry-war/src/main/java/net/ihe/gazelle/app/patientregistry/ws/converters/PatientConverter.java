/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.ws.converters;

import net.ihe.gazelle.app.patientregistry.business.exceptions.MissingRequiredDataException;
import net.ihe.gazelle.app.patientregistry.business.patient.Address;
import net.ihe.gazelle.app.patientregistry.business.patient.GenderCode;
import net.ihe.gazelle.app.patientregistry.business.patient.Observation;
import net.ihe.gazelle.app.patientregistry.business.patient.Patient;
import net.ihe.gazelle.app.patientregistry.business.patient.PersonName;
import net.ihe.gazelle.app.patientregistry.search.AddressWS;
import net.ihe.gazelle.app.patientregistry.search.MatchingPatient;
import net.ihe.gazelle.app.patientregistry.search.ObservationWS;
import net.ihe.gazelle.app.patientregistry.search.PersonNameCode;
import net.ihe.gazelle.app.patientregistry.search.PersonNameWS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * <p>PatientConverter class.</p>
 *
 * @author abe
 * @version 1.0: 03/12/2019
 */

public class PatientConverter {

    private static PatientConverter SINGLETON = new PatientConverter();

    public static PatientConverter getInstance() {
        return SINGLETON;
    }

    private static final Logger LOG = LoggerFactory.getLogger(PatientConverter.class);

    public List<MatchingPatient> convertIntoMatchingPatients(List<Patient> foundPatients) {
        if (foundPatients == null || foundPatients.isEmpty()) {
            return null;
        } else {
            List<MatchingPatient> matchingPatients = new ArrayList<MatchingPatient>();
            for (Patient patient : foundPatients) {
                MatchingPatient matchingPatient = convertPatient(patient);
                if (matchingPatient != null) {
                    matchingPatients.add(matchingPatient);
                }
            }
            return matchingPatients;
        }
    }

    public MatchingPatient convertPatient(Patient patient) {
        MatchingPatient matchingPatient = new MatchingPatient();
        matchingPatient.setPatientIdentifiers(patient.getIdentifiers());
        matchingPatient.setActive(patient.isActive());
        matchingPatient.setUuid(patient.getUuid());
        if (patient.getNames() != null) {
            for (PersonName personName : patient.getNames()) {
                addPersonName(personName, matchingPatient);
            }
        }
        setGender(patient, matchingPatient);
        matchingPatient.setDateOfBirth(patient.getDateOfBirth());
        matchingPatient.setMultipleBirthOrder(patient.getMultipleBirthOrder());
        matchingPatient.setDateOfDeath(patient.getDeathDateTime());
        if (patient.getAddresses() != null) {
            for (Address patientAddress : patient.getAddresses()) {
                addPatientAddress(patientAddress, matchingPatient);
            }
        }
        matchingPatient.setContactPoints(patient.getContactPoints());
        setObservations(patient, matchingPatient);
        return matchingPatient;
    }

    private void setObservations(Patient patient, MatchingPatient matchingPatient) {
        if (patient.getObservations() != null){
            for (Observation observation: patient.getObservations()){
                try {
                    matchingPatient.addObservation(new ObservationWS(observation.getObservationType().getLabel(), observation.getValue()));
                }catch (MissingRequiredDataException e){
                    LOG.info("Does not include observation, it is not complete");
                }
            }
        }
    }

    private void setGender(Patient patient, MatchingPatient matchingPatient) {
        if (patient.getGender() != null) {
            try {
                matchingPatient.setGender(GenderCode.valueOf(patient.getGender().getCode()));
            } catch (IllegalArgumentException e) {
                LOG.warn("Cannot find equivalend gender code for {}", patient.getGender());
            }
        }
    }

    private void addPersonName(PersonName name, MatchingPatient matchingPatient) {
        PersonNameWS nameWS = new PersonNameWS();
        nameWS.setGivens(new HashSet<>(name.getGivens()));
        nameWS.setFamily(name.getFamily());
        nameWS.setPrefix(name.getPrefix());
        nameWS.setSuffix(name.getSuffix());
        nameWS.setPrincipal(name.isPrincipal());
        nameWS.setUse(new PersonNameCode(name.getUse()));
        matchingPatient.addName(nameWS);
    }

    private void addPatientAddress(Address patientAddress, MatchingPatient matchingPatient) {
        AddressWS addressWS = new AddressWS();
        addressWS.setCity(patientAddress.getCity());
        addressWS.setCountryIso3(patientAddress.getCountryIso3());
        addressWS.setState(patientAddress.getState());
        addressWS.setPostalCode(patientAddress.getPostalCode());
        addressWS.setLines(patientAddress.getLines());
        matchingPatient.addAddress(addressWS);
    }
}
