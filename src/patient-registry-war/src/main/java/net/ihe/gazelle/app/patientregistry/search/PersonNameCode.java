/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.search;

import net.ihe.gazelle.app.patientregistry.business.patient.Code;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * <p>PersonNameCode class.</p>
 *
 * @author abe
 * @version 1.0: 18/12/2019
 */
@XmlRootElement(name = "personNameCode")
@XmlAccessorType(XmlAccessType.FIELD)
public class PersonNameCode implements Code {

    private String label;
    private String code;
    private String codingSystem;

    public PersonNameCode(){

    }

    public PersonNameCode(Code code){
        if (code != null) {
            this.label = code.getLabel();
            this.code = code.getCode();
            this.codingSystem = code.getCodingSystem();
        }
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setCodingSystem(String codingSystem) {
        this.codingSystem = codingSystem;
    }

    @Override
    public String getLabel() {
        return this.label;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getCodingSystem() {
        return this.codingSystem;
    }
}
