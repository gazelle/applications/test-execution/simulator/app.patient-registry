/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.ws;

import net.ihe.gazelle.app.patientregistry.application.search.PatientDAO;
import net.ihe.gazelle.app.patientregistry.application.search.SearchPatientService;
import net.ihe.gazelle.app.patientregistry.business.exceptions.UnknownRequestedDomainException;
import net.ihe.gazelle.app.patientregistry.business.patient.Patient;
import net.ihe.gazelle.app.patientregistry.business.search.PatientSearchCriteria;
import net.ihe.gazelle.app.patientregistry.search.MatchingPatient;
import net.ihe.gazelle.app.patientregistry.search.SearchError;
import net.ihe.gazelle.app.patientregistry.search.wrapper.PatientSearchRequestWrapper;
import net.ihe.gazelle.app.patientregistry.search.wrapper.SearchResponseWrapper;
import net.ihe.gazelle.app.patientregistry.ws.converters.PatientConverter;
import net.ihe.gazelle.app.patientregistry.ws.converters.PatientSearchCriteriaWSConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>PatientSearchImpl class.</p>
 *
 * @author abe
 * @version 1.0: 31/10/2019
 */
@Named("patientSearch")
public class PatientSearchImpl implements PatientSearchAPI {

    @Inject
    private PatientDAO patientDAO;

    private static final Logger LOG = LoggerFactory.getLogger(PatientSearchImpl.class);

    public PatientSearchImpl(){
    }

    PatientSearchImpl(PatientDAO patientDAO){
        this.patientDAO = patientDAO;
    }

    public Response searchForPatients(PatientSearchRequestWrapper patientSearchRequestWrapper) {
        if (patientSearchRequestWrapper == null || patientSearchRequestWrapper.hashCode() == 0) {
            // we don't want to process a request with no criteria at all
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            List<Patient> foundPatients;
            SearchResponseWrapper searchResponseWrapper = new SearchResponseWrapper();
            try {
                PatientSearchCriteria searchCriteria =
                        PatientSearchCriteriaWSConverter.getInstance().toPatientSearchCriteria(patientSearchRequestWrapper.getPatientSearchCriteria());
                SearchPatientService searchPatientService = new SearchPatientService(patientDAO);
                foundPatients = searchPatientService.getPatientsForCriteria(searchCriteria,
                        patientSearchRequestWrapper.getRestrictedDomains());

                List<MatchingPatient> matchingPatients = PatientConverter.getInstance().convertIntoMatchingPatients(foundPatients);
                searchResponseWrapper.setPatients(matchingPatients);
                return Response.ok(searchResponseWrapper).build();
            } catch (IllegalArgumentException e) {
                LOG.error("Exception occurred while processing the request", e);
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } catch (UnknownRequestedDomainException e) {
                buildUnknownDomainExceptionResponse(e, searchResponseWrapper);
                return Response.status(Response.Status.NOT_FOUND).entity(searchResponseWrapper).build();
            } catch (NullPointerException e) {
                LOG.info("Patient search failed with message: {}", e.getMessage(), e);
                return Response.status(Response.Status.BAD_REQUEST).build();
            }
        }
    }

    public Response searchPatientByUniqueId(String uuid) {
    if (uuid == null || uuid.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            try {
                SearchPatientService searchPatientService = new SearchPatientService(patientDAO);
                Patient patient = searchPatientService.getPatientsByUUID(uuid);
                if (patient == null) {
                    return Response.status(Response.Status.NOT_FOUND).build();
                } else {
                    MatchingPatient matchingPatient = PatientConverter.getInstance().convertPatient(patient);
                    SearchResponseWrapper wrapper = new SearchResponseWrapper(matchingPatient);
                    return Response.ok(wrapper).build();
                }
            }  catch (IllegalArgumentException e) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            } catch (RuntimeException e) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
        }
    }

    // TODO implement about
    @Override
    public Response about() {
        return null;
    }


    private void buildUnknownDomainExceptionResponse(UnknownRequestedDomainException exception, SearchResponseWrapper searchResponseWrapper) {
        List<SearchError> errors = new ArrayList<SearchError>();
        for (String unknownDomain : exception.getUnknownDomains()) {
            SearchError searchError = new SearchError();
            searchError.setCause(unknownDomain);
            searchError.setErrorMessage("Unknown domains");
            errors.add(searchError);
        }
        searchResponseWrapper.setSearchErrors(errors);
    }
}
