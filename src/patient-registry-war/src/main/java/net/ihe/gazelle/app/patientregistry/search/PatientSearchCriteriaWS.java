/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.search;

import net.ihe.gazelle.app.patientregistry.business.patient.ContactPoint;
import net.ihe.gazelle.app.patientregistry.business.search.DateInterval;
import net.ihe.gazelle.app.patientregistry.business.search.GenderCode;
import net.ihe.gazelle.app.patientregistry.business.search.IdentifierSearchCriteria;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>PatientCriteria class.</p>
 *
 * @author abe
 * @version 1.0: 03/11/2019
 */
@XmlRootElement(name = "patientSearchCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
public class PatientSearchCriteriaWS {

    //@XmlElementWrapper(name = "personNames")
    @XmlElement(name = "personName")
    private List<PersonNameWS> personNames;

    private String mothersMaidenName;

    private DateInterval dateOfBirth;

    private DateInterval dateOfDeath;

    private Boolean deceased;

    private GenderCode genderCode;

   // @XmlElementWrapper(name = "identifiers")
    @XmlElement(name = "identifier")
    private List<IdentifierSearchCriteria> identifiers;

    private IdentifierSearchCriteria accountNumber;

   // @XmlElementWrapper(name = "addresses")
    @XmlElement(name = "address")
    private List<AddressWS> addresses;

    //@XmlElementWrapper(name = "contactPoints")
    @XmlElement(name = "contactPoint")
    private List<ContactPoint> contactPoints;

    private Boolean active;

    public PatientSearchCriteriaWS() {
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getMothersMaidenName() {
        return mothersMaidenName;
    }

    public void setMothersMaidenName(String mothersMaidenName) {
        this.mothersMaidenName = mothersMaidenName;
    }

    public DateInterval getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(DateInterval dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public GenderCode getGenderCode() {
        return genderCode;
    }

    public void setGenderCode(GenderCode genderCode) {
        this.genderCode = genderCode;
    }

    public List<IdentifierSearchCriteria> getIdentifiers() {
        if (identifiers == null) {
            identifiers = new ArrayList<>();
        }
        return identifiers;
    }

    public void setIdentifiers(List<IdentifierSearchCriteria> identifiers) {
        this.identifiers = identifiers;
    }

    public IdentifierSearchCriteria getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(IdentifierSearchCriteria accountNumber) {
        this.accountNumber = accountNumber;
    }

    public List<AddressWS> getAddresses() {
        if (addresses == null) {
            addresses = new ArrayList<>();
        }
        return addresses;
    }

    public void setAddresses(List<AddressWS> addresses) {
        this.addresses = addresses;
    }

    public List<ContactPoint> getContactPoints() {
        if (contactPoints == null) {
            contactPoints = new ArrayList<>();
        }
        return contactPoints;
    }

    public void setContactPoints(List<ContactPoint> contactPoints) {
        this.contactPoints = contactPoints;
    }

    public List<PersonNameWS> getPersonNames() {
        if (personNames == null) {
            personNames = new ArrayList<>();
        }
        return personNames;
    }

    public void setPersonNames(List<PersonNameWS> personNames) {
        this.personNames = personNames;
    }

    public DateInterval getDateOfDeath() {
        return dateOfDeath;
    }

    public void setDateOfDeath(DateInterval dateOfDeath) {
        this.dateOfDeath = dateOfDeath;
    }

    public Boolean getDeceased() {
        return deceased;
    }

    public void setDeceased(Boolean deceased) {
        this.deceased = deceased;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PatientSearchCriteriaWS)) {
            return false;
        }

        PatientSearchCriteriaWS that = (PatientSearchCriteriaWS) o;

        if (personNames != null ? !personNames.equals(that.personNames) : that.personNames != null) {
            return false;
        }
        if (mothersMaidenName != null ? !mothersMaidenName.equals(that.mothersMaidenName) : that.mothersMaidenName != null) {
            return false;
        }
        if (dateOfBirth != null ? !dateOfBirth.equals(that.dateOfBirth) : that.dateOfBirth != null) {
            return false;
        }
        if (dateOfDeath != null ? !dateOfDeath.equals(that.dateOfDeath) : that.dateOfDeath != null) {
            return false;
        }
        if (deceased != null ? !deceased.equals(that.deceased) : that.deceased != null) {
            return false;
        }
        if (genderCode != that.genderCode) {
            return false;
        }
        if (identifiers != null ? !identifiers.equals(that.identifiers) : that.identifiers != null) {
            return false;
        }
        if (accountNumber != null ? !accountNumber.equals(that.accountNumber) : that.accountNumber != null) {
            return false;
        }
        if (addresses != null ? !addresses.equals(that.addresses) : that.addresses != null) {
            return false;
        }
        if (contactPoints != null ? !contactPoints.equals(that.contactPoints) : that.contactPoints != null) {
            return false;
        }
        return active != null ? active.equals(that.active) : that.active == null;
    }

    @Override
    public int hashCode() {
        int result = personNames != null ? personNames.hashCode() : 0;
        result = 31 * result + (mothersMaidenName != null ? mothersMaidenName.hashCode() : 0);
        result = 31 * result + (dateOfBirth != null ? dateOfBirth.hashCode() : 0);
        result = 31 * result + (dateOfDeath != null ? dateOfDeath.hashCode() : 0);
        result = 31 * result + (deceased != null ? deceased.hashCode() : 0);
        result = 31 * result + (genderCode != null ? genderCode.hashCode() : 0);
        result = 31 * result + (identifiers != null ? identifiers.hashCode() : 0);
        result = 31 * result + (accountNumber != null ? accountNumber.hashCode() : 0);
        result = 31 * result + (addresses != null ? addresses.hashCode() : 0);
        result = 31 * result + (contactPoints != null ? contactPoints.hashCode() : 0);
        result = 31 * result + (active != null ? active.hashCode() : 0);
        return result;
    }
}
