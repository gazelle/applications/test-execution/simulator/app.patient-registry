/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.ws;

import net.ihe.gazelle.app.patientregistry.search.wrapper.PatientSearchRequestWrapper;

import javax.ejb.Local;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 * <p>PatientSearchAPI interface.</p>
 *
 * @author abe
 * @version 1.0: 31/10/2019
 */
@Local
@Path("/patient/search")
public interface PatientSearchAPI {

    @POST
    @Path("/filter")
    @Produces("application/xml")
    Response searchForPatients(PatientSearchRequestWrapper patientSearchRequestWrapper);

    @GET
    @Path("/{uuid}")
    @Produces("application/xml")
    Response searchPatientByUniqueId(@PathParam("uuid") String uniqueIdentifier);

    @GET
    Response about();
}
