/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.search.wrapper;

import net.ihe.gazelle.app.patientregistry.search.PatientSearchCriteriaWS;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * <p>PatientSearchRequestWrapper class.</p>
 *
 * @author abe
 * @version 1.0: 20/11/2019
 */


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PatientSearchRequestWrapper {

    @XmlElement(name="patientCriteria")
    private PatientSearchCriteriaWS patientSearchCriteria;

    //@XmlElementWrapper(name = "restrictedDomains")
    @XmlElement(name = "restrictedDomain")
    private List<String> restrictedDomains;


    public PatientSearchRequestWrapper(){

    }

    public PatientSearchCriteriaWS getPatientSearchCriteria() {
        return patientSearchCriteria;
    }

    public void setPatientSearchCriteria(PatientSearchCriteriaWS patientSearchCriteria) {
        this.patientSearchCriteria = patientSearchCriteria;
    }

    public List<String> getRestrictedDomains() {
        return restrictedDomains;
    }

    public void setRestrictedDomains(List<String> restrictedDomains) {
        this.restrictedDomains = restrictedDomains;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PatientSearchRequestWrapper)) {
            return false;
        }

        PatientSearchRequestWrapper that = (PatientSearchRequestWrapper) o;

        if (patientSearchCriteria != null ? !patientSearchCriteria.equals(that.patientSearchCriteria) : that.patientSearchCriteria != null) {
            return false;
        }
        return restrictedDomains != null ? restrictedDomains.equals(that.restrictedDomains) : that.restrictedDomains == null;
    }

    @Override
    public int hashCode() {
        int result = patientSearchCriteria != null ? patientSearchCriteria.hashCode() : 0;
        result = 31 * result + (restrictedDomains != null ? restrictedDomains.hashCode() : 0);
        return result;
    }
}
