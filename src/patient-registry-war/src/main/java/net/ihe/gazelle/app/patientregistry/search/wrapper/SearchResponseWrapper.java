/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.search.wrapper;

import net.ihe.gazelle.app.patientregistry.search.MatchingPatient;
import net.ihe.gazelle.app.patientregistry.search.SearchError;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>SearchResponseWrapper class.</p>
 *
 * @author abe
 * @version 1.0: 20/11/2019
 */

@XmlRootElement(name = "SearchResponseWrapper")
@XmlAccessorType(XmlAccessType.FIELD)
public class SearchResponseWrapper {

    //@XmlElementWrapper(name = "patients")
    @XmlElement(name="patient")
    private List<MatchingPatient> patients;

    //@XmlElementWrapper(name = "searchErrors")
    @XmlElement(name = "searchError")
    private List<SearchError> searchErrors;

    public List<MatchingPatient> getPatients() {
        return patients;
    }

    public void setPatients(List<MatchingPatient> patients) {
        this.patients = patients;
    }

    public List<SearchError> getSearchErrors() {
        return searchErrors;
    }

    public void setSearchErrors(List<SearchError> searchErrors) {
        this.searchErrors = searchErrors;
    }

    public SearchResponseWrapper(){

    }

    public SearchResponseWrapper(MatchingPatient patient){
        this.patients = new ArrayList<MatchingPatient>();
        this.patients.add(patient);
    }

}
