/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.ws;

/**
 * <p>WSApplication class.</p>
 *
 * @author abe
 * @version 1.0: 16/12/2019
 */

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * our Application
 */
@ApplicationPath("/")
public class WSApplication extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> wsClasses = new HashSet<>();
        wsClasses.add(PatientSearchImpl.class);
        return wsClasses;
    }

}
