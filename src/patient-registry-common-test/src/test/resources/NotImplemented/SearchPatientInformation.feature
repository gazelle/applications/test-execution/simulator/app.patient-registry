Feature: Search Patient information in any domain

  Background:
    Given the following assigning authorities exist
      | systemIdentifier | systemName |
      | urn:oid:1.2.3.4  | AA1        |
      | urn:oid:5.6.7.8  | AA2        |
      | urn:oid:9.8.7.6  | AA3        |
    Given the following patients exist
      | uuid  | LastName | FirstName  | Gender | active | birthdate  |
      | uuid0 | Bars     | Alain      | MALE   | true   | 1984-09-19 |
      | uuid1 | Bambou   | Albert     | MALE   | true   | 1929-12-01 |
      | uuid2 | Tambour  | Marjolaine | FEMALE | true   | 2019-01-19 |
      | uuid3 | Argh     | Ced        | MALE   | false  | 1984-09-29 |
    And patients have the following addresses
      | uuid  | line1                | line2         | city               | countryIso3 | postalCode | state   |
      | uuid0 | 4 Rue Hélène-Boucher | Z.A. Bellevue | Thorigné-Fouillard | FRA         | 35325      |         |
      | uuid1 | 1 Rue des vignes     |               | Thorigné-Fouillard | FRA         | 35325      |         |
      | uuid2 | 2 Rue de Paris       |               | Rennes             | FRA         | 35000      |         |
      | uuid3 | 88C south St         |               | Perth              | GBR         | PH2 8PD    | England |
    And patients have the following patient identifiers
      | uuid  | systemIdentifier | value       |
      | uuid0 | urn:oid:1.2.3.4  | identifier0 |
      | uuid0 | urn:oid:5.6.7.8  | identifier0 |
      | uuid1 | urn:oid:1.2.3.4  | identifier1 |
      | uuid2 | urn:oid:1.2.3.4  | identifier2 |
      | uuid3 | urn:oid:1.2.3.4  | identifier3 |
      | uuid3 | urn:oid:9.8.7.6  | identifier0 |
    And patients have the following telecom
      | uuid  | system | value              |
      | uuid0 | email  | alain@testpdqm.com |
      | uuid1 | phone  | 02 44 55 44 66     |
      | uuid2 | phone  | 555-444-666        |
      | uuid3 | email  | ced@testpdql.com   |


  Scenario Outline: Nominal Patient search with one criterion
    Given search criteria "<parameter>" "<verb>" "<value>"
    When search is done
    Then received HTTP status code 200
    And received <resultSize> patients
    And all received patients "<parameter>" "<verb>" "<value>"

    Examples: Simple cases
      | parameter  | verb       | value                        | resultSize |
      | LastName   | startsWith | Ba                           | 2          |
      | LastName   | contains   | ar                           | 2          |
      | LastName   | is         | Bars                         | 1          |
      | FirstName  | contains   | ai                           | 2          |
      | FirstName  | is         | Alain                        | 1          |
      | Gender     | is         | female                       | 1          |
      | Identifier | is         | urn:oid:1.2.3.4\|identifier0 | 1          |
      | Identifier | is         | identifier0                  | 3          |
      | Address    | contains   | Rennes                       | 1          |
      | City       | is         | Thorigné-Fouillard           | 2          |
      | PostalCode | startsWith | 35                           | 3          |
      | Country    | is         | GBR                          | 1          |
      | State      | is         | England                      | 1          |
      | uuid       | is         | uuid3                        | 1          |
      | Active     | is         | false                        | 1          |
      | BirthDate  | is         | eq2019-01-19                 | 1          |
      | Telecom    | is         | alain@testpdqm.com           | 1          |
      | Telecom    | is         | 555444666                    | 1          |

    Examples: Case and accent insensitive
      | parameter | verb       | value              | resultSize |
      | FirstName | startsWith | al                 | 2          |
      | LastName  | is         | argh               | 1          |
      | Address   | contains   | rue helene-boucher | 1          |


  Scenario Outline: Patient search with one criterion but no result
    Given search criteria "<parameter>" "<verb>" "<value>"
    When search is done
    Then received HTTP status code 200
    But received 0 patients

    Examples:
      | parameter   | verb     | value |
      | LastName    | is       | toto  |
      | FirstName   | contains | Al-   |
      | Gender      | is       | other |
      | AddressCity | contains | e F   |


  Scenario Outline: Nominal patient search with 3 criteria

    Given search criteria <criteria1>
    And search criteria <criteria2>
    And search criteria <criteria3>
    When search is done
    Then received HTTP status code 200
    And received <resultSize> patients
    And all received patients <criteria1>
    And all received patients <criteria2>
    And all received patients <criteria3>

    Examples:
      | criteria1                       | criteria2                       | criteria3             | resultSize |
      | Firstname,contains,ai           | Gender,is,female                |                       | 1          |
      | BirthDate,is,ge2000             | BirthDate,is,le2020             |                       | 1          |
      | BirthDate,is,gt1984-09          | BirthDate,is,lt1984-11          |                       | 2          |
      | AddressPostalCode,startsWith,35 | Identifier,is,urn:oid:1.2.3.4\| | LastName,contains,bou | 1          |

  Scenario Outline: Patient search with 3 independent matching criteria but no result when associated

    Given search criteria <criteria1>
    And search criteria <criteria2>
    And search criteria <criteria3>
    When search is done
    Then received HTTP status code 200
    But received 0 patients

    Examples:
      | criteria1      | criteria2               | criteria3             | resultSize |
      | Gender,is,male | FirstName,contains,lain | LastName,contains,bou | 0          |

  Scenario: Patient search is performed with no criteria at all
    Given search criteria "" "" ""
    When search is done
    Then received HTTP status code 400


Feature: Search Patient information in a domain

  Background:
    Given the following patients exist:
      | uuid  | LastName | FirstName  |
      | uuid0 | Bars     | Alain      |
      | uuid1 | Bambou   | Albert     |
      | uuid2 | Tambour  | Marjolaine |
      | uuid3 | Argh     | Ced        |
    And patients have the following identifiers:
      | uuid  | domain          | identifier  |
      | uuid0 | urn:oid:1.2.3.4 | identifier0 |
      | uuid0 | urn:oid:5.6.7.8 | identifier0 |
      | uuid1 | urn:oid:1.2.3.4 | identifier1 |
      | uuid2 | urn:oid:1.2.3.4 | identifier2 |
      | uuid3 | urn:oid:1.2.3.4 | identifier3 |
      | uuid3 | urn:oid:5.6.7.8 | identifier1 |
      | uuid3 | urn:oid:9.8.7.6 | identifier0 |


  Scenario Outline: Nominal Patient search with an explicit domain
    Given search criteria "Identifier" "is" "urn:oid:1.2.3.4|"
    And search criteria "<parameter>" "<verb>" "<value>"
    When search is done
    Then received HTTP status code 200
    And received <resultSize> patients
    But no received patients "Identifier" "is" "urn:oid:5.6.7.8|"
    And no received patients "Identifier" "is" "urn:oid:9.8.7.6|"

    Examples:
      | parameter | verb       | value | resultSize |
      | LastName  | starts with | Ba    | 2          |
      | FirstName | is         | Ced   | 1          |

  Scenario: Patient search matches but no identifier in the requested domain
    Given search criteria "Identifier" "is" "urn:oid:5.6.7.8|"
    And search criteria "LastName" "is" "Bambou"
    When search is done
    Then received HTTP status code 200
    But received 0 patients

  Scenario : Patient search in a not recognized domain
    Given search criteria "Identifier" "is" "urn:oid:2.2.2.2|"
    And search criteria "LastName" "starts with" "Ba"
    When search is done
    Then received HTTP status code 404

    # GAZELLE REQUIREMENT : Patient search with only domain restrictions as criteria should be rejected, as it is assimilated to a database dump
  Scenario: Patient search with only one domain restriction as criteria should be rejected
    Given search criteria "Identifier" "is" "urn:oid:1.2.3.4|"
    When search is done
    Then received HTTP status code 400



Feature: Search Patient information with several domains

  Background:
    Given the following patients exist:
      | uuid  | LastName | FirstName  |
      | uuid0 | Bars     | Alain      |
      | uuid1 | Bambou   | Albert     |
      | uuid2 | Tambour  | Marjolaine |
      | uuid3 | Argh     | Ced        |
    And patients have the following identifiers:
      | uuid  | domain          | identifier  |
      | uuid0 | urn:oid:1.2.3.4 | identifier0 |
      | uuid0 | urn:oid:5.6.7.8 | identifier0 |
      | uuid1 | urn:oid:1.2.3.4 | identifier1 |
      | uuid2 | urn:oid:1.2.3.4 | identifier2 |
      | uuid3 | urn:oid:1.2.3.4 | identifier3 |
      | uuid3 | urn:oid:5.6.7.8 | identifier1 |
      | uuid3 | urn:oid:9.8.7.6 | identifier0 |


  Scenario Outline: Nominal Patient search in several domain
    Given search criteria "<parameter>" "<verb>" "<value>"
    And search criteria "Identifier" "is" "urn:oid:5.6.7.8|,urn:oid:9.8.7.6|"
    When search is done
    Then received HTTP status code 200
    And received <resultSize> patients
    And all received patients "<parameter>" "<verb>" "<value>"
    But no received patients "Identifier" "is" "urn:oid:1.2.3.4|"

    Examples:
      | parameter | verb       | value | resultSize |
      | LastName  | starts with | Ba    | 2          |


      # GAZELLE REQUIREMENT : Patient search with only domain restrictions as criteria should be rejected, as it is assimilated to a database dump
  Scenario: Patient search with only domain restrictions as criteria should be rejected
    Given search criteria "Identifier" "is" "urn:oid:1.2.3.4|,urn:oid:5.6.7.8|"
    When search is done
    Then received HTTP status code 400

