Feature: Testing patient registry get patients

  Background:
    Given the following assigning authorities exist
      | systemIdentifier | systemName |
      | 1.2.3.4          | AA1        |
      | 5.6.7.8          | AA2        |
      | 9.8.7.6          | AA3        |
    Given the following patients exist
      | uuid  | LastName | FirstName  | Gender |
      | uuid0 | Bars     | Alain      | MALE   |
      | uuid1 | Bambou   | Albert     | MALE   |
      | uuid2 | Tambour  | Marjolaine | FEMALE |
      | uuid3 | Argh     | Ced        | MALE   |
    And patients have the following addresses
      | uuid  | line1                | line2         | city               | countryIso3 | postalCode | state   |
      | uuid0 | 4 Rue Hélène-Boucher | Z.A. Bellevue | Thorigné-Fouillard | FRA         | 35325      |         |
      | uuid1 | 1 Rue des vignes     |               | Thorigné-Fouillard | FRA         | 35325      |         |
      | uuid2 | 2 Rue de Paris       |               | Rennes             | FRA         | 35000      |         |
      | uuid3 | 88C south St         |               | Perth              | GBR         | PH2 8PD    | England |
    And patients have the following patient identifiers
      | uuid  | systemIdentifier | value       |
      | uuid0 | 1.2.3.4          | identifier0 |
      | uuid0 | 5.6.7.8          | identifier0 |
      | uuid1 | 1.2.3.4          | identifier1 |
      | uuid2 | 1.2.3.4          | identifier2 |
      | uuid3 | 1.2.3.4          | identifier3 |
      | uuid3 | 9.8.7.6          | identifier0 |

  Scenario: Retrieve patient
    When retrieve is made on id "uuid0"
    Then received HTTP status code 200
    And received 1 patients

  Scenario: Retrieve patient and read content
    When retrieve is made on id "uuid0"
    Then received HTTP status code 200
    And received 1 patients
    And received patient 1 has identifier "identifier0" from assigning authority "1.2.3.4"
    # And received patient 1 has "LastName" "is" "Bars"
    # And reveived patient 1 has "FirstName" "is" "Alain"
    # And received patient 1 has "Gender" "is" "MALE"
    # And received patient 1 has "Address" "contains" "4 Rue Hélène-Boucher"
    # And received patient 1 has "City" "is" "Thorigné-Fouillard"
    # And received patient 1 has "Country" "is" "FRA"
    # And received patient 1 has "PostalCode" "is" "35325"

  Scenario Outline: Nominal Patient search with one criterion
    Given search criteria "<parameter>" "<verb>" "<value>"
    When search is done
    Then received HTTP status code 200
    And received <resultSize> patients


    Examples: Simple cases
      | parameter  | verb        | value              | resultSize |
      | LastName   | starts with | Ba                 | 2          |
      | LastName   | contains    | ar                 | 2          |
      | LastName   | is          | Bars               | 1          |
      | FirstName  | contains    | ai                 | 2          |
      | FirstName  | is          | Alain              | 1          |
      | Gender     | is          | female             | 1          |
      | Address    | contains    | 2 Rue de Paris     | 1          |
      | City       | is          | Thorigné-Fouillard | 2          |
      | PostalCode | starts with | 35                 | 3          |
      | Country    | is          | GBR                | 1          |
      | State      | is          | England            | 1          |

    Examples: Case  insensitive
      | parameter | verb        | value | resultSize |
      | FirstName | starts with | al    | 2          |
      | LastName  | is          | argh  | 1          |