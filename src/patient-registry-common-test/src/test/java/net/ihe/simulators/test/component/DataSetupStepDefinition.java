package net.ihe.simulators.test.component;

import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.BeforeStep;
import io.cucumber.java.en.Given;
import net.ihe.gazelle.app.patientregistry.application.search.PatientDAO;
import net.ihe.gazelle.app.patientregistry.business.exceptions.MissingRequiredDataException;
import net.ihe.gazelle.app.patientregistry.business.patient.Address;
import net.ihe.gazelle.app.patientregistry.business.patient.GenderCode;
import net.ihe.gazelle.app.patientregistry.business.patient.Patient;
import net.ihe.gazelle.app.patientregistry.business.patient.PatientIdentifier;
import net.ihe.gazelle.app.patientregistry.business.patient.PersonName;
import net.ihe.gazelle.app.patientregistry.db.adapters.PatientDAOImpl;
import net.ihe.gazelle.app.patientregistry.db.adapters.PatientTestDataFeedDAOImpl;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DataSetupStepDefinition {

   private static final String PERSISTENCE_UNIT_NAME = "patientRegistryRealDatabasePU";

   PatientTestDataFeedDAO patientTestDataFeedDAO;
   PatientDAO patientDAO;
   EntityManager entityManager;

   @Before
   public void setUpInMemoryDB() {
      entityManager = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME).createEntityManager();
      patientTestDataFeedDAO = new PatientTestDataFeedDAOImpl(entityManager);
      patientDAO = new PatientDAOImpl(entityManager);
   }

   @After
   public void tearDownInMemoryDB() {

      entityManager.close();
      patientTestDataFeedDAO = null;
      entityManager = null;
   }

   @BeforeStep
   public void startTransaction() {
      entityManager.getTransaction().begin();
   }

   @AfterStep
   public void endTransaction() {
      entityManager.getTransaction().commit();
   }

   @Given("the following assigning authorities exist")
   public void feedAssigningAuthorities(List<Map<String, String>> assigningAuthorityDataList) {
      for (Map<String, String> assigningAuthorityData : assigningAuthorityDataList) {
         patientTestDataFeedDAO.createAssigningAuthority(assigningAuthorityData.get("systemName"), assigningAuthorityData.get("systemIdentifier"));
      }
   }

   @Given("the following patients exist")
   public void feedPatients(List<Map<String, String>> patientDataList) {
      for (Map<String, String> patientData : patientDataList) {

         Patient patient = new Patient();
         patient.setUuid(patientData.get("uuid"));

         PersonName personName = new PersonName();
         personName.setFamily(patientData.get("LastName"));
         List<String> given = new ArrayList<>();
         given.add(patientData.get("FirstName"));
         personName.setGivens(given);
         patient.getNames().add(personName);

         patient.setGender(GenderCode.valueOf(patientData.get("Gender")));

         patientTestDataFeedDAO.createPatient(patient);

      }
   }

   @Given("patients have the following addresses")
   public void feedPatientAddress(List<Map<String, String>> patientAddressDataList) {
      for (Map<String, String> addressData : patientAddressDataList) {
         String patientUUID = addressData.get("uuid");

         Address address = new Address();
         Set<String> lines = new HashSet<>();
         lines.add(addressData.get("line1"));
         lines.add(addressData.get("line2"));
         address.setLines(lines);
         address.setCity(addressData.get("city"));
         address.setCountryIso3(addressData.get("countryIso3"));
         address.setPostalCode(addressData.get("postalCode"));
         address.setState(addressData.get("state"));

         patientTestDataFeedDAO.addAddress(patientUUID, address);
      }
   }

   @Given("patients have the following patient identifiers")
   public void feedPatientIdentifiers(List<Map<String, String>> patientIdentifierDataList) throws MissingRequiredDataException {
      for (Map<String, String> identifierData : patientIdentifierDataList) {
         String patientUUID = identifierData.get("uuid");

         PatientIdentifier patientIdentifier = new PatientIdentifier(identifierData.get("value"), identifierData.get("systemIdentifier"));

         patientTestDataFeedDAO.addPatientIdentifier(patientUUID, patientIdentifier);
      }
   }

}
