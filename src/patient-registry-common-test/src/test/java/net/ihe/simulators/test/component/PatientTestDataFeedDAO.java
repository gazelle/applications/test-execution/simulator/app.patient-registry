package net.ihe.simulators.test.component;

import net.ihe.gazelle.app.patientregistry.business.patient.Address;
import net.ihe.gazelle.app.patientregistry.business.patient.Patient;
import net.ihe.gazelle.app.patientregistry.business.patient.PatientIdentifier;

public interface PatientTestDataFeedDAO {

   void createPatient(Patient patient);

   void createAssigningAuthority(String systemName, String systemIdentifier);

   void addAddress(String patientUUID, Address address);

   void addPatientIdentifier(String patientUUID, PatientIdentifier patientIdentifier);
}
