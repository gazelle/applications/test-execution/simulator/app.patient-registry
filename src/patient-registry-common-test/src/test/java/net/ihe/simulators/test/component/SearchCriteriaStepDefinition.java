package net.ihe.simulators.test.component;

import io.cucumber.java.en.Given;
import jdk.jshell.spi.ExecutionControl;
import net.ihe.gazelle.app.patientregistry.business.patient.ContactPoint;
import net.ihe.gazelle.app.patientregistry.business.patient.GenderCode;
import net.ihe.gazelle.app.patientregistry.business.search.DateInterval;
import net.ihe.gazelle.app.patientregistry.business.search.IdentifierSearchCriteria;
import net.ihe.gazelle.app.patientregistry.search.AddressWS;
import net.ihe.gazelle.app.patientregistry.search.PatientSearchCriteriaWS;
import net.ihe.gazelle.app.patientregistry.search.PersonNameWS;
import net.ihe.gazelle.app.patientregistry.search.wrapper.PatientSearchRequestWrapper;

import static org.junit.Assert.assertNotNull;

public class SearchCriteriaStepDefinition {

    public PatientSearchRequestWrapper criterias;

    public SearchCriteriaStepDefinition(PatientSearchRequestWrapper criterias) {
        this.criterias = criterias;
        this.criterias.setPatientSearchCriteria(new PatientSearchCriteriaWS());
    }


    @Given("search criteria {string} {string} {string}")
    public void searchCriteriaIs(String searchCriteriaName, String searchCriteriaVerbText, String searchCriteriaValue) {
        SearchCriteriaVerb verb = SearchCriteriaVerb.fromString(searchCriteriaVerbText);
        assertNotNull(verb);
        SearchCriteriaName name = SearchCriteriaName.fromString(searchCriteriaName);
        assertNotNull(name);
        String actualizedValue = this.applyVerb(verb, searchCriteriaValue);
        assertNotNull(actualizedValue);

        applyActualizedValue(name, actualizedValue);

    }

    private String applyVerb(SearchCriteriaVerb verb, String searchCriteriaValue) {
        switch (verb) {
            case EQUALS:
                return searchCriteriaValue;
            case STARTWITH:
                return searchCriteriaValue + "*";
            case CONTAINS:
                return "*" + searchCriteriaValue + "*";
            case BOGUS:
            case DEFAULT:
            default:
                return searchCriteriaValue;
        }
    }

    private void applyActualizedValue(SearchCriteriaName name, String actualizedValue) {
        PatientSearchCriteriaWS searchCriteria = this.criterias.getPatientSearchCriteria();
        AddressWS address = new AddressWS();
        PersonNameWS personName = new PersonNameWS();
        switch (name) {
//            case ACTIVE:
//                searchCriteria.setActive(Boolean.valueOf(actualizedValue));
//                break;
            case ADDRESS:
                address.addLine(actualizedValue);
                searchCriteria.getAddresses().add(address);
                break;
            case ADDRESS_CITY:
                address.setCity(actualizedValue);
                searchCriteria.getAddresses().add(address);
                break;
            case ADDRESS_COUNTRY:
                address.setCountryIso3(actualizedValue);
                searchCriteria.getAddresses().add(address);
                break;
            case ADDRESS_POSTALCODE:
                address.setPostalCode(actualizedValue);
                searchCriteria.getAddresses().add(address);
                break;
            case ADDRESS_STATE:
                address.setState(actualizedValue);
                searchCriteria.getAddresses().add(address);
                break;
//            case BIRTHDATE:
//                // TODO to implement
//                break;
            case FAMILY:
                personName.setFamily(actualizedValue);
                searchCriteria.getPersonNames().add(personName);
                break;
            case GENDER:
                searchCriteria.setGenderCode(net.ihe.gazelle.app.patientregistry.business.search.GenderCode.valueOf(GenderCode.fromString(actualizedValue)));
                break;
            case GIVEN:
                personName.addGiven(actualizedValue);
                searchCriteria.getPersonNames().add(personName);
                break;
//            case ID:
//                // TODO to implement
//                break;
//            case IDENTIFIER:
//                IdentifierSearchCriteria identifier = new IdentifierSearchCriteria();
//                // TODO to implement
//                searchCriteria.getIdentifiers().add(identifier);
//                break;
//            case TELECOM:
//                ContactPoint contactPoint = new ContactPoint();
//                contactPoint.setValue(actualizedValue);
//                // TODO Check type
//                searchCriteria.getContactPoints().add(contactPoint);
//                break;
        }
    }

    public enum SearchCriteriaVerb {
        EQUALS("is", "name"),
        CONTAINS("contains", "name"),
        STARTWITH("starts with", "id"),
        BOGUS("bogus", "bogus"),
        DEFAULT("default", "default");

        private String text;
        private String queryParam;

        SearchCriteriaVerb(String text, String queryParam) {
            this.text = text;
            this.queryParam = queryParam;
        }

        public static SearchCriteriaVerb fromString(String text) {
            for (SearchCriteriaVerb b : SearchCriteriaVerb.values()) {
                if (b.text.equalsIgnoreCase(text)) {
                    return b;
                }
            }
            return DEFAULT;
        }
    }

    public enum SearchCriteriaName {
        ID("id"),
        //ACTIVE("active"),
        //IDENTIFIER("identifier"),
        FAMILY("LastName"),
        GIVEN("FirstName"),
        //TELECOM("Contact"),
        //BIRTHDATE("Birthdate"),
        ADDRESS("Address"),
        ADDRESS_CITY("City"),
        ADDRESS_COUNTRY("Country"),
        ADDRESS_POSTALCODE("PostalCode"),
        ADDRESS_STATE("State"),
        GENDER("Gender");


        private String text;

        SearchCriteriaName(String text) {
            this.text = text;
        }

        public static SearchCriteriaName fromString(String text) {
            for (SearchCriteriaName b : SearchCriteriaName.values()) {
                if (b.text.equalsIgnoreCase(text)) {
                    return b;
                }
            }
            throw new IllegalArgumentException(String.format("Not supported search criteria %s", text));
        }
    }
}
