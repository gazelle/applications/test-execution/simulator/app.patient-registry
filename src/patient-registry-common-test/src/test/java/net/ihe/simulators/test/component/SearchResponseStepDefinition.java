package net.ihe.simulators.test.component;

import io.cucumber.java.en.Then;
import net.ihe.gazelle.app.patientregistry.business.patient.PatientIdentifier;
import net.ihe.gazelle.app.patientregistry.search.wrapper.SearchResponseWrapper;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SearchResponseStepDefinition {

   private SearchResponseWrapper searchResponse;

   public SearchResponseStepDefinition(SearchResponseWrapper searchResponse) {
      this.searchResponse = searchResponse;
   }

   @Then("received {int} patients")
   public void receivedResultSizePatients(int responseListSize) {
      assertEquals(responseListSize, searchResponse.getPatients().size());
   }

   @Then("received patient {int} has identifier {string} from assigning authority {string}")
   public void receivedPatientWithIdentifier(int patientIndex, String identifierValue, String systemIdentifier) {
      assertTrue(String.format("Retrieve patient should contain identifier %s %s", systemIdentifier, identifierValue),
            containsPatientIdentifier(searchResponse.getPatients().get(patientIndex - 1).getPatientIdentifiers(), systemIdentifier, identifierValue));
   }

   private boolean containsPatientIdentifier(Set<PatientIdentifier> patientIdentifiers, String systemIdentifier, String identifierValue) {
      for (PatientIdentifier patientIdentifier : patientIdentifiers) {
         if (patientIdentifier.getSystemIdentifier().equals(systemIdentifier) && patientIdentifier.getValue().equals(identifierValue)) {
            return true;
         }
      }
      return false;
   }

}
