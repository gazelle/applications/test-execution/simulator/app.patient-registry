package net.ihe.simulators.test.component;

import io.cucumber.java.Before;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:Feature" , glue ={"net.ihe.simulators.test.component"})
public class CucumberLauncherTestIT {



}
