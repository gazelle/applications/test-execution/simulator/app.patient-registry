package net.ihe.simulators.test.component;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.ihe.gazelle.app.patientregistry.search.wrapper.PatientSearchRequestWrapper;
import net.ihe.gazelle.app.patientregistry.search.wrapper.SearchResponseWrapper;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;

public class HTTPRequestStepDefinition {

   // Cucumber shared states
   private PatientSearchRequestWrapper searchCriteria;
   private SearchResponseWrapper searchResponse;

   // local members
   private Response httpResponse;
   private String baseUrl;
   private WebTarget webTarget
         = ClientBuilder.newClient().target(getBaseUrl() + "/patient-registry/patient");

   public HTTPRequestStepDefinition(PatientSearchRequestWrapper searchCriteria, SearchResponseWrapper searchResponse) {
      this.searchCriteria = searchCriteria;
      this.searchResponse = searchResponse;
   }

   public String getBaseUrl() {
      if (this.baseUrl == null) {
         this.baseUrl = System.getenv().get("APP_URL");
         if (this.baseUrl == null) {
            this.baseUrl = "http://localhost:8080";
         }
      }
      return this.baseUrl;
   }

   @When("retrieve is made on id {string}")
   public void retrieveIsMade(String id) {
      this.httpResponse = webTarget.path("/search/" + id).request(MediaType.APPLICATION_XML_TYPE).get();
      loadSearchResponse(httpResponse.readEntity(SearchResponseWrapper.class));
   }

   @When("search is done")
   public void searchIsMade() {
      this.httpResponse = webTarget.path("/search/filter").request(MediaType.APPLICATION_XML_TYPE)
            .post(Entity.entity(searchCriteria, MediaType.APPLICATION_XML_TYPE));
      loadSearchResponse(httpResponse.readEntity(SearchResponseWrapper.class));
   }

   @Then("received HTTP status code ([0-9]+)$")
   public void httpReturnResponseCodeIs(Integer responseCode) {
      assertEquals(responseCode.intValue(), httpResponse.getStatus());
   }

   private void loadSearchResponse(SearchResponseWrapper searchResponseLocal) {
      searchResponse.setPatients(searchResponseLocal.getPatients());
      searchResponse.setSearchErrors(searchResponseLocal.getSearchErrors());
   }
}
