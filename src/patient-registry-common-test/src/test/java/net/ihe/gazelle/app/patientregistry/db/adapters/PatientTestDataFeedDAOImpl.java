package net.ihe.gazelle.app.patientregistry.db.adapters;

import net.ihe.gazelle.app.patientregistry.application.search.HierarchicDesignatorDAO;
import net.ihe.gazelle.app.patientregistry.business.patient.Address;
import net.ihe.gazelle.app.patientregistry.business.patient.Patient;
import net.ihe.gazelle.app.patientregistry.business.patient.PatientIdentifier;
import net.ihe.gazelle.app.patientregistry.db.patient.GenderCodeDB;
import net.ihe.gazelle.app.patientregistry.db.patient.HierarchicDesignator;
import net.ihe.gazelle.app.patientregistry.db.patient.PatientAddress;
import net.ihe.gazelle.app.patientregistry.db.patient.PatientDB;
import net.ihe.gazelle.app.patientregistry.db.patient.PatientIdentifierDB;
import net.ihe.simulators.test.component.PatientTestDataFeedDAO;
import org.hibernate.query.criteria.HibernateCriteriaBuilder;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Iterator;
import java.util.List;

public class PatientTestDataFeedDAOImpl implements PatientTestDataFeedDAO {

   EntityManager entityManager;

   public PatientTestDataFeedDAOImpl(EntityManager entityManager) {
      this.entityManager = entityManager;
   }

   @Override
   public void createPatient(Patient patient) {
      PatientDB patientDB = new PatientDB();

      patientDB.setUuid(patient.getUuid());
      patientDB.setLastName(patient.getNames().iterator().next().getFamily());
      patientDB.setFirstName(patient.getNames().iterator().next().getGivens().get(0));
      patientDB.setGenderCode(GenderCodeDB.fromGenderCode(patient.getGender()));

      entityManager.persist(patientDB);
   }

   @Override
   public void createAssigningAuthority(String systemName, String systemIdentifier) {
      HierarchicDesignator assigningAuthority = new HierarchicDesignator();
      assigningAuthority.setUniversalID(systemIdentifier);
      assigningAuthority.setNamespaceID(systemName);
      entityManager.persist(assigningAuthority);
   }

   @Override
   public void addAddress(String patientUUID, Address address) {

      PatientDAOImpl patientDAO = new PatientDAOImpl(entityManager);
      PatientDB patientDB = patientDAO.getPatientDBByUUID(patientUUID);

      PatientAddress patientAddress = new PatientAddress();
      Iterator<String> lineIterator = address.getLines().iterator();
      patientAddress.setAddressLine(lineIterator.next() + lineIterator.next());
      patientAddress.setZipCode(address.getPostalCode());
      patientAddress.setCity(address.getCity());
      patientAddress.setCountryCode(address.getCountryIso3());
      patientAddress.setState(address.getState());

      patientAddress.setPatient(patientDB);
      patientDB.getAddressList().add(patientAddress);

      entityManager.merge(patientDB);
   }

   @Override
   public void addPatientIdentifier(String patientUUID, PatientIdentifier patientIdentifier) {

      PatientDAOImpl patientDAO = new PatientDAOImpl(entityManager);
      PatientDB patientDB = patientDAO.getPatientDBByUUID(patientUUID);

      PatientIdentifierDB patientIdentifierDB = new PatientIdentifierDB();
      patientIdentifierDB.setIdNumber(patientIdentifier.getValue());

      List<HierarchicDesignator> matchingAssigningAuhtorities = getAssigningAuthorityFromSystemIdentifier(patientIdentifier.getSystemIdentifier());
      if (!matchingAssigningAuhtorities.isEmpty()) {
         patientIdentifierDB.setDomain(matchingAssigningAuhtorities.get(0));
      } else {
         throw  new IllegalArgumentException("Unknown assigning authority, register it first before assigning patient ids");
      }

      // bi-directional association
      patientIdentifierDB.getPatients().add(patientDB);
      patientDB.getPatientIdentifiers().add(patientIdentifierDB);

      entityManager.merge(patientDB);
   }

   private Integer getPatientDBIdFromUUID(String uuid) {
      TypedQuery<Integer> query = entityManager.createQuery("select p.id from PatientDB p where p.uuid = :uuid", Integer.class);
      query.setParameter("uuid", uuid);
      return query.getSingleResult();
   }

   private List<HierarchicDesignator> getAssigningAuthorityFromSystemIdentifier(String systemIdenfitier) {
      HierarchicDesignatorDAOImpl hierarchicDesignatorDAO = new HierarchicDesignatorDAOImpl(entityManager);
      return hierarchicDesignatorDAO.findHierarchicDesignatorsByUniversalId(systemIdenfitier);
   }

}
