/**
 * Copyright 2019 IHE International (http://www.ihe.net)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package net.ihe.gazelle.app.patientregistry.application.search;

import net.ihe.gazelle.app.patientregistry.business.exceptions.UnknownRequestedDomainException;
import net.ihe.gazelle.app.patientregistry.business.patient.Patient;
import net.ihe.gazelle.app.patientregistry.business.search.PatientSearchCriteria;

import java.util.List;

/**
 * <p>PatientRegistry class.</p>
 *
 * @author abe
 * @version 1.0: 03/12/2019
 */

public class SearchPatientService {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(SearchPatientService.class);
    private PatientDAO patientDAO;

    public SearchPatientService(PatientDAO patientDAO) throws IllegalArgumentException {
        if (patientDAO == null){
            LOG.error("PatientDAO is null, cannot instanciate SearchPatientService");
            throw new IllegalArgumentException("PatientDAO cannot be null");
        } else {
            this.patientDAO = patientDAO;
        }
    }

    public List<Patient> getPatientsForCriteria(PatientSearchCriteria patientCriteria, List<String> restrictedDomains) throws UnknownRequestedDomainException {
        return patientDAO.findPatients(patientCriteria, restrictedDomains);
    }

    public Patient getPatientsByUUID(String uuid){
        return patientDAO.getPatientByUUID(uuid);
    }
}
